package com.wasltec.driver.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.wasltec.driver.Activities.LoginActivity;
import com.wasltec.driver.Activities.MainActivity;
import com.wasltec.driver.Activities.RegisterActivity;
import com.wasltec.driver.Activities.TermsConditionsActivity;
import com.wasltec.driver.Adapters.SpinnerAdapters.CarModelsSpinnerAdapter;
import com.wasltec.driver.Adapters.SpinnerAdapters.CountrySpinnerAdapter;
import com.wasltec.driver.Adapters.SpinnerAdapters.ISpinnerAdapter;
import com.wasltec.driver.Adapters.SpinnerAdapters.VehiclesSpinnerAdapter;
import com.wasltec.driver.Helpers.DialogsHelper;
import com.wasltec.driver.Helpers.ImageHelper;
import com.wasltec.driver.Helpers.JSONHelper;
import com.wasltec.driver.Helpers.KeyboardHelper;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.Service.PostRequest;
import com.wasltec.driver.Helpers.Service.ServicePoster;
import com.wasltec.driver.Helpers.Service.ServiceResult;
import com.wasltec.driver.Model.City;
import com.wasltec.driver.Model.Country;
import com.wasltec.driver.Model.RegisterModel;
import com.wasltec.driver.R;
import com.wasltec.driver.Views.MediaChooser;
import com.wasltec.driver.WebService.Responses.GetDataResponse;
import com.wasltec.driver.WebService.ViewModels.RegisterViewModel;
import com.wasltec.driver.WebService.ViewModels.ValidationResult;
import com.wasltec.driver.WebService.ViewModels.VehicleType;
import com.wasltec.driver.volley.AppController;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterFragment extends Fragment implements View.OnClickListener {

    private View vNoData, vData, vLoading;
    public ImageView user_photo, user_id, user_istimara;
    Context context;
    private GetDataResponse mGetDataResponse;
    private final String USER_PHOTO_NAME = "user_photo";
    private final String USER_ID_PHOTO_NAME = "userId_photo";
    private final String USER_ISTIMARA_PHOTO_NAME = "user_istimara_photo";

    private List<String> countriesString = new ArrayList<>();
    private List<String> citiesString = new ArrayList<>();
    private List<City> Cities;
    EditText numberEditText, edt_firstName, passwordEditText, carPlatNumberEditText, LastName, email;
    TextView countryCodeEditText, employeeCodeEditText;
    RegisterViewModel registerViewModel;
    Spinner countrySpinner, carModelSpinner, citiesSpinner, serviceTypeSpinner, vehicleTypeSpinner;
    private List<Country> countries;
    String lang;
    private final int CAPTURE_USER_PHOTO_REQUEST_CODE = 50;
    private final int CAPTURE_USER_ID_PHOTO_REQUEST_CODE = 60;
    private final int CAPTURE_USER_ISTIMARA_PHOTO_REQUEST_CODE = 70;
    private final int ATTATCH_USER_PHOTO_REQUEST_CODE = 80;
    private final int ATTATCH_USER_ID_PHOTO_REQUEST_CODE = 90;
    private final int ATTATCH_USER_ISTIMARA_PHOTO_REQUEST_CODE = 100;
    public static int mRequestCode;
    boolean repeat = false;

    private Bitmap i1, i2, i3;

    int countrySelectedItem = 0, vehicleSelectedItem = 0, serviceSelectedItem = 0;
    boolean plateETVisibility, vehicleETVisibility, modelETVisibility;


    public RegisterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        new LanguageHelper().initLanguage(getActivity(), true);
        View fragment = inflater.inflate(R.layout.fragment_register, container, false);
        vNoData = fragment.findViewById(R.id.vNoData);
        vData = fragment.findViewById(R.id.vData);
        fragment.findViewById(R.id.btnRetry).setOnClickListener(this);
        fragment.findViewById(R.id.btnRegister).setOnClickListener(this);
        fragment.findViewById(R.id.terms).setOnClickListener(this);
        numberEditText = (EditText) fragment.findViewById(R.id.numberEditText);
        edt_firstName = (EditText) fragment.findViewById(R.id.firstName);
        LastName = (EditText) fragment.findViewById(R.id.LastName);
        passwordEditText = (EditText) fragment.findViewById(R.id.passwordEditText);
        email = (EditText) fragment.findViewById(R.id.email);
        carPlatNumberEditText = (EditText) fragment.findViewById(R.id.carPlateEditText);
        countrySpinner = (Spinner) fragment.findViewById(R.id.countryEditText);
        citiesSpinner = (Spinner) fragment.findViewById(R.id.cities);
        serviceTypeSpinner = (Spinner) fragment.findViewById(R.id.serviceType);
        vehicleTypeSpinner = (Spinner) fragment.findViewById(R.id.vehicleType);
        carModelSpinner = (Spinner) fragment.findViewById(R.id.carModelEditText);
        countryCodeEditText = (TextView) fragment.findViewById(R.id.countryCodeEditText);
        user_photo = (ImageView) fragment.findViewById(R.id.user_photo);
        user_id = (ImageView) fragment.findViewById(R.id.user_id);
        user_istimara = (ImageView) fragment.findViewById(R.id.user_istimara);
        employeeCodeEditText = (EditText) fragment.findViewById(R.id.employeeCodeEditText);
        registerViewModel = new RegisterViewModel(context.getApplicationContext());
        vLoading = fragment.findViewById(R.id.vLoading);
        setupUI(fragment.findViewById(R.id.registerLayout));
        lang = new LanguageHelper().getCurrentLanguage(context);
        countries = new JSONHelper().getCountries(context);
        if (mGetDataResponse == null)
            new PerformData().execute(1);
        else
            switchView(R.id.vData);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupControls();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRetry:
                switchView(R.id.vLoading);
                new PerformData().execute(1);
                break;
            case R.id.btnRegister:
                // doRegister();
                registerME();
                break;
            case R.id.terms:
                startActivity(new Intent(context, TermsConditionsActivity.class));
                break;
        }
    }

    public class PerformData extends AsyncTask<Integer, String, ServiceResult> {

        @Override
        protected ServiceResult doInBackground(Integer... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("CountryId", params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            PostRequest postRequest = new PostRequest(jsonObject, getString(R.string.get_data_url));
            return new ServicePoster(context).DoPost(postRequest);
        }

        @Override
        protected void onPostExecute(ServiceResult s) {
            Log.v("mustafa", s.getResult());
            if (!s.getSuccess()) {
                DialogsHelper.showToast(context, getString(R.string.service_error));
                switchView(R.id.vNoData);
            } else {
                GetDataResponse response = GetDataResponse.newInstance(s.getResult());
                if (response != null && response.isSuccess() && response.isValidResponse()) {
                    mGetDataResponse = response;
                    setupControls();
                    switchView(R.id.vData);
                } else {
                    DialogsHelper.showToast(context, getString(R.string.service_error));
                    switchView(R.id.vNoData);
                }
            }
        }
    }


    public void doRegister() {

        if (((BitmapDrawable) user_photo.getDrawable()) == null) {
            Toast.makeText(context, "Please,Enter Personal photo", Toast.LENGTH_LONG).show();
            return;
        }
        if (((BitmapDrawable) user_id.getDrawable()) == null) {
            Toast.makeText(context, "Please,Enter ID photo", Toast.LENGTH_LONG).show();
            return;
        }
        if (((BitmapDrawable) user_istimara.getDrawable()) == null) {
            Toast.makeText(context, "Please,Enter Licence Photo ", Toast.LENGTH_LONG).show();
            return;
        }
        if (numberEditText.getText().toString().length() < 9) {
            Toast.makeText(context, getString(R.string.register_view_model_enter_mobile_toast), Toast.LENGTH_LONG).show();
            return;
        }
        if (numberEditText.getText().toString().charAt(0) == '0') {
            Toast.makeText(context, getString(R.string.cant_start), Toast.LENGTH_LONG).show();
            return;
        }

        bindViewModel();
        ValidationResult result = registerViewModel.Validate();
        if (result.getSuccess()) {
            new KeyboardHelper().Hide(context, getActivity());
            new PerformRegister().execute(registerViewModel);
        } else {
            Toast.makeText(context, result.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void registerME() {

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest request = new StringRequest(Request.Method.POST, "http://icabservices.iwaseet.net/api/drivers/register", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                RegisterModel model = new Gson().fromJson(response, RegisterModel.class);


                if (model.getSuccess()) {
                    startActivity(new Intent(getActivity(), MainActivity.class).putExtra("ACTIVATE", model.getActivationcode()));
                    Toast.makeText(getActivity(), "successfully " + model.getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), "error" + model.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d("ACTIV", model.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "error" + error.getMessage(), Toast.LENGTH_SHORT).show();
//                Log.d("ACTIV", error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("FirstName", edt_firstName.getText().toString());
                params.put("LastName", LastName.getText().toString());
                params.put("Email", email.getText().toString());
                params.put("password", passwordEditText.getText().toString());
                params.put("number", numberEditText.getText().toString());
                params.put("CarPlatNumber", carPlatNumberEditText.getText().toString());
                params.put("InsuranceExpirationDate", "2012-04-23T18:25:43.511Z");
                params.put("InteriorColor", "1");
                params.put("ExteriorColor", "1");
                params.put("InsuranceNumber", carPlatNumberEditText.getText().toString());
                params.put("CarModel", "79");
                params.put("VehicleType", "1");
                params.put("DriverLicenseNumber", "234234");
                params.put("DriverResidencyNumber", "234235252");
                params.put("DriverResidencyExpirationDate", "2012-04-23T18:25:43.511Z");
                if (countrySpinner.getSelectedItemPosition()>1){
                    params.put("CountryId", String.valueOf(1));
                }
                else {
                    params.put("CountryId", String.valueOf(countries.get(countrySpinner.getSelectedItemPosition()).getId()));
                }
                params.put("CityId", String.valueOf(Cities.get(citiesSpinner.getSelectedItemPosition()).id));

                params.put("EmployeeCode", employeeCodeEditText.getText().toString());
                params.put("IsServiceProvider", "0");
                params.put("MobileTypeId", "2");
                params.put("PersonalPic", "dfgskfhbsdasdfadsljvhslvh");
                params.put("PersonalIDPic", "dfgskfhbsdasdfadsljvhslvh");
                params.put("PersonalDocPic", "dfgskfhbsdasdfadsljvhslvh");
                params.put("AppVersion", "4");
                params.put("OSVersion", "123");
                return params;

            }
        };

        AppController.getInstance().addToRequestQueue(request, "REGIST");
        request.setRetryPolicy(new DefaultRetryPolicy(
                7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);

    }

    public class PerformRegister extends AsyncTask<RegisterViewModel, String, String> {
        ProgressDialog mDialog;
        Bitmap x1, x2, x3;

        @Override
        protected void onPreExecute() {
            x1 = ((BitmapDrawable) user_photo.getDrawable()).getBitmap();
            x2 = ((BitmapDrawable) user_id.getDrawable()).getBitmap();
            x3 = ((BitmapDrawable) user_istimara.getDrawable()).getBitmap();
            mDialog = DialogsHelper.getProgressDialog(getActivity(), getString(R.string.loading), getString(R.string.please_wait));
            mDialog.show();
        }

        @Override
        protected String doInBackground(RegisterViewModel... params) {
            try {
                Log.v("ddd", params[0].toString());
                PostRequest postRequest = new PostRequest(params[0].toJson(), getString(R.string.register_url));
                return new ServicePoster(context).DoPost(postRequest, 1);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        private void hideLoadingDialog(String message) {
            mDialog.cancel();
            if (message.length() > 0)
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(String s) {
            mDialog.cancel();
            Log.v("mustafa", s.toString());
//            if (s.equalsIgnoreCase("error")) {
//                this.hideLoadingDialog(getString(R.string.service_error));
//            }
            try {
                JSONObject resultObject = new JSONObject(s.toString());
                if (resultObject.has("Success")) {
                    boolean success = resultObject.getBoolean("Success");
                    final String message = resultObject.getString("Message");
                    final String arabicMessage = resultObject.getString("ArabicMessage");
                    if (success) {
                        hideLoadingDialog("");
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage(getString(R.string.will_send_you_message))
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                });
                        final AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        this.hideLoadingDialog(new LanguageHelper().getCurrentLanguage(context) == LanguageHelper.ENGLISH_LANGUAGE ? message : arabicMessage);
                    }
                } else {
                    this.hideLoadingDialog(getString(R.string.service_error));
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                this.hideLoadingDialog(getString(R.string.service_error));
            }
        }
    }

    private void setupHandlers() {
        if (i1 != null) {
            user_photo.setImageBitmap(i1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                user_photo.setBackground(null);
        }
        if (i2 != null) {
            user_id.setImageBitmap(i2);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                user_id.setBackground(null);
        }
        if (i3 != null) {
            user_istimara.setImageBitmap(i3);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                user_istimara.setBackground(null);
        }
        user_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MediaChooser(context, CAPTURE_USER_PHOTO_REQUEST_CODE, ATTATCH_USER_PHOTO_REQUEST_CODE, USER_PHOTO_NAME).show();
            }
        });
        user_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MediaChooser(context, CAPTURE_USER_ID_PHOTO_REQUEST_CODE, ATTATCH_USER_ID_PHOTO_REQUEST_CODE, USER_ID_PHOTO_NAME).show();
            }
        });
        user_istimara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MediaChooser(context, CAPTURE_USER_ISTIMARA_PHOTO_REQUEST_CODE, ATTATCH_USER_ISTIMARA_PHOTO_REQUEST_CODE, USER_ISTIMARA_PHOTO_NAME).show();
            }
        });


    }

    ArrayAdapter<String> countriesArrayAdapter;

    private void setupControls() {


        if (countries != null) {
            for (int i = 0; i < countries.size(); i++) {
                if (lang.equalsIgnoreCase(LanguageHelper.ENGLISH_LANGUAGE))
                    countriesString.add(countries.get(i).getName());
                else
                    countriesString.add(countries.get(i).getNameAr());
            }

            countriesArrayAdapter = new ArrayAdapter<>(context, R.layout.spinner_layout_item, countriesString);
            CountrySpinnerAdapter adapter = new CountrySpinnerAdapter(countries,getActivity());
            countriesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            countrySpinner.setAdapter(countriesArrayAdapter);

            countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (position > 0 && position != countrySelectedItem) {

                        new PerformData().execute(countries.get(countrySpinner.getSelectedItemPosition()).getId());
                    }
                    countryCodeEditText.setText(countries.get(countrySpinner.getSelectedItemPosition()).getCode());
                    Cities = new JSONHelper().getCities(context, countries.get(countrySpinner.getSelectedItemPosition()).getId());
                    if (Cities.size() == 0) {
                        Cities = new JSONHelper().getCities(context, 0);
                    }
                    citiesString.clear();
                    for (int i = 0; i < Cities.size(); i++) {

                        if (lang.equalsIgnoreCase(LanguageHelper.ENGLISH_LANGUAGE))
                            citiesString.add(Cities.get(i).Name);
                        else
                            citiesString.add(Cities.get(i).NameAr);
                    }
                    ArrayAdapter<String> citiesArrayAdapter = new ArrayAdapter<>(context, R.layout.spinner_layout_item, citiesString); //selected item will look like a spinner set from XML
                    citiesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    citiesSpinner.setAdapter(citiesArrayAdapter);
                    countrySelectedItem = position;
                    repeat = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            countrySpinner.setSelection(countrySelectedItem);
        }

        final ArrayList<VehicleType> vehicleAdapter = new ArrayList<>();
        if (mGetDataResponse != null) {
            for (int i = 0; i < mGetDataResponse.getVehicleTypes().size(); i++) {
                if (mGetDataResponse.getVehicleTypes().get(i).isServiceProvider())
                    vehicleAdapter.add(mGetDataResponse.getVehicleTypes().get(i));
            }
            vehicleTypeSpinner.setAdapter(new VehiclesSpinnerAdapter(vehicleAdapter, getActivity()));
            vehicleTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    registerViewModel.setVehicleType(vehicleAdapter.get(position).getId());
                    vehicleSelectedItem = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            vehicleTypeSpinner.setSelection(vehicleSelectedItem);
        }
        if (mGetDataResponse != null && mGetDataResponse.getVehicleTypes().size() > 0) {
            registerViewModel.setVehicleType(mGetDataResponse.getVehicleTypes().get(0).getId());
            final String[] choices = {"Driver", "Service Provider"};
            ArrayAdapter<String> serviceTypeAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, choices);
            serviceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            serviceTypeSpinner.setAdapter(serviceTypeAdapter);
            serviceTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {
                        case 0:
                            carPlatNumberEditText.setVisibility(View.VISIBLE);
                            vehicleTypeSpinner.setVisibility(View.GONE);
                            carModelSpinner.setVisibility(View.VISIBLE);
                            plateETVisibility = true;
                            vehicleETVisibility = false;
                            modelETVisibility = true;
                            if (mGetDataResponse.getVehicleTypes().size() > 1)
                                registerViewModel.setVehicleType(mGetDataResponse.getVehicleTypes().get(0).getId());
                            break;
                        case 1:
                            carPlatNumberEditText.setVisibility(View.GONE);
                            vehicleTypeSpinner.setVisibility(View.VISIBLE);
                            carModelSpinner.setVisibility(View.GONE);
                            plateETVisibility = false;
                            vehicleETVisibility = true;
                            modelETVisibility = false;
                            break;
                    }

                    serviceSelectedItem = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            serviceTypeSpinner.setSelection(serviceSelectedItem);
            carModelSpinner.setAdapter(new CarModelsSpinnerAdapter(mGetDataResponse.getModels(), getActivity()));
            carPlatNumberEditText.setVisibility(plateETVisibility ? View.VISIBLE : View.GONE);
            vehicleTypeSpinner.setVisibility(vehicleETVisibility ? View.VISIBLE : View.GONE);
            carModelSpinner.setVisibility(modelETVisibility ? View.VISIBLE : View.GONE);
        }

        setupHandlers();
    }

    private void bindViewModel() {
        try {
            if (serviceTypeSpinner.getSelectedItemPosition() == 0)
                registerViewModel.setModel(((ISpinnerAdapter) carModelSpinner.getAdapter()).getIdForPosition(carModelSpinner.getSelectedItemPosition()));
            else
                registerViewModel.setModel(1);

            registerViewModel.setCountry(countries.get(countrySpinner.getSelectedItemPosition()).id);
            if (citiesSpinner.getAdapter().getCount() > 0)
                registerViewModel.setCity(Cities.get(citiesSpinner.getSelectedItemPosition()).id);
            registerViewModel.setFirstName(edt_firstName.getText().toString());
            registerViewModel.setLastName(LastName.getText().toString());
            registerViewModel.setEmail(email.getText().toString());
            registerViewModel.setPassword(passwordEditText.getText().toString());
            if (serviceTypeSpinner.getSelectedItemPosition() == 0)
                registerViewModel.setCarPlatNumber(carPlatNumberEditText.getText().toString());
            else
                registerViewModel.setCarPlatNumber("serviceProvider");
            registerViewModel.setNumber(countries.get(countrySpinner.getSelectedItemPosition()).getCode() + numberEditText.getText().toString());
            registerViewModel.setEmployeeCode(employeeCodeEditText.getText().toString());
            registerViewModel.setIsServiceProvider(serviceTypeSpinner.getSelectedItemPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void switchView(int viewId) {
        vLoading.setVisibility(viewId == R.id.vLoading ? View.VISIBLE : View.GONE);
        vNoData.setVisibility(viewId == R.id.vNoData ? View.VISIBLE : View.GONE);
        vData.setVisibility(viewId == R.id.vData ? View.VISIBLE : View.GONE);
    }


    public void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(v);
                    return false;
                }

            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void hideSoftKeyboard(View view) {
        new KeyboardHelper().Hide(context.getApplicationContext(), getActivity());
    }

    public void performTakingPicture(Intent intent) {
        new PerformTakingPictureHere().execute(intent);
    }


    public class PerformTakingPictureHere extends AsyncTask<Intent, Bitmap, Bitmap> {
        ImageHelper helper = new ImageHelper();
        Bitmap img;


        @Override
        protected void onPreExecute() {
            RegisterActivity.dialog = DialogsHelper.getProgressDialog(getActivity(), getString(R.string.please_wait), getString(R.string.please_wait));
            RegisterActivity.dialog.show();
        }

        @Override
        protected Bitmap doInBackground(Intent... params) {
            if (params[0].getExtras() != null && params[0].getExtras().get("data") != null)
                img = (Bitmap) params[0].getExtras().get("data");
            else
                img = helper.decodeSampledBitmapFromResource(context, params[0].getData(), 500, 500);

//            img = helper.rotateMyPhoto(new File(params[0].getData().getPath()), img);
            return img;
        }

        @Override
        protected void onPostExecute(Bitmap img) {
            super.onPostExecute(img);
            if (img != null) {
                switch (mRequestCode) {
                    case CAPTURE_USER_PHOTO_REQUEST_CODE:
                    case ATTATCH_USER_PHOTO_REQUEST_CODE:
                        user_photo.setImageBitmap(img);
                        i1 = img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                            user_photo.setBackground(null);
                        break;
                    case CAPTURE_USER_ID_PHOTO_REQUEST_CODE:
                    case ATTATCH_USER_ID_PHOTO_REQUEST_CODE:
                        user_id.setImageBitmap(img);
                        i2 = img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                            user_id.setBackground(null);
                        break;
                    case CAPTURE_USER_ISTIMARA_PHOTO_REQUEST_CODE:
                    case ATTATCH_USER_ISTIMARA_PHOTO_REQUEST_CODE:
                        user_istimara.setImageBitmap(img);
                        i3 = img;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                            user_istimara.setBackground(null);
                        break;
                }
                Log.e("photo", "taken");
            }
            if ((RegisterActivity.dialog != null) && RegisterActivity.dialog.isShowing())
                RegisterActivity.dialog.cancel();
        }
    }

}
