package com.wasltec.driver;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.Service.ServicePoster;
import com.wasltec.driver.Helpers.Service.ServiceResult;
import com.wasltec.driver.Helpers.SharedPrefHelper;
import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.WebService.Requests.UpdatePushRequest;
import com.wasltec.driver.WebService.ViewModels.UpdatePushViewModel;


public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {

      String token = intent.getStringExtra("tokenkey");

            sendRegistrationToServer(true, token);
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh, retry", e);
            FirebaseInstanceId.getInstance().getToken();
        }
    }


    private void sendRegistrationToServer(boolean isEnabled, String token) {
        if (isEnabled) {
            UpdatePushViewModel model = new UpdatePushViewModel();
            model.setPushEnabled(true);
            model.setPushToken(token);
            UpdatePushRequest request = new UpdatePushRequest();
            request.setModel(model);
            String accessToken = ((Driver) new DatabaseHandler(this).getSingle(new Driver(), 1)).getAccessToken();
            ServiceResult result = new ServicePoster(this).DoPost(request, accessToken);
            new SharedPrefHelper().setSharedBoolean(this, SharedPrefHelper.SENT_TOKEN_TO_SERVER, result.getSuccess());
        } else {
            new SharedPrefHelper().setSharedBoolean(this, SharedPrefHelper.SENT_TOKEN_TO_SERVER, false);
        }
    }
}
