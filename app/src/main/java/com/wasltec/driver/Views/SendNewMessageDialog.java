package com.wasltec.driver.Views;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.wasltec.driver.R;

public class SendNewMessageDialog extends DialogFragment {

    NewMessageNotification searchNotification;
    View view;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            searchNotification = (NewMessageNotification) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.send_message_dialog, null);
        builder.setView(view)
                .setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText txt = (EditText) view.findViewById(R.id.txtNewMessage);
                        final String m = txt.getText().toString();
                        searchNotification.onDialogPositiveClick(m);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).setTitle(R.string.send_message);
        return builder.create();
    }

    public interface NewMessageNotification {
        void onDialogPositiveClick(String message);
    }
}