package com.wasltec.driver.Views;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.wasltec.driver.R;

public class Alert extends DialogFragment {

    String message = "";

    DialogInterface.OnClickListener dialogInterfaceOnClickListener;
    View view;

    public Alert() {

        super();

    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setOnPositiveButtonClicked(DialogInterface.OnClickListener dialogInterfaceOnClickListener) {

        this.dialogInterfaceOnClickListener = dialogInterfaceOnClickListener;
    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        view = inflater.inflate(R.layout.new_message_dialog, null);
        TextView txt = (TextView) view.findViewById(R.id.txtNewMessage);
        txt.setText(this.message);


        builder.setView(view)
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if (dialogInterfaceOnClickListener != null) {

                            dialogInterfaceOnClickListener.onClick(dialog, id);
                        }
                    }
                })
                .setTitle("Wslni Taxi");
        return builder.create();
    }
}


