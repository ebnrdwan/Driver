package com.wasltec.driver.Views;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


import com.wasltec.driver.R;

public class NewMessageDialog extends DialogFragment {


    String message = "";
    SearchNotification searchNotification;
    View view;

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            searchNotification = (SearchNotification) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.new_message_dialog, null);
        TextView txt = (TextView) view.findViewById(R.id.txtNewMessage);
        txt.setText(this.message);
        builder.setView(view)
                .setPositiveButton(R.string.replay_message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        searchNotification.onDialogPositiveClick();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).setTitle(R.string.new_message);
        return builder.create();
    }

    public interface SearchNotification {

        void onDialogPositiveClick();

    }
}
