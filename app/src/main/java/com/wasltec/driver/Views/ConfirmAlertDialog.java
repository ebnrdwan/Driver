package com.wasltec.driver.Views;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.wasltec.driver.R;

public class ConfirmAlertDialog extends AlertDialog implements View.OnClickListener {
    private String message1;
    private String message2;
    private View.OnClickListener onClickListener;

    public ConfirmAlertDialog(Context context, String message1, String message2) {
        super(context);
        this.message1 = message1;
        this.message2 = message2;
    }

    public void setOnButtonClickListener(View.OnClickListener onClickListener) {

        this.onClickListener = onClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.confirm_dialog);
        getWindow().setBackgroundDrawableResource(R.drawable.thank_background);
        setCancelable(false);
        TextView tvMessage1 = (TextView)findViewById(R.id.tvMessage1);
        TextView tvMessage2 = (TextView)findViewById(R.id.tvMessage2);
        Button btnOk = (Button) findViewById(R.id.btnOk);
        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        tvMessage1.setText(this.message1);
        tvMessage2.setText(this.message2);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnOk:

                if(this.onClickListener != null) {

                    this.onClickListener.onClick(view);
                }
                dismiss();

                break;

            case R.id.btnCancel:
                dismiss();
                break;

        }
    }
}
