package com.wasltec.driver.Views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.wasltec.driver.R;

import java.io.File;

public class MediaChooser extends AlertDialog implements View.OnClickListener {
    int cameraRequestCode,attachRequestCode;
    Context mContext;
    File imageFolderPath;
    String imageName;

    public MediaChooser(Context context, int cameraRequestCode, int attachRequestCode , String imageName) {
        super(context);
        this.mContext = context;
        this.cameraRequestCode = cameraRequestCode;
        this.attachRequestCode = attachRequestCode;
        this.imageName=imageName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.media_chooser_layout);
        int width_dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, mContext.getResources().getDisplayMetrics());
        getWindow().setLayout(width_dp, WindowManager.LayoutParams.WRAP_CONTENT);
        imageFolderPath = new File(Environment.getExternalStorageDirectory().getPath() + "/Wasltec/DriverImages/");
        if(!imageFolderPath.exists())
            imageFolderPath.mkdirs();
        setCancelable(true);
        TextView attach_image = (TextView) findViewById(R.id.attach_image);
        TextView take_photo = (TextView) findViewById(R.id.take_photo);
        attach_image.setOnClickListener(this);
        take_photo.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.attach_image:
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                ((Activity) mContext).startActivityForResult(i, attachRequestCode);
                dismiss();
                break;
            case R.id.take_photo:
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                    ((Activity) mContext).startActivityForResult(intent, cameraRequestCode);
                } else {
                    Toast.makeText(getOwnerActivity(), "can't find camera", Toast.LENGTH_SHORT).show();
                }
                dismiss();
                break;
        }
    }
}