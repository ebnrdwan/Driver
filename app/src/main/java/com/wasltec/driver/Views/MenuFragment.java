package com.wasltec.driver.Views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wasltec.driver.Activities.AddCreditActivity;
import com.wasltec.driver.Activities.BalanceActivity;
import com.wasltec.driver.Activities.LaterRequestsActivity;
import com.wasltec.driver.Adapters.Custom_menu_Adapter;
import com.wasltec.driver.Activities.ContactUsActivity;
import com.wasltec.driver.Helpers.AppStateHelper;
import com.wasltec.driver.Helpers.AppStateContext;
import com.wasltec.driver.Helpers.DriverService;
import com.wasltec.driver.Activities.HistoryActivity;
import com.wasltec.driver.Activities.ProfileActivity;
import com.wasltec.driver.R;

public class MenuFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, Animation.AnimationListener {
    public static String[] menu_list_array;
    Context mcontext;
    boolean menu_visible = false;
    Custom_menu_Adapter adapter;
    RelativeLayout sliding_menu_list = null;
    Animation slideDwon_menu, slideUp_menu;
    ImageButton my_cutom_menu, language_btn;
    TextView connection_title,credit;
    private ListView menu_listView;
    private View.OnClickListener onClickListener;
    private View.OnClickListener onClickExitListener;
    static boolean mSupportCredit;

    public static MenuFragment newInstance(boolean supportCredit) {
        mSupportCredit = supportCredit;
        return new MenuFragment();
    }

    public void setOnClickExitListener(View.OnClickListener onClickExitListener) {
        this.onClickExitListener = onClickExitListener;
    }

    public void setOnChangeLanguageListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mcontext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideDwon_menu = AnimationUtils.loadAnimation(mcontext,
                R.anim.sliding_down_menu);
        slideUp_menu = AnimationUtils.loadAnimation(mcontext,
                R.anim.slide_up);
        slideUp_menu.setAnimationListener(this);
        slideDwon_menu.setAnimationListener(this);
        if (mSupportCredit)
            menu_list_array = getActivity().getResources().getStringArray(R.array.menu_list_array);
        else
            menu_list_array = getActivity().getResources().getStringArray(R.array.menu_list_array_balance);
    }

    public void setMenu_btnEnabled(final boolean enabled) {
        if (getActivity() == null) return;
        (getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (my_cutom_menu != null)
                    my_cutom_menu.setEnabled(enabled);
            }
        });
    }

    public void setConnection_title(final String title) {
        if (getActivity() == null) return;
        (getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connection_title != null)
                    connection_title.setText(title);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.custom_menu_fragment, container, false);
        menu_listView = (ListView) v.findViewById(R.id.menu_list_view);
        this.adapter = new Custom_menu_Adapter(mcontext, menu_list_array);
        menu_listView.setAdapter(adapter);
        this.adapter.notifyDataSetChanged();
        menu_listView.setOnItemClickListener(this);
        sliding_menu_list = (RelativeLayout) v.findViewById(R.id.slide_menu_list);
        credit = (TextView) v.findViewById(R.id.tvCredit);
        if (!mSupportCredit)
            credit.setVisibility(View.GONE);
        my_cutom_menu = (ImageButton) v.findViewById(R.id.menu_icon);
        language_btn = (ImageButton) v.findViewById(R.id.lang_menu_btn);
        language_btn.setOnClickListener(this);
        my_cutom_menu.setOnClickListener(this);
        connection_title = (TextView) v.findViewById(R.id.connection_title);
        setConnection_title(AppStateHelper.getAppState(getActivity(), DriverService.isRun));
        setMenu_btnEnabled(AppStateContext.State != null && !(AppStateContext.State.isInCall() || AppStateContext.State.isInTrip()));
        return v;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.lang_menu_btn) {
            if (this.onClickListener != null) {
                this.onClickListener.onClick(view);
            }
        }

        if (view.getId() == R.id.menu_icon) {
            if (!menu_visible) {
                ((Activity) mcontext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sliding_menu_list.setVisibility(View.VISIBLE);
                        menu_listView.setVisibility(View.VISIBLE);
                        sliding_menu_list.startAnimation(slideDwon_menu);
                    }
                });
                menu_visible = true;
            } else {
                ((Activity) mcontext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sliding_menu_list.setVisibility(View.VISIBLE);
                        menu_listView.setVisibility(View.VISIBLE);
                        sliding_menu_list.startAnimation(slideUp_menu);
                        menu_listView.setVisibility(View.INVISIBLE);
                    }
                });
                menu_visible = false;
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        switch (position) {
            case 0:
                this.startActivity(new Intent(mcontext, ProfileActivity.class));
                break;
            case 1:
                this.startActivity(new Intent(mcontext, HistoryActivity.class));
                break;
            case 2:
                startActivity(new Intent(mcontext, ContactUsActivity.class));
                break;
            case 3:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Waslni Taxi App");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://goo.gl/9d3dKO");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case 4:
                if (mSupportCredit)
                    startActivity(new Intent(getActivity(), AddCreditActivity.class));
                else
                    startActivity(new Intent(mcontext, BalanceActivity.class));
                break;
            case 5:
                startActivity(new Intent(getActivity(), LaterRequestsActivity.class));
                break;
            case 6:
                if (this.onClickExitListener != null) {

                    this.onClickExitListener.onClick(view);
                }

                break;
        }
        ((Activity) mcontext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sliding_menu_list.setVisibility(View.VISIBLE);
                menu_listView.setVisibility(View.VISIBLE);
                sliding_menu_list.startAnimation(slideUp_menu);
                menu_listView.setVisibility(View.INVISIBLE);
            }
        });
        menu_visible = false;
    }

    @Override
    public void onAnimationEnd(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }
}