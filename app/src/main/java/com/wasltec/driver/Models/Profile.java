package com.wasltec.driver.Models;

import android.content.ContentValues;
import android.database.Cursor;
import org.json.JSONObject;
import java.util.ArrayList;

public class Profile implements IModel {
    private String mFirstName,mEmail,mPlateNumber,mCompany,mPassword,mCurrancyArabic,mCurrancyEnglish,mVehicleTypeArabic,mVehicleTypeEnglish;
    private double mHourCost,mKilometerCost,mOpenDoorPrice,mWaitingPerMinuteCost;
    private int mId,mCredit;
    private boolean mSupportCredit,isServiceProvider;

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    private final String TABLE_NAME="profile";
    private final String ID_KEY ="id";
    private final String NAME_KEY="name";
    private final String EMAIL_KEY="email";
    private final String PLATE_NUMBER_KEY ="plateNumber";
    private final String COMPANY_KEY ="company";
    private final String CREDIT_KEY = "credit";
    private final String HOUR_COST_KEY = "hourCost";
    private final String KILOMETER_COST_KEY ="kilometerCost";
    private final String OPEN_DOOR_PRICE_KEY = "openDoorPriceKey";
    private final String WAITING_PER_MINUTE_COST="waitingPerMinuteCost";
    private final String PASSWORD_KEY ="password";
    private final String SUPPORT_CREDIT = "supportCredit";
    private final String CURRANCY_ARABIC = "currancyArabic";
    private final String CURRANCY_ENGLISH = "currancyEnglish";
    private final String IS_SERVICE_PROVIDER = "isServiceProvider";
    private final String VEHICLE_TYPE_ARABIC = "vehicleTypeArabic";
    private final String VEHICLE_TYPE_ENGLISH = "vehicleTypeEnglish";

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getSqlCreation() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + ID_KEY + " INTEGER NOT NULL," + NAME_KEY + " text NOT NULL," + EMAIL_KEY + " text NOT NULL," + PLATE_NUMBER_KEY + " TEXT NOT NULL," + COMPANY_KEY + " TEXT NOT NULL," + CREDIT_KEY + " NUMBER NOT NULL,"+HOUR_COST_KEY+" NUMBER NOT NULL,"+KILOMETER_COST_KEY+" NUMBER NOT NULL,"+OPEN_DOOR_PRICE_KEY+" NUMBER NOT NULL,"+WAITING_PER_MINUTE_COST+" NUMBER NOT NULL, "+PASSWORD_KEY+" TEXT NOT NULL, "+ SUPPORT_CREDIT + " NUMBER NOT NULL," + IS_SERVICE_PROVIDER + " NUMBER NOT NULL,"+VEHICLE_TYPE_ARABIC+" TEXT NOT NULL, "+VEHICLE_TYPE_ENGLISH+" TEXT NOT NULL, "+CURRANCY_ARABIC+" TEXT NOT NULL, "+CURRANCY_ENGLISH+" TEXT NOT NULL);";
    }

    @Override
    public String getClearSql() {
        return "DELETE FROM " + TABLE_NAME;
    }

    @Override
    public String getListSql() {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + "  ";
    }

    @Override
    public String getSingleSql(int id) {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + " WHERE  " + ID_KEY + "= " + id;
    }

    @Override
    public ArrayList<IModel> fromJson(JSONObject jsonObject) {
        return null;
    }

    @Override
    public IModel fromCursor(Cursor cursor) {
        setId(1);
        setFirstName(cursor.getString((cursor.getColumnIndex(NAME_KEY))));
        setEmail(cursor.getString((cursor.getColumnIndex(EMAIL_KEY))));
        setPlateNumber(cursor.getString((cursor.getColumnIndex(PLATE_NUMBER_KEY))));
        setCompany(cursor.getString((cursor.getColumnIndex(COMPANY_KEY))));
        setCredit(cursor.getInt(cursor.getColumnIndex(CREDIT_KEY)));
        setHourCost(cursor.getDouble(cursor.getColumnIndex(HOUR_COST_KEY)));
        setOpenDoorPrice(cursor.getDouble(cursor.getColumnIndex(OPEN_DOOR_PRICE_KEY)));
        setWaitingPerMinuteCost(cursor.getDouble(cursor.getColumnIndex(WAITING_PER_MINUTE_COST)));
        setKilometerCost(cursor.getDouble(cursor.getColumnIndex(KILOMETER_COST_KEY)));
        if(cursor.getColumnIndex(PASSWORD_KEY) >=0)
        setPassword(cursor.getString(cursor.getColumnIndex(PASSWORD_KEY)));
        setCurrancyArabic(cursor.getString(cursor.getColumnIndex(CURRANCY_ARABIC)));
        setCurrancyEnglish(cursor.getString(cursor.getColumnIndex(CURRANCY_ENGLISH)));
        setSupportCredit(cursor.getInt(cursor.getColumnIndex(SUPPORT_CREDIT)) == 1);
        setIsServiceProvider(cursor.getInt(cursor.getColumnIndex(IS_SERVICE_PROVIDER)) == 1);
        if (cursor.getColumnIndex(VEHICLE_TYPE_ARABIC)>0) {
            setVehicleTypeArabic(cursor.getString(cursor.getColumnIndex(VEHICLE_TYPE_ARABIC)));
            setVehicleTypeEnglish(cursor.getString(cursor.getColumnIndex(VEHICLE_TYPE_ENGLISH)));
        }
        return this;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ID_KEY, 1);
        values.put(NAME_KEY, getFirstName());
        values.put(EMAIL_KEY, getEmail());
        values.put(PLATE_NUMBER_KEY, getPlateNumber());
        values.put(COMPANY_KEY, getCompany());
        values.put(CREDIT_KEY, getCredit());
        values.put(HOUR_COST_KEY, getHourCost());
        values.put(OPEN_DOOR_PRICE_KEY, getOpenDoorPrice());
        values.put(WAITING_PER_MINUTE_COST, getWaitingPerMinuteCost());
        values.put(KILOMETER_COST_KEY, getKilometerCost());
        values.put(PASSWORD_KEY, getPassword());
        values.put(SUPPORT_CREDIT, isSupportCredit());
        values.put(CURRANCY_ARABIC, getCurrancyArabic());
        values.put(CURRANCY_ENGLISH, getCurrancyEnglish());
        values.put(IS_SERVICE_PROVIDER, isServiceProvider());
        values.put(VEHICLE_TYPE_ARABIC,getVehicleTypeArabic());
        values.put(VEHICLE_TYPE_ENGLISH,getVehicleTypeEnglish());
        return values;
    }

    public double getWaitingPerMinuteCost() {
        return mWaitingPerMinuteCost;
    }

    public void setWaitingPerMinuteCost(double waitingPerMinuteCost) {
        mWaitingPerMinuteCost = waitingPerMinuteCost;
    }

    public double getOpenDoorPrice() {
        return mOpenDoorPrice;
    }

    public void setOpenDoorPrice(double openDoorPrice) {
        mOpenDoorPrice = openDoorPrice;
    }

    public double getKilometerCost() {
        return mKilometerCost;
    }

    public void setKilometerCost(double kilometerCost) {
        mKilometerCost = kilometerCost;
    }

    public double getHourCost() {
        return mHourCost;
    }

    public void setHourCost(double hourCost) {
        mHourCost = hourCost;
    }

    public int getId() {
        return mId;
    }

    @Override
    public String getIdField() {
        return ID_KEY;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPlateNumber() {
        return mPlateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        mPlateNumber = plateNumber;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public int getCredit() {
        return mCredit;
    }

    public void setCredit(int credit) {
        mCredit = credit;
    }

    public String getCurrancyArabic() {
        return mCurrancyArabic;
    }

    public void setCurrancyArabic(String mCurrancyArabic) {
        this.mCurrancyArabic = mCurrancyArabic;
    }

    public String getCurrancyEnglish() {
        return mCurrancyEnglish;
    }

    public void setCurrancyEnglish(String mCurrancyEnglish) {
        this.mCurrancyEnglish = mCurrancyEnglish;
    }

    public boolean isSupportCredit() {
        return mSupportCredit;
    }

    public void setSupportCredit(boolean mSupportCredit) {
        this.mSupportCredit = mSupportCredit;
    }

    public boolean isServiceProvider() {
        return isServiceProvider;
    }

    public void setIsServiceProvider(boolean isServiceProvider) {
        this.isServiceProvider = isServiceProvider;
    }

    public String getVehicleTypeArabic() {
        return mVehicleTypeArabic;
    }

    public void setVehicleTypeArabic(String mVehicleTypeArabic) {
        this.mVehicleTypeArabic = mVehicleTypeArabic;
    }

    public String getVehicleTypeEnglish() {
        return mVehicleTypeEnglish;
    }

    public void setVehicleTypeEnglish(String mVehicleTypeEnglish) {
        this.mVehicleTypeEnglish = mVehicleTypeEnglish;
    }
}
