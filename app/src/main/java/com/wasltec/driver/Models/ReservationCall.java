package com.wasltec.driver.Models;


import java.util.Date;

public class ReservationCall {
    //new HubReservationLaterCallViewModel { ConnectedClient = reservation.Client, EndLat = reservation.EndPlaceLat, EndLng = reservation.EndPlaceLng, EndPlace = reservation.EndPlace, EndDate = reservation.EndDate, StartDate = reservation.StartDate, StartLat = reservation.StartPlaceLat, StartLng = reservation.StartPlaceLng, StartPlace = reservation.StartPlace, PaymentType = (int)reservation.PaymentTypeId });

    private ClientViewModel mClientViewModel;
    private double mEndLat;
    private double mEndLng;
    private String mEndPlace;
    private Date mEndDate;
    private  Date mStartDate;
    private double mStartLat;
    private  double mStartLng;
    private String mStartPlace;
    private int mPaymentType;

    public ClientViewModel getClientViewModel() {
        return mClientViewModel;
    }

    public void setClientViewModel(ClientViewModel clientViewModel) {
        mClientViewModel = clientViewModel;
    }

    public double getEndLat() {
        return mEndLat;
    }

    public void setEndLat(double endLat) {
        mEndLat = endLat;
    }

    public double getEndLng() {
        return mEndLng;
    }

    public void setEndLng(double endLng) {
        mEndLng = endLng;
    }

    public String getEndPlace() {
        return mEndPlace;
    }

    public void setEndPlace(String endPlace) {
        mEndPlace = endPlace;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Date endDate) {
        mEndDate = endDate;
    }

    public Date getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Date startDate) {
        mStartDate = startDate;
    }

    public double getStartLat() {
        return mStartLat;
    }

    public void setStartLat(double startLat) {
        mStartLat = startLat;
    }

    public double getStartLng() {
        return mStartLng;
    }

    public void setStartLng(double startLng) {
        mStartLng = startLng;
    }

    public String getStartPlace() {
        return mStartPlace;
    }

    public void setStartPlace(String startPlace) {
        mStartPlace = startPlace;
    }

    public int getPaymentType() {
        return mPaymentType;
    }

    public void setPaymentType(int paymentType) {
        mPaymentType = paymentType;
    }
}
