package com.wasltec.driver.Models;

import com.google.android.gms.maps.model.Marker;


public class Client {
    public String UserName;
    public String Name;
    public String Number;
    public String Address;
    public double Lat;
    public double Lng;
    public String ClientNumber = "";
    public Marker clientMarker = null;
    public boolean showClientMarker = false;
    public int paymentType;
    public Client() {

    }
    public Client(String _userName, String _name, String _number, String _address, double _lat, double _lng,int _paymentType) {
        this.UserName = _userName;
        this.Number = _number;
        this.Name = _name;
        this.Address = _address;
        this.Lat = _lat;
        this.Lng = _lng;
        this.paymentType = _paymentType;
    }
}
