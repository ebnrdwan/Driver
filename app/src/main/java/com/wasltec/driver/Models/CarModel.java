package com.wasltec.driver.Models;


public class CarModel {
    public CarModel() {

    }

    public CarModel(int _id, String _name) {
        this.id = _id;
        this.Name = _name;
    }
    public CarModel(int _id, String _name,String _nameAr) {
        this.id = _id;
        this.Name = _name;
        this.NameAr = _nameAr;
    }
    public int id;
    public String Name;
    public String NameAr;
}
