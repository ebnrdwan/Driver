package com.wasltec.driver.Models;

import android.content.ContentValues;
import android.database.Cursor;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class Driver extends BaseModel implements IModel {
    public final static String TABLE_NAME = "mDriver";
    public static final String NAME_FIELD = "Name";
    public static final String PASSWORD_FIELD = "password";
    public static final String COUNTRY_ID_FIELD = "countryId";
    public static final String VEHICLE_TYPE_FIELD = "vehicleType";
    public static final String CREDIT_FIELD = "credit";
    public static final String ACCESS_TOKEN_FIELD = "accessToken";
    public static final String ACCESS_EXPIRES_FIELD = "accessExpires";
    public static final String ID_FIELD = "Id";

    public static final String SQL_CREATION = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COUNTRY_ID_FIELD + " INTEGER NOT NULL," + NAME_FIELD + " text NOT NULL," + PASSWORD_FIELD + " text NOT NULL," + VEHICLE_TYPE_FIELD + " NUMBER NOT NULL," + CREDIT_FIELD + "CREDIT NUMBER NOT NULL," + ACCESS_TOKEN_FIELD + " TEXT NOT NULL," + ACCESS_EXPIRES_FIELD + " NUMBER NOT NULL," + ID_FIELD + " INTEGER NOT NULL);";
    private String mCountry,mName,mPassword,mAccessToken;
    private int mCountryId,mCredit,mVehicleType,mId;
    private long mAccessExpires;
    @Override
    public String getIdField() {
        return ID_FIELD;
    }
    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public long getAccessExpires() {
        return mAccessExpires;
    }

    public void setAccessExpires(long accessExpires) {
        mAccessExpires = accessExpires;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public int getCredit() {
        return mCredit;
    }

    public void setCredit(int credit) {
        mCredit = credit;
    }

    public int getVehicleType() {
        return mVehicleType;
    }

    public void setVehicleType(int vehicleType) {
        mVehicleType = vehicleType;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getSqlCreation() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COUNTRY_ID_FIELD + " INTEGER NOT NULL," + NAME_FIELD + " text NOT NULL," + PASSWORD_FIELD + " text NOT NULL," + VEHICLE_TYPE_FIELD + " NUMBER NOT NULL," + CREDIT_FIELD + " NUMBER NOT NULL," + ACCESS_TOKEN_FIELD + " TEXT NOT NULL," + ACCESS_EXPIRES_FIELD + " NUMBER NOT NULL," + ID_FIELD + " INTEGER NOT NULL);";
    }

    @Override
    public String getClearSql() {
        return "DELETE FROM " + TABLE_NAME;
    }

    @Override
    public String getListSql() {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + "  ";
    }

    @Override
    public String getSingleSql(int id) {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + " WHERE  " + ID_FIELD + "= " + id;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }


    public int getId() {
        return mId;
    }

    public void setId(int nid) {
        mId = nid;
    }


    @Override
    public ArrayList<IModel> fromJson(JSONObject jsonObject) {
        ArrayList<IModel> result = new ArrayList<>();
        try {
            JSONArray array = jsonObject.getJSONArray("CityList");
            for (int i = 0; i < array.length(); i++) {
                JSONObject item = array.getJSONObject(i);
                Driver driver = new Driver();
                driver.setCountryId(item.getInt(COUNTRY_ID_FIELD));
                driver.setName(item.getString(NAME_FIELD));
                driver.setPassword(item.getString(PASSWORD_FIELD));
                driver.setCredit(item.getInt(CREDIT_FIELD));
                driver.setVehicleType(item.getInt(VEHICLE_TYPE_FIELD));
                driver.setId(item.getInt(ID_FIELD));
                driver.setAccessToken(item.getString(ACCESS_TOKEN_FIELD));
                driver.setAccessExpires(item.getLong(ACCESS_EXPIRES_FIELD));
                result.add(driver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public IModel fromCursor(Cursor cursor) {
        setCountryId(cursor.getInt((cursor.getColumnIndex(COUNTRY_ID_FIELD))));
        setName(cursor.getString((cursor.getColumnIndex(NAME_FIELD))));
        setPassword(cursor.getString((cursor.getColumnIndex(PASSWORD_FIELD))));
        setCredit(cursor.getInt((cursor.getColumnIndex(CREDIT_FIELD))));
        setVehicleType(cursor.getInt(cursor.getColumnIndex(VEHICLE_TYPE_FIELD)));
        setId(cursor.getInt((cursor.getColumnIndex(ID_FIELD))));
        setAccessToken(cursor.getString(cursor.getColumnIndex(ACCESS_TOKEN_FIELD)));
        setAccessExpires(cursor.getLong(cursor.getColumnIndex(ACCESS_EXPIRES_FIELD)));
        return this;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(COUNTRY_ID_FIELD, getCountryId());
        values.put(NAME_FIELD, getName());
        values.put(PASSWORD_FIELD, getPassword());
        values.put(CREDIT_FIELD, getCredit());
        values.put(VEHICLE_TYPE_FIELD, getVehicleType());
        values.put(ID_FIELD, getId());
        values.put(ACCESS_TOKEN_FIELD, getAccessToken());
        values.put(ACCESS_EXPIRES_FIELD, getAccessExpires());
        return values;
    }
}
