package com.wasltec.driver.Models;

public class BalanceInfo {
    public double tripsCost;
    public int tripsCount;
    public BalanceInfo(double tripsCost,int tripsCount){
        this.tripsCost = tripsCost;
        this.tripsCount = tripsCount;
    }
}
