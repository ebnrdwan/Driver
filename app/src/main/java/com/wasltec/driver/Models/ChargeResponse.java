package com.wasltec.driver.Models;


import com.wasltec.driver.Helpers.Service.ServiceResult;

import org.json.JSONObject;

public class ChargeResponse {
    private boolean mSuccess;
    private String mEnglishMessage;
    private String mArabicMessage;
    private int mNewCredit;

    public ChargeResponse(ServiceResult result) {
        setSuccess(result.getSuccess());
        if (isSuccess()) {
            try {
                JSONObject object = new JSONObject(result.getResult());
                setSuccess(object.getBoolean("Success"));
                setEnglishMessage(object.getString("EnglishMessage"));
                setArabicMessage(object.getString("ArabicMessage"));
                setNewCredit(object.getInt("CurrentCredit"));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public void setSuccess(boolean success) {
        mSuccess = success;
    }

    public int getNewCredit() {
        return mNewCredit;
    }

    public void setNewCredit(int newCredit) {
        mNewCredit = newCredit;
    }

    public String getEnglishMessage() {
        return mEnglishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        mEnglishMessage = englishMessage;
    }

    public String getArabicMessage() {
        return mArabicMessage;
    }

    public void setArabicMessage(String arabicMessage) {
        mArabicMessage = arabicMessage;
    }
}
