package com.wasltec.driver.Models;


public class Vehicle {
    public Vehicle() {

    }

    public Vehicle(int _id, String _name) {
        this.id = _id;
        this.Name = _name;
    }

    public int id;
    public String Name;
}
