package com.wasltec.driver.Models;

import org.json.JSONException;
import org.json.JSONObject;
public class Coordinates {
    public double Lat;
    public double Lon;
public Coordinates(){
    this.Lat = 0;
    this.Lon = 0;
}
    public Coordinates(double latitude, double longitude)  {
        this.Lat = latitude;
        this.Lon = longitude;
    }
    public double getLatitude() {
        return Lat;
    }

    public double getLongitude() {
        return Lon;
    }

    public void setLatitude(double latitude) {
        this.Lat = latitude;
    }

    public void setLongitude(double longitude) {
        this.Lon = longitude;
    }
    public JSONObject getJson(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Lat",getLatitude());
            jsonObject.put("Lon",getLongitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
    @Override
    public String toString() {
        return "{Lat:"+getLatitude()+",Lon:"+getLongitude()+"}";
    }
}
