package com.wasltec.driver.Models;

import android.content.Context;

import com.wasltec.driver.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HistoryTrip {

    public int id;
    public int ClientId;
    public Date startTime;
    public Date endTime;
    public double cost;
    public double distance;
    public ArrayList<TripCoordinates> tripCoordinates;
    private SimpleDateFormat formatter;

    public HistoryTrip() {
        tripCoordinates = new ArrayList<>();
        formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    }

    public String getLabeledStartDate(Context context) {

        return String.format("%s : %s", context.getResources().getString(R.string.start_date), formatter.format(this.startTime));
    }

    public String getLabeledEndDate(Context context) {

        return String.format("%s : %s", context.getResources().getString(R.string.end_date), formatter.format(this.endTime));
    }

    public String getLabeledDistance(Context context) {

        return String.format("%s : %.2f KM", context.getResources().getString(R.string.distance), this.distance);
    }

    public String getLabeledCost(Context context) {

        return String.format("%s : %.2f", context.getResources().getString(R.string.cost), this.cost);
    }

    public String getStartDate(Context context) {

        return String.format("%s", formatter.format(this.startTime));
    }

    public String getEndDate(Context context) {

        return String.format("%s", formatter.format(this.endTime));
    }

    public String getDistance(Context context) {

        return String.format("%.2f KM", this.distance);
    }

    public String getCost(Context context) {

        return String.format("%.2f", this.cost);
    }




}
