package com.wasltec.driver.Models;

import android.content.Context;

import com.wasltec.driver.Database.DatabaseHandler;

public class Info implements DataModel {
    public int id;
    public String name = "";
    public String code = "";
    public double openDoorPrice=0;
    public double kiloMeterCost = 0;

    @Override
    public void Save(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.updateInfo(this);
    }

    @Override
    public void getById(Context context, int id) {

    }
}
