package com.wasltec.driver.Models;


import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

import java.util.ArrayList;

public interface IModel {
    String getTableName();
    String getSqlCreation();
    String getClearSql();
    String getListSql();
    String getSingleSql(int id);
    int getId();
    String getIdField();


    ArrayList<IModel> fromJson(JSONObject jsonObject);

    IModel fromCursor(Cursor cursor);
    ContentValues toContentValues();

}
