package com.wasltec.driver.Models;

public class Trip {
    public Trip() {

    }

    public Trip(int tripId) {
        setId(tripId);
    }

    public Trip(int Id, double cost,double waitPerMinuteCost) {
        setId(Id);
        setCost(cost);
        setWaitPerMinuteCost(waitPerMinuteCost);
    }
    private double waitPerMinuteCost ;
    private long waitSeconds ;
    private int stopTime ;

    private int mId;
    private double mCost;
    private long mMeterDistance;
    private float mKMDistance;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public double getCost() {
        return mCost;
    }

    public void setCost(double cost) {
        mCost = cost;
    }

    public long getMeterDistance() {
        return mMeterDistance;
    }

    public void setMeterDistance(long meterDistance) {
        mMeterDistance = meterDistance;
    }

    public float getKMDistance() {
        return mKMDistance;
    }

    public void setKMDistance(float KMDistance) {
        mKMDistance = KMDistance;
    }

    public double getWaitPerMinuteCost() {
        return waitPerMinuteCost;
    }

    public void setWaitPerMinuteCost(double waitPerMinuteCost) {
        this.waitPerMinuteCost = waitPerMinuteCost;
    }

    public long getWaitSeconds() {
        return waitSeconds;
    }

    public void setWaitSeconds(long waitSeconds) {
        this.waitSeconds = waitSeconds;
    }

    public int getStopTime() {
        return stopTime;
    }

    public void setStopTime(int stopTime) {
        this.stopTime = stopTime;
    }
}
