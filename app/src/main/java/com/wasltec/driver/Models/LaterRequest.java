package com.wasltec.driver.Models;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;
import com.wasltec.driver.Helpers.DateHelper;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Date;

public class LaterRequest implements IModel {
    private String mClientName, mClientNumber, mStartPlace, mEndPlace;
    private double mStartDate, mStartLat, mStartLng, mEndLat, mEndLng, mCost, mDistance, mEndDate;
    private int mId,mPaymentType,mCallID;
    private boolean mStarted, mMissed, mEnded, mApproved;

    public LaterRequest() {
    }

    public LaterRequest(int reservationId, String clientName, String clientNumber, String startDate, double endLat, double endLng, String endPlace, String startPlace, int paymentType, double startLat, double startLng,int callID) {
        setClientName(clientName);
        setClientNumber(clientNumber);
        setStartDate(DateHelper.parse(startDate).getTime());
        setEndLat(endLat);
        setEndLng(endLng);
        setEndPlace(endPlace);
        setStartPlace(startPlace);
        setStartLat(startLat);
        setStartLng(startLng);
        setId(reservationId);
        setApproved(false);
        setMissed(false);
        setStarted(false);
        setEnded(false);
        setPaymentType(paymentType);
        setCallID(callID);
    }

    private final String TABLE_NAME = "laterRequest";
    private final String ID_KEY = "id";
    private final String CLIENT_NAME_KEY = "clientName";
    private final String CLIENT_NUMBER_KEY = "clientNumber";
    private final String START_PLACE_KEY = "startPlace";
    private final String END_PLACE_KEY = "endPlace";
    private final String START_DATE_KEY = "startDate";
    private final String END_DATE_KEY = "endDate";
    private final String START_LAT_KEY = "startLat";
    private final String END_LAT_KEY = "endLat";
    private final String START_LNG_KEY = "startLng";
    private final String END_LNG_KEY = "endLng";
    private final String STARTED_KEY = "started";
    private final String MISSED_KEY = "missed";
    private final String ENDED_KEY = "ended";
    private final String COST_KEY = "cost";
    private final String DISTANCE_KEY = "distance";
    private final String APPROVED_KEY = "approved";
    private final String CALL_ID_KEY = "callId";

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getSqlCreation() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + ID_KEY + " INTEGER NOT NULL," + CLIENT_NAME_KEY + " text NOT NULL," + CLIENT_NUMBER_KEY + " text NOT NULL," + START_PLACE_KEY + " TEXT NOT NULL," + END_PLACE_KEY + " TEXT NOT NULL," + START_DATE_KEY + " NUMBER NOT NULL," + END_DATE_KEY + " NUMBER NOT NULL," + START_LAT_KEY + " NUMBER NOT NULL," + END_LAT_KEY + " NUMBER NOT NULL," + START_LNG_KEY + " NUMBER NOT NULL, " + END_LNG_KEY + " TEXT NOT NULL, " + STARTED_KEY + " NUMBER NOT NULL, " + MISSED_KEY + " NUMBER NOT NULL, " + ENDED_KEY + " NUMBER NOT NULL, " + COST_KEY + " NUMBER NOT NULL," + DISTANCE_KEY + " NUMBER NOT NULL, " + CALL_ID_KEY + " NUMBER NOT NULL, "+ APPROVED_KEY + " NUMBER NOT NULL);";
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClientName() + "\n");
        sb.append(getClientNumber() + "\n");
        sb.append(DateHelper.formatDate(new Date((long) getStartDate())) + "\n");
        sb.append(getStartPlace() + "\n");
        return sb.toString();
    }

    @Override
    public String getClearSql() {
        return "DELETE FROM " + TABLE_NAME;
    }

    @Override
    public String getListSql() {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + " WHERE " + APPROVED_KEY + " = 1  ORDER BY " + ID_KEY + " desc";
    }

    @Override
    public String getSingleSql(int id) {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + " WHERE  " + ID_KEY + "= " + id;
    }

    public boolean isApproved() {
        return mApproved;
    }

    public void setApproved(boolean approved) {
        mApproved = approved;
    }

    public double getEndDate() {
        return mEndDate;
    }

    public void setEndDate(double endDate) {
        mEndDate = endDate;
    }

    @Override
    public ArrayList<IModel> fromJson(JSONObject jsonObject) {
        return null;
    }

    public double getCost() {
        return mCost;
    }

    public void setCost(double cost) {
        mCost = cost;
    }

    public double getDistance() {
        return mDistance;
    }

    public void setDistance(double distance) {
        mDistance = distance;
    }

    public String getClientName() {
        return mClientName;
    }

    public void setClientName(String clientName) {
        mClientName = clientName;
    }

    public String getClientNumber() {
        return mClientNumber;
    }

    public void setClientNumber(String clientNumber) {
        mClientNumber = clientNumber;
    }

    public String getStartPlace() {
        return mStartPlace;
    }

    public void setStartPlace(String startPlace) {
        mStartPlace = startPlace;
    }

    public String getEndPlace() {
        return mEndPlace;
    }

    public void setEndPlace(String endPlace) {
        mEndPlace = endPlace;
    }

    public double getStartDate() {
        return mStartDate;
    }

    public void setStartDate(double startDate) {
        mStartDate = startDate;
    }

    public double getStartLat() {
        return mStartLat;
    }

    public void setStartLat(double startLat) {
        mStartLat = startLat;
    }

    public double getStartLng() {
        return mStartLng;
    }

    public void setStartLng(double startLng) {
        mStartLng = startLng;
    }

    public double getEndLat() {
        return mEndLat;
    }

    public void setEndLat(double endLat) {
        mEndLat = endLat;
    }

    public double getEndLng() {
        return mEndLng;
    }

    public void setEndLng(double endLng) {
        mEndLng = endLng;
    }

    public void setId(int id) {
        mId = id;
    }

    public boolean isStarted() {
        return mStarted;
    }

    public void setStarted(boolean started) {
        mStarted = started;
    }

    public boolean isMissed() {
        return mMissed;
    }

    public void setMissed(boolean missed) {
        mMissed = missed;
    }

    public boolean isEnded() {
        return mEnded;
    }

    public void setEnded(boolean ended) {
        mEnded = ended;
    }

    @Override
    public IModel fromCursor(Cursor cursor) {
        LaterRequest request = new LaterRequest();
        request.setId(cursor.getInt(cursor.getColumnIndex(ID_KEY)));
        request.setClientName(cursor.getString((cursor.getColumnIndex(CLIENT_NAME_KEY))));
        request.setClientNumber(cursor.getString((cursor.getColumnIndex(CLIENT_NUMBER_KEY))));
        request.setStartPlace(cursor.getString((cursor.getColumnIndex(START_PLACE_KEY))));
        request.setEndPlace(cursor.getString((cursor.getColumnIndex(END_PLACE_KEY))));
        request.setStartDate(cursor.getLong(cursor.getColumnIndex(START_DATE_KEY)));
        request.setEndDate(cursor.getLong(cursor.getColumnIndex(END_DATE_KEY)));
        request.setStartLat(cursor.getDouble(cursor.getColumnIndex(START_LAT_KEY)));
        request.setStartLng(cursor.getDouble(cursor.getColumnIndex(START_LNG_KEY)));
        request.setEndLat(cursor.getDouble(cursor.getColumnIndex(END_LAT_KEY)));
        request.setEndLng(cursor.getDouble(cursor.getColumnIndex(END_LNG_KEY)));
        request.setCost(cursor.getDouble(cursor.getColumnIndex(COST_KEY)));
        request.setDistance(cursor.getDouble(cursor.getColumnIndex(DISTANCE_KEY)));
        request.setStarted(cursor.getInt(cursor.getColumnIndex(STARTED_KEY)) == 1);
        Log.v("missed", cursor.getInt(cursor.getColumnIndex(MISSED_KEY)) + "");
        request.setMissed(cursor.getInt(cursor.getColumnIndex(MISSED_KEY)) == 1);
        request.setEnded(cursor.getInt(cursor.getColumnIndex(ENDED_KEY)) == 1);
        request.setApproved(cursor.getInt(cursor.getColumnIndex(APPROVED_KEY)) == 1);
        request.setCallID(cursor.getInt(cursor.getColumnIndex(CALL_ID_KEY)));
        return request;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(ID_KEY, getId());
        values.put(CLIENT_NAME_KEY, getClientName());
        values.put(CLIENT_NUMBER_KEY, getClientNumber());
        values.put(START_PLACE_KEY, getStartPlace());
        values.put(END_PLACE_KEY, getEndPlace());
        values.put(START_DATE_KEY, getStartDate());
        values.put(END_DATE_KEY, getEndDate());
        values.put(START_LAT_KEY, getStartLat());
        values.put(START_LNG_KEY, getStartLng());
        values.put(END_LAT_KEY, getEndLat());
        values.put(END_LNG_KEY, getEndLng());
        values.put(COST_KEY, getCost());
        values.put(DISTANCE_KEY, getDistance());
        values.put(STARTED_KEY, isStarted());
        values.put(MISSED_KEY, isMissed());
        values.put(ENDED_KEY, isEnded());
        values.put(APPROVED_KEY, isApproved());
        values.put(CALL_ID_KEY,getCallID());
        return values;
    }

    public int getId() {
        return mId;
    }

    @Override
    public String getIdField() {
        return ID_KEY;
    }

    public int getPaymentType() {
        return mPaymentType;
    }

    public void setPaymentType(int mPaymentType) {
        this.mPaymentType = mPaymentType;
    }

    public int getCallID() {
        return mCallID;
    }

    public void setCallID(int callID) {
        this.mCallID = callID;
    }
}