package com.wasltec.driver.Models;

import android.content.Context;

public interface DataModel {
     void Save(Context context);
    void getById(Context context, int id);
}
