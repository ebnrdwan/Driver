package com.wasltec.driver.Models;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class City extends BaseModel implements IModel {
    public final static String TABLE_NAME = "mCity";
    public static final String COUNTRY_FIELD = "mCountry";
    public static final String COUNTRY_ID_FIELD = "CountryId";
    public static final String ENGLISH_NAME_FIELD = "EnglishName";
    public static final String ARABIC_NAME_FIELD = "ArabicName";
    public static final String ID_FIELD = "Id";

    public static final String SQL_CREATION = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COUNTRY_ID_FIELD + " INTEGER NOT NULL," + ENGLISH_NAME_FIELD + " text NOT NULL," + ARABIC_NAME_FIELD + " text NOT NULL," + ID_FIELD + " INTEGER NOT NULL);";
    private String mCountry;
    private int mCountryId;
    private String mEnglishName;
    private String mArabicName;
    private int mId;
    @Override
    public String getIdField() {
        return ID_FIELD;
    }
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getSqlCreation() {
        return  "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + COUNTRY_ID_FIELD + " INTEGER NOT NULL," + ENGLISH_NAME_FIELD + " text NOT NULL," + ARABIC_NAME_FIELD + " text NOT NULL," + ID_FIELD + " INTEGER NOT NULL);";
    }

    @Override
    public String getClearSql() {
        return "DELETE FROM " + TABLE_NAME;
    }

    @Override
    public String getListSql() {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + "  ";
    }

    @Override
    public String getSingleSql(int id) {
        return "SELECT " + TABLE_NAME + ".* FROM " + TABLE_NAME + " WHERE  " + ID_FIELD + "= " + id;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }

    public String getEnglishName() {
        return mEnglishName;
    }

    public void setEnglishName(String englishName) {
        mEnglishName = englishName;
    }

    public String getArabicName() {
        return mArabicName;
    }

    public void setArabicName(String arabicName) {
        mArabicName = arabicName;
    }

    public int getId() {
        return mId;
    }

    public void setId(int nid) {
        mId = nid;
    }



    @Override
    public ArrayList<IModel> fromJson(JSONObject jsonObject) {
        ArrayList<IModel> result = new ArrayList<>();
        try {
            JSONArray array = jsonObject.getJSONArray("CityList");
            for (int i = 0; i < array.length(); i++) {
                JSONObject item = array.getJSONObject(i);
                City city = new City();
                city.setCountryId(item.getInt(COUNTRY_ID_FIELD));
                city.setEnglishName(item.getString(ENGLISH_NAME_FIELD));
                city.setArabicName(item.getString(ARABIC_NAME_FIELD));
                city.setId(item.getInt(ID_FIELD));

                result.add(city);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public IModel fromCursor(Cursor cursor) {
        this.setCountry(cursor.getString((cursor.getColumnIndex(COUNTRY_FIELD))));
        this.setCountryId(cursor.getInt((cursor.getColumnIndex(COUNTRY_ID_FIELD))));
        this.setEnglishName(cursor.getString((cursor.getColumnIndex(ENGLISH_NAME_FIELD))));
        this.setArabicName(cursor.getString((cursor.getColumnIndex(ARABIC_NAME_FIELD))));
        this.setId(cursor.getInt((cursor.getColumnIndex(ID_FIELD))));
        return this;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(COUNTRY_ID_FIELD, this.getCountryId());
        values.put(ENGLISH_NAME_FIELD, this.getEnglishName());
        values.put(ARABIC_NAME_FIELD, this.getArabicName());
        values.put(ID_FIELD, this.getId());
        return values;
    }
}
