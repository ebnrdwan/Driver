package com.wasltec.driver.Models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONObject;
import java.util.ArrayList;

public class Balance implements IModel {
    public final String TABLE_NAME = "Balance";
    public final String TRIP_ID = "tripId";
    public final String TRIP_DATE = "tripDate";
    public final String TRIP_ADDRESS = "tripAddress";
    public final String TRIP_STATUS = "tripStatus";
    public final String TOTAL_AMOUNT = "totalAmount";
    public final String PAIED_AMOUNT = "paiedAmount";
    public final String SUBSTRACTED_AMOUNT = "substractedAmount";
    public final String PROMOTION_AMOUNT = "promotionAmount";
    public final static int TRIP_STARTED_STATUS = 1;
    public final static int TRIP_ENDED_STATUS = 2;

    public boolean insertTripSummary(SQLiteDatabase db, TripSummary mTrip) {
        ContentValues values = new ContentValues();
        values.put(TRIP_ID, mTrip.TRIP_ID);
        values.put(TRIP_DATE, mTrip.TRIP_DATE);
        values.put(TRIP_ADDRESS, mTrip.TRIP_ADDRESS);
        values.put(TRIP_STATUS, mTrip.TRIP_STATUS);
        values.put(TOTAL_AMOUNT, mTrip.TOTAL_AMOUNT);
        values.put(PAIED_AMOUNT, mTrip.PAIED_AMOUNT);
        values.put(SUBSTRACTED_AMOUNT, mTrip.SUBSTRACTED_AMOUNT);
        values.put(PROMOTION_AMOUNT, mTrip.PROMOTION_AMOUNT);
        try {
            db.insert(TABLE_NAME, null, values);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateTripSummary(SQLiteDatabase db, TripSummary mTrip) {
        ContentValues values = new ContentValues();
        values.put(TRIP_ID, mTrip.TRIP_ID);
        values.put(TRIP_DATE, mTrip.TRIP_DATE);
        values.put(TRIP_ADDRESS, mTrip.TRIP_ADDRESS);
        values.put(TRIP_STATUS, mTrip.TRIP_STATUS);
        values.put(TOTAL_AMOUNT, mTrip.TOTAL_AMOUNT);
        values.put(PAIED_AMOUNT, mTrip.PAIED_AMOUNT);
        values.put(SUBSTRACTED_AMOUNT, mTrip.SUBSTRACTED_AMOUNT);
        values.put(PROMOTION_AMOUNT, mTrip.PROMOTION_AMOUNT);
        try {
            db.update(TABLE_NAME, values, TRIP_ID + " = " + mTrip.TRIP_ID, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public double getBalance(SQLiteDatabase db) {
        String query = "SELECT  * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() == 0)
            return 0;
        double totalBalance = 0;
        if (cursor.moveToFirst()) {
            do {
                totalBalance += cursor.getDouble(cursor.getColumnIndex(PROMOTION_AMOUNT)) - cursor.getDouble(cursor.getColumnIndex(SUBSTRACTED_AMOUNT));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return totalBalance;
    }

    public BalanceInfo getTripsCostAndCount(SQLiteDatabase db) {
        String query = "SELECT  * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() == 0)
            return null;
        double totalTripsCost = 0;
        int totalTripsCount = 0;
        if (cursor.moveToFirst()) {
            do {
                totalTripsCost += cursor.getDouble(cursor.getColumnIndex(TOTAL_AMOUNT));
                totalTripsCount++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return new BalanceInfo(totalTripsCost, totalTripsCount);
    }

    public ArrayList<TripSummary> getAllTripsummaries(SQLiteDatabase db) {
        String query = "SELECT  * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() == 0)
            return null;
        ArrayList<TripSummary> mTripSummaryList = new ArrayList<>();
        TripSummary mTripSummaryObj;
        if (cursor.moveToFirst()) {
            do {
                mTripSummaryObj = new TripSummary();
                mTripSummaryObj.TRIP_ID = cursor.getInt(cursor.getColumnIndex(TRIP_ID));
                mTripSummaryObj.TRIP_DATE = cursor.getLong(cursor.getColumnIndex(TRIP_DATE));
                mTripSummaryObj.TRIP_ADDRESS = cursor.getString(cursor.getColumnIndex(TRIP_ADDRESS));
                mTripSummaryObj.TRIP_STATUS = cursor.getInt(cursor.getColumnIndex(TRIP_STATUS));
                mTripSummaryObj.TOTAL_AMOUNT = cursor.getDouble(cursor.getColumnIndex(TOTAL_AMOUNT));
                mTripSummaryObj.PAIED_AMOUNT = cursor.getDouble(cursor.getColumnIndex(PAIED_AMOUNT));
                mTripSummaryObj.SUBSTRACTED_AMOUNT = cursor.getDouble(cursor.getColumnIndex(SUBSTRACTED_AMOUNT));
                mTripSummaryObj.PROMOTION_AMOUNT = cursor.getDouble(cursor.getColumnIndex(PROMOTION_AMOUNT));
                mTripSummaryList.add(mTripSummaryObj);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return mTripSummaryList;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getSqlCreation() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                "(" + TRIP_ID + " INTEGER NOT NULL," +
                TRIP_DATE + " NUMERIC NOT NULL," +
                TRIP_ADDRESS + " TEXT NOT NULL," +
                TRIP_STATUS + " TEXT NOT NULL," +
                TOTAL_AMOUNT + " REAL NOT NULL," +
                PAIED_AMOUNT + " REAL NOT NULL," +
                SUBSTRACTED_AMOUNT + " REAL NOT NULL," +
                PROMOTION_AMOUNT + " REAL NOT NULL);";
    }

    @Override
    public String getClearSql() {
        return null;
    }

    @Override
    public String getListSql() {
        return null;
    }

    @Override
    public String getSingleSql(int id) {
        return null;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public String getIdField() {
        return null;
    }

    @Override
    public ArrayList<IModel> fromJson(JSONObject jsonObject) {
        return null;
    }

    @Override
    public IModel fromCursor(Cursor cursor) {
        return null;
    }

    @Override
    public ContentValues toContentValues() {
        return null;
    }
}