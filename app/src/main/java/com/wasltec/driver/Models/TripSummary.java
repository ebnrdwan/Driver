package com.wasltec.driver.Models;

public class TripSummary {
    public  int TRIP_ID ;
    public  long TRIP_DATE ;
    public  String TRIP_ADDRESS ;
    public  int TRIP_STATUS ;
    public  double TOTAL_AMOUNT ;
    public  double PAIED_AMOUNT ;
    public  double SUBSTRACTED_AMOUNT ;
    public  double PROMOTION_AMOUNT ;

    public TripSummary(){
    }

    public TripSummary(  int TRIP_ID ,
            long TRIP_DATE ,
            String TRIP_ADDRESS ,
            int TRIP_STATUS ,
            double TOTAL_AMOUNT ,
            double PAIED_AMOUNT ,
            double SUBSTRACTED_AMOUNT ,
            double PROMOTION_AMOUNT ){

        this.TRIP_ID=TRIP_ID;
        this.TRIP_DATE=TRIP_DATE ;
        this.TRIP_ADDRESS=TRIP_ADDRESS ;
        this.TRIP_STATUS=TRIP_STATUS ;
        this.TOTAL_AMOUNT =TOTAL_AMOUNT;
        this.PAIED_AMOUNT =PAIED_AMOUNT;
        this.SUBSTRACTED_AMOUNT =SUBSTRACTED_AMOUNT;
        this.PROMOTION_AMOUNT=PROMOTION_AMOUNT ;
    }

}
