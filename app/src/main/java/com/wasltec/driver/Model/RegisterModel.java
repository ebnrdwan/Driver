package com.wasltec.driver.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abdulrhman on 19/10/2017.
 */

public class RegisterModel {
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Code")
    private int Code;
    @SerializedName("Message")
    private String Message;
    @SerializedName("ArabicMessage")
    private String ArabicMessage;
    @SerializedName("Activationcode")
    private String Activationcode;

    public boolean getSuccess() {
        return Success;
    }

    public int getCode() {
        return Code;
    }

    public String getMessage() {
        return Message;
    }

    public String getArabicMessage() {
        return ArabicMessage;
    }

    public String getActivationcode() {
        return Activationcode;
    }
}
