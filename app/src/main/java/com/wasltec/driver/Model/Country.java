package com.wasltec.driver.Model;

public class Country {
    public Country() {

    }
    public Country(int _id, String _name) {
        this.id = _id;
        this.Name = _name;
    }
    public Country(int _id, String _name,String _nameAr) {
        this.id = _id;
        this.Name = _name;
        this.NameAr = _nameAr;
    }
    public int id;
    public String Name;
    public String Code;
    public String NameAr;

    public int getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

    public String getCode() {
        return Code;
    }

    public String getNameAr() {
        return NameAr;
    }
}
