package com.wasltec.driver.Model;

public class City {

    public City() {

    }
    public City(int countryId,int _id, String _name) {
        this.id = _id;
        this.Name = _name;
        this.countryId=countryId;
    }
    public City(int countryId,int _id, String _name,String _nameAr) {
        this.id = _id;
        this.Name = _name;
        this.NameAr = _nameAr;
        this.countryId=countryId;
    }
    public int countryId;
    public int id;
    public String Name;
    public String NameAr;
}
