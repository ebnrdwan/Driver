package com.wasltec.driver.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.wasltec.driver.R;

public class BalanceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    IClickListener mClickListener;
    TextView tvDate, tvId, tvCost, tvAddress;
    public BalanceViewHolder(View itemView, IClickListener listener) {
        super(itemView);
        mClickListener = listener;
        tvId = (TextView) itemView.findViewById(R.id.tvId);
        tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
        tvCost = (TextView) itemView.findViewById(R.id.tvCost);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mClickListener != null)
            mClickListener.itemClicked(getAdapterPosition());
    }
}