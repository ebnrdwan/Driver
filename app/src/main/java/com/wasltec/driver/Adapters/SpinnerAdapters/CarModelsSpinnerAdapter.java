package com.wasltec.driver.Adapters.SpinnerAdapters;

import android.app.Activity;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.wasltec.driver.WebService.ViewModels.CarModel;

import java.util.ArrayList;

public class CarModelsSpinnerAdapter implements SpinnerAdapter, ISpinnerAdapter {
    private ArrayList<CarModel> mData;
    private Activity mContext;

    public CarModelsSpinnerAdapter(ArrayList<CarModel> data,Activity context)
    {
       this.mData =data;
        mContext = context;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = mContext.getLayoutInflater().inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        ((CheckedTextView) view.findViewById(android.R.id.text1)).setText(mData.get(position).getEnglishName());
        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        if (mData != null) return mData.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mData != null) return mData.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (mData != null) return mData.get(position).getId();
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mContext.getLayoutInflater().inflate(android.R.layout.simple_spinner_item, parent, false);
        ((TextView) view.findViewById(android.R.id.text1)).setText(mData.get(position).getEnglishName());
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return mData == null;
    }

    @Override
    public int getPositionForId(int id) {
        int position = 0;
        if (this.mData != null) {
            for (int i = 0; i < mData.size(); i++) {
                if (mData.get(i).getId() == id) {
                    position = i;
                    break;
                }
            }
        }
        return position;
    }

    @Override
    public int getIdForPosition(int position) {
        if (mData != null && mData.size() >= position&&mData.size()>0)
            return mData.get(position).getId();
        return 0;
    }
}
