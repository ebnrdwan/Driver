package com.wasltec.driver.Adapters;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wasltec.driver.R;

public class Custom_menu_Adapter extends BaseAdapter implements View.OnClickListener {
    private static LayoutInflater inflater = null;
    String[] result;
    Context context;

    public Custom_menu_Adapter(Context mainActivity, String[] prgmNameList) {
        // TODO Auto-generated constructor stub
        result = prgmNameList;
        context = mainActivity;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View rowView = convertView;
        if (convertView == null)
            rowView = inflater.inflate(R.layout.item_menu_view, null);

        TextView tv = (TextView) rowView.findViewById(R.id.tv_item);
        tv.setText(result[position]);

        return rowView;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        // Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_LONG).show();
    }
}


