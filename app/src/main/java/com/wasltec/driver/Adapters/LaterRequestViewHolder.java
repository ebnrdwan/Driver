package com.wasltec.driver.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wasltec.driver.R;

public class LaterRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    IClickListener mClickListener;
    TextView tvClientName, tvStartDate, tvStartPlace, tvState;
    Button btnStart;

    public LaterRequestViewHolder(View itemView, IClickListener listener) {
        super(itemView);
        mClickListener = listener;
        tvClientName = (TextView) itemView.findViewById(R.id.tvClientName);
        tvStartDate = (TextView) itemView.findViewById(R.id.tvStartDate);
        tvStartPlace = (TextView) itemView.findViewById(R.id.tvStartPlace);
        tvState = (TextView) itemView.findViewById(R.id.tvState);
        btnStart = (Button) itemView.findViewById(R.id.btnStartReservation);
        btnStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mClickListener != null)
            mClickListener.itemClicked(getAdapterPosition());
    }
}
