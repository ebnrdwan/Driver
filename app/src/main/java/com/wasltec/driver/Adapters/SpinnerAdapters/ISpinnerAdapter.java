package com.wasltec.driver.Adapters.SpinnerAdapters;


public interface ISpinnerAdapter {
    int getPositionForId(int id);
    int getIdForPosition(int position);
}
