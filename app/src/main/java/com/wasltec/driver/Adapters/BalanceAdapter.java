package com.wasltec.driver.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wasltec.driver.Helpers.DateHelper;
import com.wasltec.driver.Models.TripSummary;
import com.wasltec.driver.R;

import java.util.ArrayList;
import java.util.Date;

public class BalanceAdapter extends RecyclerView.Adapter<BalanceViewHolder>  {
    private ArrayList<TripSummary> mData;
    String mCurrency;
    IClickListener mClickListener;
    public BalanceAdapter(ArrayList<TripSummary> data,IClickListener listener,String currency){
        mData=data;
        mClickListener = listener;
        mCurrency = currency;
    }
    @Override
    public BalanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_balance, parent, false);
        return new BalanceViewHolder( view, mClickListener);
    }

    @Override
    public void onBindViewHolder(BalanceViewHolder holder, int position) {
        TripSummary item = mData.get(position);
        if(item != null){
            holder.tvId.setText(item.TRIP_ID+"");
            holder.tvDate.setText(DateHelper.formatDateTime(new Date(item.TRIP_DATE)) );
            holder.tvAddress.setText(item.TRIP_ADDRESS);
            holder.tvCost.setText(mCurrency+item.TOTAL_AMOUNT);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
