package com.wasltec.driver.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wasltec.driver.Models.HistoryTrip;
import com.wasltec.driver.R;

import java.util.ArrayList;

public class TripAdapter extends ArrayAdapter<HistoryTrip> {

    ArrayList<HistoryTrip> tripList;
    Context context;
    LayoutInflater inflater;

    public TripAdapter(Context context, ArrayList<HistoryTrip> tripList) {
        super(context, R.layout.row_trip);
        this.context = context;
        this.tripList = tripList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HistoryTrip trip = this.tripList.get(position);
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.row_trip, parent, false);
        }
        TextView tvTripStartDate = (TextView) view.findViewById(R.id.tvTripStartDate);
        TextView tvTripEndDate = (TextView) view.findViewById(R.id.tvTripEndDate);
        TextView tvTripDistance = (TextView) view.findViewById(R.id.tvTripDistance);
        TextView tvTripCost = (TextView) view.findViewById(R.id.tvTripCost);
        String[] StartDatetime = trip.getStartDate(context).split(" ");
        String[] endDatetime = trip.getEndDate(context).split(" ");
        String startdate = StartDatetime[0];
        String enddate = endDatetime[0];
        tvTripStartDate.setText(startdate);
        tvTripEndDate.setText(enddate);
        tvTripDistance.setText(trip.getDistance(context));
        tvTripCost.setText(trip.getCost(context));
        return view;
    }

    @Override
    public int getCount() {
        return this.tripList.size();
    }
}