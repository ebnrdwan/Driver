package com.wasltec.driver.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.wasltec.driver.Helpers.DateHelper;
import com.wasltec.driver.Models.LaterRequest;
import com.wasltec.driver.R;
import java.util.ArrayList;
import java.util.Date;

public class LaterRequestsAdapter extends RecyclerView.Adapter<LaterRequestViewHolder> {
    private ArrayList<LaterRequest> mData;
    IClickListener mClickListener;

    public LaterRequestsAdapter(ArrayList<LaterRequest> data, IClickListener listener) {
        mData = data;
        mClickListener = listener;
    }

    @Override
    public LaterRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_later_request, parent, false);

        return new LaterRequestViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(LaterRequestViewHolder holder, int position) {
        LaterRequest item = mData.get(position);
        if (item != null) {
            holder.tvClientName.setText(item.getClientName());
            holder.tvStartPlace.setText(item.getStartPlace());
            holder.tvStartDate.setText(DateHelper.formatDateTime(new Date((long) item.getStartDate())));
            String state = "Pending";
            if (item.isMissed()) state = "Missed";
            if (item.isStarted()) state = "Started";
            if (item.isEnded()) state = "Done";
            if (item.isMissed() || item.isStarted() || item.isEnded())
                holder.btnStart.setVisibility(View.GONE);
            holder.tvState.setText(state);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
