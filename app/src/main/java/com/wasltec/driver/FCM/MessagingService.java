package com.wasltec.driver.FCM;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wasltec.driver.Helpers.HubSubscriber;

import org.json.JSONObject;

/**
 * Created by Abdulrhman on 31/10/2017.
 */

public class MessagingService extends FirebaseMessagingService{
    private static final String TAG = "MyInstanceIDLS";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG,remoteMessage.toString());
        try {

            JSONObject allData = new JSONObject(remoteMessage.getData());
            JSONObject allBody;
            switch (allData.getString("message")) {
                case "Call":
                    allBody = allData.getJSONObject("body");
                    Log.e(TAG,remoteMessage.toString());
                    new HubSubscriber().call(allData.getString("messageKey"),
                            allBody.getInt("CallId"),
                            (float) allBody.getDouble("Lat"),
                            (float) allBody.getDouble("Lng"),
                            allBody.getString("ClientNumber"),
                            allBody.getString("ClientName"),
                            allBody.getString("ClientAddress"),
                            allBody.getString("ClientNumber"), 0, 0, null,allBody.getInt("PaymentTypeId"));
                    break;
                case "LaterCall":
                    allBody = allData.getJSONObject("body");
                    Log.e(TAG,remoteMessage.toString());
                    new HubSubscriber().LaterCall(allData.getString("messageKey"),
                            allBody.getInt("Id"),
                            allBody.getString("ClientName"),
                            allBody.getString("ClientNumber"),
                            (float) allBody.getDouble("EndLat"),
                            (float) allBody.getDouble("EndLng"),
                            allBody.getString("EndPlace"),
                             allBody.getString("StartDate"),
                            (float) allBody.getDouble("StartLat"),
                            (float) allBody.getDouble("StartLng"),
                            allBody.getString("StartPlace"),
                            allBody.getInt("PaymentType"),0);
                    break;

                case "Starttrip":
                    allBody = allData.getJSONObject("body");
                    Log.e(TAG,remoteMessage.toString());
                    new HubSubscriber().Starttrip(allData.getString("messageKey"), true, 0, allBody.getInt("TripId"));
                    break;
                case "CancelWait":
                    allBody = allData.getJSONObject("body");
                    Log.e(TAG,remoteMessage.toString());
                    new HubSubscriber().CancelWait(allData.getString("messageKey"),allBody.getString("ClientNumber"));
                    break;
                case "ClientResponse":
                    allBody = allData.getJSONObject("body");
                    Log.e(TAG,remoteMessage.toString());
                    new HubSubscriber().ClientResponse(allData.getString("messageKey"),
                            allBody.getBoolean("Accept"),
                            allBody.getString("ClientNumber"));
                    break;
                case "ReservationConfirm":
                    allBody = allData.getJSONObject("body");
                    Log.e(TAG, remoteMessage.toString());
                    new HubSubscriber().ReservationConfirm(allData.getString("messageKey"),
                            allBody.getInt("ReservationId"),allBody.getBoolean("Accepted"));

                    break;
                case "Message":
                    Log.e(TAG, remoteMessage.toString());
                    new HubSubscriber().Message(allData.getString("messageKey"),null,
                            allData.getJSONObject("body").getString("Message"));

                    break;

                case "Refund":
                    Log.e(TAG, remoteMessage.toString());
                    new HubSubscriber().Refund(allData.getString("messageKey"),0,
                            allData.getJSONObject("body").getInt("credit"));
                    break;
                case "Subtract":
                    Log.e(TAG, remoteMessage.toString());
                    new HubSubscriber().Subtract(allData.getString("messageKey"),0,
                            allData.getJSONObject("body").getInt("AddCreditParameters"));
                    break;
                case "TripSummary":
                    allBody = allData.getJSONObject("body");
                    Log.e(TAG, remoteMessage.toString());
                    new HubSubscriber().TripSummary(allData.getString("messageKey"),
                            allBody.getInt("status"),
                            allBody.getInt("tripid"),
                            allBody.getDouble("Cost"),
                            allBody.getDouble("Distance"),
                            allBody.getDouble("substractedamount"),
                            allBody.getDouble("percentageamount"),
                            allBody.getDouble("payableCash"));
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

