package com.wasltec.driver.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.UiHelpers.ReverseSlideButton;
import com.wasltec.driver.Helpers.UiHelpers.SlideButton;
import com.wasltec.driver.Helpers.UiHelpers.SlideButtonListener;
import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.R;

public class LanguageChooser extends AppCompatActivity implements SlideButtonListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Driver driver = (Driver)new DatabaseHandler(this).getSingle(new Driver(),1);
        if (driver != null && driver.getAccessToken() != null && driver.getAccessToken().length() > 0) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else{
            setContentView(R.layout.activity_language_chooser);
            SlideButton btn = (SlideButton) findViewById(R.id.btnEnglish);
            btn.ref = 1;
            btn.setSlideButtonListener(this);
            ReverseSlideButton btn2 = (ReverseSlideButton) findViewById(R.id.btnArabic);
            btn2.ref = 2;
            btn2.setSlideButtonListener(this);
        }
    }

    private void start(String language) {
        new LanguageHelper().changeLanguage(this, language);
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }

    @Override
    public void handleSlide(int tag) {
        switch (tag) {
            case 1:
                start(LanguageHelper.ENGLISH_LANGUAGE);
                break;
            case 2:
                start(LanguageHelper.ARABIC_LANGUAGE);
                break;
        }
    }
}