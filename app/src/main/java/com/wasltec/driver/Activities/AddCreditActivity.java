package com.wasltec.driver.Activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.AppStateContext;
import com.wasltec.driver.Helpers.BroadcastHelper;
import com.wasltec.driver.Helpers.DialogsHelper;
import com.wasltec.driver.Helpers.DriverService;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.Service.ServicePoster;
import com.wasltec.driver.Helpers.Service.ServiceResult;
import com.wasltec.driver.Models.Profile;
import com.wasltec.driver.Models.ChargeResponse;
import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.R;
import com.wasltec.driver.WebService.Requests.ChargeRequest;

public class AddCreditActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etChargeCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new LanguageHelper().initLanguage(this, true);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_credit);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.add_credit_label));
        }
        etChargeCard = (EditText) findViewById(R.id.etChargeCard);
        findViewById(R.id.btnCharge).setOnClickListener(this);
        Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);

        ((TextView) findViewById(R.id.tvCurrentCredit)).setText(String.valueOf(profile.getCredit()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnCharge) {
            if (etChargeCard.getText().toString().length() < 14)
                DialogsHelper.getAlert(this, getString(R.string.error), getString(R.string.plese_enter_charge_number), getString(R.string.ok)).show();
            else
                new DoChargeAsync().execute(etChargeCard.getText().toString());
        }
    }

    class DoChargeAsync extends AsyncTask<String, Void, ChargeResponse> {
        ProgressDialog mDialog;

        @Override
        protected void onPostExecute(ChargeResponse chargeResponse) {
            super.onPostExecute(chargeResponse);
            mDialog.cancel();
            if (chargeResponse.isSuccess()) {
                etChargeCard.setText("");
                ((TextView)findViewById(R.id.tvCurrentCredit)).setText(String.valueOf(chargeResponse.getNewCredit()));
                Profile profile = (Profile)new DatabaseHandler(DriverService.context).getSingle(new Profile(),1);
                profile.setCredit(chargeResponse.getNewCredit());
                new DatabaseHandler(DriverService.context).updateItem(profile);
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_CREDIT);
                DialogsHelper.getAlert(AddCreditActivity.this, getString(R.string.success), getString(R.string.charge_success)+"\n" + getString(R.string.current_credit) + chargeResponse.getNewCredit(), getString(R.string.ok)).show();
                AppStateContext.doUpdateCredit();
            } else {
                DialogsHelper.getAlert(AddCreditActivity.this, getString(R.string.error), getString(R.string.fail_to_charge_card) + "\n" + ((new LanguageHelper().getCurrentLanguage(AddCreditActivity.this).equalsIgnoreCase(LanguageHelper.ENGLISH_LANGUAGE))?chargeResponse.getEnglishMessage():chargeResponse.getArabicMessage()), getString(R.string.ok)).show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = DialogsHelper.getProgressDialog(AddCreditActivity.this, R.string.please_wait);
            mDialog.show();
        }

        @Override
        protected ChargeResponse doInBackground(String... params) {
            String accessToken = ((Driver) new DatabaseHandler(AddCreditActivity.this).getSingle(new Driver(), 1)).getAccessToken();
            ServiceResult result = new ServicePoster(AddCreditActivity.this).DoPost(new ChargeRequest(params[0]), accessToken);
            return new ChargeResponse(result);
        }
    }
}
