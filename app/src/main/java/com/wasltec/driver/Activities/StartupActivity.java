package com.wasltec.driver.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.UiHelpers.SlideButtonListener;
import com.wasltec.driver.Models.Driver;


public class StartupActivity extends AppCompatActivity implements SlideButtonListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LanguageHelper().initLanguage(this, true);
        Driver driver =(Driver) new DatabaseHandler(getApplicationContext()).getSingle(new Driver(), 1);
        if (driver != null &&  driver.getAccessToken() != null && driver.getAccessToken().length() > 0) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            startActivity(new Intent(this,LanguageChooser.class));
            finish();
        }
    }


    @Override
    public void handleSlide(int tag) {
        startActivity(new Intent(this, (tag == 1) ? LoginActivity.class : RegisterActivity.class));
        finish();
    }

    private void start(String language) {
        new LanguageHelper().changeLanguage(this, language);
        startActivity(new Intent(this,LoginActivity.class));
        finish();
    }
}
