package com.wasltec.driver.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import com.wasltec.driver.Fragments.RegisterFragment;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.UiHelpers.MySwitch;
import com.wasltec.driver.R;

public class RegisterActivity extends AppCompatActivity implements MySwitch.OnChangeAttemptListener, CompoundButton.OnCheckedChangeListener {

    MySwitch swipToLogin;
    RegisterFragment fragment;
    final String FRAGMENT_TAG = "fragmentTag";
    public static ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LanguageHelper().initLanguage(this, true);
        setContentView(R.layout.activity_register);
        View backgroundimage = findViewById(R.id.register_layout);
        Drawable background = backgroundimage.getBackground();
        background.setAlpha(80);
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        fragment =(RegisterFragment) fm.findFragmentByTag(FRAGMENT_TAG);
        if (fragment==null) {
            fragment = new RegisterFragment();
            fm.beginTransaction().add(R.id.fragment_container,fragment,FRAGMENT_TAG).commit();
        }

        swipToLogin = (MySwitch) findViewById(R.id.swipToLogin);
        swipToLogin.setChecked(true);
        swipToLogin.disableClick();
        swipToLogin.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if ((dialog != null) && dialog.isShowing())
            dialog.cancel();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        RegisterFragment.mRequestCode=requestCode;
        if (resultCode == RESULT_OK&&fragment!=null)
            fragment.performTakingPicture(data);
    }

    @Override
    public void onChangeAttempted(boolean isChecked) {
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }
    }
}
