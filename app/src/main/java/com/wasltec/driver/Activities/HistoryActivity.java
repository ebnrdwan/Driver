package com.wasltec.driver.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.wasltec.driver.Adapters.TripAdapter;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Models.HistoryTrip;
import com.wasltec.driver.R;

import java.util.ArrayList;

public class HistoryActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView lvHistory;
    TextView tvEmptyList;
    TripAdapter tripAdapter;
    ArrayList<HistoryTrip> tripList;
    Button btn_back;
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        initComponents();
    }


    private void initComponents() {

        db = new DatabaseHandler(getApplicationContext());
        tripList = db.getAllTrips();
        tvEmptyList = (TextView) findViewById(R.id.tvEmptyList);
        lvHistory = (ListView) findViewById(R.id.lvHistory);
        //active back button
        btn_back = (Button) findViewById(R.id.btn_history_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        lvHistory.setOnItemClickListener(this);
        if (tripList.size() > 0) {
            tvEmptyList.setVisibility(View.GONE);
            lvHistory.setVisibility(View.VISIBLE);
            tripAdapter = new TripAdapter(this, tripList);
            lvHistory.setAdapter(tripAdapter);
            tripAdapter.notifyDataSetChanged();
        } else {

            tvEmptyList.setVisibility(View.VISIBLE);
            lvHistory.setVisibility(View.GONE);
        }

    }

 /*   @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        super.onOptionsItemSelected(item);

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }*/

    // List View Handler
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Intent intent = new Intent(this, TripRouteActivity.class);
        intent.putExtra("tripId", tripList.get(i).id);
        startActivity(intent);
    }


}
