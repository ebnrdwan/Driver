package com.wasltec.driver.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.wasltec.driver.Adapters.IClickListener;
import com.wasltec.driver.Adapters.LaterRequestsAdapter;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.BroadcastHelper;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Models.IModel;
import com.wasltec.driver.Models.LaterRequest;
import com.wasltec.driver.R;

import java.util.ArrayList;
import java.util.Date;

public class LaterRequestsActivity extends AppCompatActivity implements IClickListener {
    View vNoData;
    RecyclerView mRecyclerView;
    LaterRequestsAdapter mAdapter;
    ArrayList<LaterRequest> mData;
    GridLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LanguageHelper().initLanguage(this, true);
        setContentView(R.layout.activity_later_requests);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.later_request);
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.rvData);
        vNoData = findViewById(R.id.vNoData);
        DatabaseHandler db = new DatabaseHandler(this);
        ArrayList<IModel> items = db.getList(new LaterRequest());

        if (items.size() > 0) {
            mData = new ArrayList<>();
            for (IModel item : items) {
                LaterRequest request = (LaterRequest) item;
                if (request.getStartDate() < new Date().getTime() && !request.isStarted()) {
                    request.setMissed(true);
                    db.updateItem(request);
                }
                mData.add(request);
            }
            mAdapter = new LaterRequestsAdapter(mData, this);
            int rows = 1;
            mLayoutManager = new GridLayoutManager(this, rows, GridLayoutManager.VERTICAL, false);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            vNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void itemClicked(int position) {
        BroadcastHelper.sendInform(BroadcastHelper.START_RESERVATION, String.valueOf(mData.get(position).getId()));
        finish();
    }
}
