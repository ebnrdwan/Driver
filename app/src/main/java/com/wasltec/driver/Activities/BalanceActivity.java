package com.wasltec.driver.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.wasltec.driver.Adapters.BalanceAdapter;
import com.wasltec.driver.Adapters.IClickListener;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Models.Balance;
import com.wasltec.driver.Models.BalanceInfo;
import com.wasltec.driver.Models.Profile;
import com.wasltec.driver.Models.TripSummary;
import com.wasltec.driver.R;
import java.util.ArrayList;

public class BalanceActivity extends AppCompatActivity implements IClickListener {
    View vNoData;
    RecyclerView mRecyclerView;
    BalanceAdapter mAdapter;
    GridLayoutManager mLayoutManager;
    TextView balance,verifiedTrips;
    String currency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LanguageHelper().initLanguage(this, true);
        Profile profile = (Profile)new DatabaseHandler(this).getSingle(new Profile(), 1);
        if (getBaseContext().getResources().getConfiguration().locale.getLanguage().equals(LanguageHelper.ENGLISH_LANGUAGE))
            currency = profile.getCurrancyEnglish()+" ";
        else
            currency = profile.getCurrancyArabic()+" ";

        setContentView(R.layout.activity_balance);
        balance = (TextView) findViewById(R.id.balance);
        verifiedTrips = (TextView) findViewById(R.id.verified_trips);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.balance);
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.rvData);
        vNoData = findViewById(R.id.vNoData);
        DatabaseHandler db = new DatabaseHandler(this);
        ArrayList<TripSummary>items = new Balance().getAllTripsummaries(db.getReadableDatabase());

        if (items!=null&&items.size() > 0) {
            BalanceInfo mBalanceInfo = new Balance().getTripsCostAndCount(db.getReadableDatabase());
            balance.setText(currency+String.format("%.2f", new Balance().getBalance(db.getReadableDatabase())));
            verifiedTrips.setText(currency+String.format("%.2f", mBalanceInfo.tripsCost)+" ("+mBalanceInfo.tripsCount+")");
            mAdapter = new BalanceAdapter(items, this,currency);
            int rows = 1;
            mLayoutManager = new GridLayoutManager(this, rows, GridLayoutManager.VERTICAL, false);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            balance.setText("0");
            verifiedTrips.setText("0");
            vNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void itemClicked(int position) {
    }
}
