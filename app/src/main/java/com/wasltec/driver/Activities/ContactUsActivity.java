package com.wasltec.driver.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.DialogsHelper;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.Service.ServicePoster;
import com.wasltec.driver.Helpers.Service.ServiceResult;
import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.R;
import com.wasltec.driver.WebService.Requests.ContactUsRequest;
import com.wasltec.driver.WebService.Responses.ContactUsResponse;
import com.wasltec.driver.WebService.ViewModels.ValidationResult;

public class ContactUsActivity extends Activity implements View.OnClickListener {
    EditText mTitleTextView;
    EditText mBodyTextView;
    private ContactUsRequest mContactUsRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LanguageHelper().initLanguage(this, true);
        setContentView(R.layout.activity_contact_us);
        mContactUsRequest = new ContactUsRequest();
        mTitleTextView = (EditText) findViewById(R.id.edt_title);
        mBodyTextView = (EditText) findViewById(R.id.edt_message);
        findViewById(R.id.btnSendMessage).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSendMessage) {
            mContactUsRequest.getContactsUsViewModel().setTitle(mTitleTextView.getText().toString());
            mContactUsRequest.getContactsUsViewModel().setBody(mBodyTextView.getText().toString());
            ValidationResult result = mContactUsRequest.getContactsUsViewModel().Validate();
            if (!result.getSuccess())
                DialogsHelper.showToast(this, getString(result.getMessage()));
            else
                new SendContactMessage().execute(mContactUsRequest);
        }
    }


    public class SendContactMessage extends AsyncTask<ContactUsRequest, Integer, ServiceResult> {
        private ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = DialogsHelper.getProgressDialog(ContactUsActivity.this, R.string.please_wait);
            mDialog.show();
        }

        @Override
        protected ServiceResult doInBackground(ContactUsRequest... params) {
            String accessToken = ((Driver)new DatabaseHandler(ContactUsActivity.this).getSingle(new Driver(),1)).getAccessToken();
            return new ServicePoster(ContactUsActivity.this).DoPost(params[0],accessToken);
        }


        @Override
        protected void onPostExecute(ServiceResult s) {
            mDialog.cancel();
            if (s.getSuccess()) {
                ContactUsResponse response = ContactUsResponse.newInstance(s.getResult());
                if (response.isSuccess()) {
                    DialogsHelper.showToast(ContactUsActivity.this, getString(R.string.message_send_successfully));
                    finish();
                } else {
                    DialogsHelper.showToast(ContactUsActivity.this, getString(R.string.fail_to_send_message));
                }
            } else {
                DialogsHelper.getAlert(ContactUsActivity.this, getString(R.string.error), getString(R.string.couldnt_connect), getString(R.string.ok)).show();
            }

        }


    }
}