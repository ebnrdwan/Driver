package com.wasltec.driver.Activities;

import android.app.Activity;
import android.os.Bundle;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.R;

public class TermsConditionsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LanguageHelper().initLanguage(this, true);
        setContentView(R.layout.terms_conditions_activity);
    }
}
