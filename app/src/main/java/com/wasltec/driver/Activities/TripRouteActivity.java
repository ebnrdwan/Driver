package com.wasltec.driver.Activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Models.HistoryTrip;
import com.wasltec.driver.Models.TripCoordinates;
import com.wasltec.driver.R;

import java.util.ArrayList;

public class TripRouteActivity extends AppCompatActivity {

    GoogleMap mMap;
    HistoryTrip trip;
    DatabaseHandler db;
    FragmentManager theFragmentManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_route);

        int tripId = getIntent().getExtras().getInt("tripId");
        prepareDatabase(tripId);
        initComponents();
        prepareMap();
        drawRoute();
    }

    private void initComponents() {

        TextView tvTripStartDate = (TextView) findViewById(R.id.tvTripStartDate);
        TextView tvTripEndDate = (TextView) findViewById(R.id.tvTripEndDate);
        TextView tvTripDistance = (TextView) findViewById(R.id.tvTripDistance);
        TextView tvTripCost = (TextView) findViewById(R.id.tvTripCost);

        tvTripStartDate.setText(trip.getLabeledStartDate(this));
        tvTripEndDate.setText(trip.getLabeledEndDate(this));
        tvTripDistance.setText(trip.getLabeledDistance(this));
        tvTripCost.setText(trip.getLabeledCost(this));
    }


    private void prepareDatabase(int tripId) {

        db = new DatabaseHandler(getApplicationContext());
        trip = db.getTripInfo(tripId);
    }

    private void prepareMap() {

        theFragmentManager = getSupportFragmentManager();
        MapFragment mMap = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
                        if (mMap != null)


mMap.getMapAsync(new OnMapReadyCallback() {
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    }
});

    }

    private void drawRoute() {
        if (mMap == null)
            return;
        ArrayList<TripCoordinates> tripCoordinates = trip.tripCoordinates;

        if (tripCoordinates.size() > 0) {
            addMarker(tripCoordinates.get(0), R.string.start, R.drawable.man);
            addMarker(tripCoordinates.get(tripCoordinates.size() - 1), R.string.end, R.drawable.car);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(tripCoordinates.get(0).getLatitude(), tripCoordinates.get(0).getLongitude()), 15));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(tripCoordinates.get(0).getLatitude(), tripCoordinates.get(0).getLongitude()), 14.0f));
        }

        PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.RED);

        for (TripCoordinates coordinates : tripCoordinates) {

            LatLng point = new LatLng(coordinates.getLatitude(), coordinates.getLongitude());
            rectLine.add(point);
        }
        mMap.addPolyline(rectLine);


    }

    private void addMarker(TripCoordinates coordinates, int titleId, int drawableId) {

        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(coordinates.getLatitude(), coordinates.getLongitude()))
                .title(getString(titleId));
        Marker marker = mMap.addMarker(markerOptions);
        marker.setIcon(BitmapDescriptorFactory.fromResource(drawableId));

    }
}
