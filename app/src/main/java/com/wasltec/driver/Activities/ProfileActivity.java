package com.wasltec.driver.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.wasltec.driver.Helpers.DialogsHelper;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.Service.ServicePoster;
import com.wasltec.driver.Helpers.Service.ServiceResult;
import com.wasltec.driver.Models.Profile;
import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.R;
import com.wasltec.driver.WebService.Requests.FetchProfileRequest;
import com.wasltec.driver.WebService.Requests.UpdateProfileRequest;
import com.wasltec.driver.WebService.Responses.FetchProfileResponse;
import com.wasltec.driver.WebService.Responses.UpdateProfileResponse;
import com.wasltec.driver.WebService.ViewModels.ValidationResult;

public class ProfileActivity extends Activity implements View.OnClickListener {
    private Profile mProfile;
    EditText etFirstName, etPassword, etEmail;
    UpdateProfileRequest mRequest;
    TextView txtVehicleType;
    String vehicleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequest = new UpdateProfileRequest();
        mProfile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
        setContentView(R.layout.activity_profile);
        setUpActivity();
    }

    private void setUpActivity() {
        txtVehicleType = (TextView) findViewById(R.id.txtCarType);
        etFirstName = (EditText) findViewById(R.id.txtName);
        etPassword = (EditText) findViewById(R.id.txtPassword);
        etEmail = (EditText) findViewById(R.id.txtEmailAddress);
        findViewById(R.id.btnCancel).setOnClickListener(this);
        findViewById(R.id.btnSave).setOnClickListener(this);
        if (mProfile.getEmail() != null && mProfile.getEmail().length() != 0) {
            etFirstName.setText(mProfile.getFirstName());
            etEmail.setText(mProfile.getEmail());
            etPassword.setText(mProfile.getPassword());
            if (getBaseContext().getResources().getConfiguration().locale.getLanguage().equals(LanguageHelper.ENGLISH_LANGUAGE))
                vehicleType = mProfile.getVehicleTypeEnglish();
            else
                vehicleType = mProfile.getVehicleTypeArabic();
            if (vehicleType.equals("zzzzzzz")) {
                Driver driver = (Driver) new DatabaseHandler(getApplicationContext()).getSingle(new Driver(), 1);
                new FetchProfileAsync().execute(driver.getAccessToken());
            }else {
                txtVehicleType.setText(getResources().getString(R.string.vehicle_type) + "   :    " + vehicleType);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                finish();
                break;
            case R.id.btnSave:
                bindRequest();
                ValidationResult result = mRequest.getModel().Validate();
                if (result.getSuccess())
                    new UpdateProfileAsync().execute(mRequest);
                else
                    DialogsHelper.showToast(this, getString(result.getMessage()));
                break;
        }
    }

    private void bindRequest() {
        mRequest.getModel().setFirstName(etFirstName.getText().toString());
        mRequest.getModel().setEmail(etEmail.getText().toString());
        mRequest.getModel().setNewPassword(etPassword.getText().toString());
    }

    public class FetchProfileAsync extends AsyncTask<String, String, ServiceResult> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = DialogsHelper.getProgressDialog(ProfileActivity.this, getString(R.string.loading), getString(R.string.please_wait));
            dialog.show();
        }

        @Override
        protected ServiceResult doInBackground(String... params) {
            Log.v("at", params[0]);
            return new ServicePoster(ProfileActivity.this).DoPost(new FetchProfileRequest(), params[0]);
        }

        @Override
        protected void onPostExecute(ServiceResult s) {
            dialog.cancel();

            if (s == null) {
                DialogsHelper.showToast(ProfileActivity.this, getString(R.string.login_activity_service_error_toast));
                return;
            }
            FetchProfileResponse response = FetchProfileResponse.newInstance(s.getResult());
            if (response == null) {
                DialogsHelper.showToast(ProfileActivity.this, getString(R.string.main_activity_login_failed_toast));
            } else {
                response.getProfile().setPassword(mProfile.getPassword());
                response.getProfile().setId(1);
                new DatabaseHandler(ProfileActivity.this).updateItem(response.getProfile());
                mProfile.setVehicleTypeEnglish(response.getProfile().getVehicleTypeEnglish());
                mProfile.setVehicleTypeArabic(response.getProfile().getVehicleTypeArabic());
                if (getBaseContext().getResources().getConfiguration().locale.getLanguage().equals(LanguageHelper.ENGLISH_LANGUAGE))
                    vehicleType = mProfile.getVehicleTypeEnglish();
                else
                    vehicleType = mProfile.getVehicleTypeArabic();
                txtVehicleType.setText(getResources().getString(R.string.vehicle_type) + "   :    " + vehicleType);
            }
        }
    }

    public class UpdateProfileAsync extends AsyncTask<UpdateProfileRequest, Integer, UpdateProfileResponse> {
        ProgressDialog mDialog;

        @Override
        protected void onPreExecute() {
            mDialog = DialogsHelper.getProgressDialog(ProfileActivity.this, getString(R.string.please_wait), getString(R.string.please_wait));
            mDialog.show();
        }

        @Override
        protected UpdateProfileResponse doInBackground(UpdateProfileRequest... params) {
            String accessToken = ((Driver) new DatabaseHandler(ProfileActivity.this).getSingle(new Driver(), 1)).getAccessToken();
            return UpdateProfileResponse.newInstance(new ServicePoster(ProfileActivity.this).DoPost(params[0], accessToken).getResult());
        }

        @Override
        protected void onPostExecute(UpdateProfileResponse s) {
            mDialog.cancel();
            if (s != null && s.isSuccess()) {
                mProfile.setEmail(mRequest.getModel().getEmail());
                mProfile.setFirstName(mRequest.getModel().getFirstName());
                mProfile.setPassword(mRequest.getModel().getNewPassword());
                new DatabaseHandler(ProfileActivity.this).updateItem(mProfile);
                Driver driver = (Driver) new DatabaseHandler(ProfileActivity.this).getSingle(new Driver(), 1);
                driver.setPassword(mRequest.getModel().getNewPassword());
                new DatabaseHandler(ProfileActivity.this).updateItem(driver);
                DialogsHelper.showToast(ProfileActivity.this, getString(R.string.update_profile_success));
            } else {
                DialogsHelper.getAlert(ProfileActivity.this, getString(R.string.error), getString(R.string.couldnt_connect), getString(R.string.ok)).show();
            }
        }
    }
}
