package com.wasltec.driver.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.DialogsHelper;
import com.wasltec.driver.Helpers.KeyboardHelper;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.Permissions;
import com.wasltec.driver.Helpers.Service.PostRequest;
import com.wasltec.driver.Helpers.Service.ServicePoster;
import com.wasltec.driver.Helpers.Service.ServiceResult;
import com.wasltec.driver.Helpers.UiHelpers.MySwitch;
import com.wasltec.driver.R;
import com.wasltec.driver.WebService.Requests.FetchProfileRequest;
import com.wasltec.driver.WebService.Responses.FetchProfileResponse;
import com.wasltec.driver.WebService.ViewModels.ForgetPasswordViewModel;
import com.wasltec.driver.WebService.ViewModels.LoginViewModel;
import com.wasltec.driver.WebService.Responses.ForgetPasswordResponse;
import com.wasltec.driver.WebService.Responses.LoginResponse;
import com.wasltec.driver.WebService.ViewModels.ValidationResult;


public class LoginActivity extends AppCompatActivity implements MySwitch.OnChangeAttemptListener, CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    EditText txtUserName, txtPassword;
    LoginViewModel mModel;
    ForgetPasswordViewModel mForgetPasswordViewModel;
    MySwitch swipeToRegister;
    LoginResponse mResponse;
    Permissions permissions;

    public void hideSoftKeyboard(View view) {
        new KeyboardHelper().Hide(getApplicationContext(), this);
    }

    @Override
    public void onChangeAttempted(boolean isChecked) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked) {
            startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            finish();
        }
    }

    public void setupUI(View view) {
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(v);
                    return false;
                }

            });
        }
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    private void bindViewModel() {
        mModel.setNumber("2"+txtUserName.getText().toString());
        mModel.setPassword(txtPassword.getText().toString());
    }

    public void doLogin() {
        new KeyboardHelper().Hide(getApplicationContext(), this);
        bindViewModel();
        ValidationResult result = mModel.Validate();
        if (result.getSuccess()) {
            new PerformLogin().execute(mModel);
        } else {
            DialogsHelper.showToast(this, getString(result.getMessage()));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mModel = new LoginViewModel();
        new LanguageHelper().initLanguage(this, true);
        setContentView(R.layout.activity_login);
        checkPer();
        View backgroundimage = findViewById(R.id.login_layout);
        permissions = new Permissions();
        Drawable background = backgroundimage.getBackground();
        background.setAlpha(80);
        findViewById(R.id.btnLogin).setOnClickListener(this);
        findViewById(R.id.btnForgetPassword).setOnClickListener(this);
        setupUI(findViewById(R.id.login_layout));
        swipeToRegister = (MySwitch) findViewById(R.id.swipToRegister);
        swipeToRegister.setChecked(true);
        swipeToRegister.disableClick();
        swipeToRegister.setOnCheckedChangeListener(this);
        txtUserName = (EditText) findViewById(R.id.numberEditText);
        txtPassword = (EditText) findViewById(R.id.passwordEditText);
    }

    private void checkPer() {
        try {
            if (permissions.hasPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

            } else if (permissions.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else if (permissions.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else if (permissions.hasPermission(this, Manifest.permission.CALL_PHONE)) {

            } else {
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, 1113);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int i = 0, len = permissions.length; i < len; i++) {
            switch (requestCode) {
                case 1113:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    } else {
                        finish();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }

    }

    public void doForgetPassword() {
        mForgetPasswordViewModel = new ForgetPasswordViewModel();
        mForgetPasswordViewModel.setNumber(txtUserName.getText().toString());
        ValidationResult result = mForgetPasswordViewModel.Validate();
        if (result.getSuccess())
            new ForgetPasswordAsync().execute(mForgetPasswordViewModel);
        else
            DialogsHelper.showToast(this, getString(result.getMessage()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                doLogin();
                break;
            case R.id.btnForgetPassword:
                doForgetPassword();
                break;
        }
    }


    public class ForgetPasswordAsync extends AsyncTask<ForgetPasswordViewModel, Integer, ServiceResult> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = DialogsHelper.getProgressDialog(LoginActivity.this, getString(R.string.loading), getString(R.string.please_wait));
            dialog.show();
        }

        @Override
        protected ServiceResult doInBackground(ForgetPasswordViewModel... params) {
            return new ServicePoster(LoginActivity.this).DoPost(new PostRequest(params[0].toJson(), getString(R.string.forget_password_url)));
        }

        @Override
        protected void onPostExecute(ServiceResult s) {
            dialog.cancel();
            ForgetPasswordResponse response = ForgetPasswordResponse.newInstance(s.getResult());

            if (response != null && response.isSuccess()) {
                DialogsHelper.showToast(LoginActivity.this, getString(R.string.will_send_password));
            } else {
                if (response != null && response.isValidResponse())
                    DialogsHelper.showToast(LoginActivity.this, getString(R.string.service_error));
                else if (response != null)
                    DialogsHelper.showToast(LoginActivity.this, response.getMessage());
                else
                    DialogsHelper.showToast(LoginActivity.this, getString(R.string.service_error));
            }
        }


    }


    public class PerformLogin extends AsyncTask<LoginViewModel, String, ServiceResult> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = DialogsHelper.getProgressDialog(LoginActivity.this, getString(R.string.loading), getString(R.string.please_wait));
            dialog.show();
        }

        @Override
        protected ServiceResult doInBackground(LoginViewModel... params) {
            return new ServicePoster(LoginActivity.this).DoPost(params[0].toString(), getString(R.string.login_url), true);
        }

        @Override
        protected void onPostExecute(ServiceResult s) {
            dialog.cancel();

            if (s == null) {
                DialogsHelper.showToast(LoginActivity.this, getString(R.string.login_activity_service_error_toast));
                return;
            }
            if(s.getResult() != null && s.getResult().length() > 0){
            LoginResponse response = LoginResponse.newInstance(s.getResult(), LoginActivity.this.mModel);
            Log.v("res", s.getResult());
            if (response == null) {
                DialogsHelper.showToast(LoginActivity.this, getString(R.string.main_activity_login_failed_toast));
            } else {
                mResponse=response;
                new FetchProfileAsync().execute(response.getDriver().getAccessToken());
                // save login info
                // goto main activity
            }}
            else{
                DialogsHelper.showToast(LoginActivity.this, getString(R.string.fail_to_fetch_service));
            }

        }
    }

    public class FetchProfileAsync extends AsyncTask<String, String, ServiceResult> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = DialogsHelper.getProgressDialog(LoginActivity.this, getString(R.string.loading), getString(R.string.please_wait));
            dialog.show();
        }

        @Override
        protected ServiceResult doInBackground(String... params) {
            Log.v("at", params[0]);
            return new ServicePoster(LoginActivity.this).DoPost(new FetchProfileRequest(), params[0]);
        }

        @Override
        protected void onPostExecute(ServiceResult s) {
            dialog.cancel();

            if (s == null) {
                DialogsHelper.showToast(LoginActivity.this, getString(R.string.login_activity_service_error_toast));
                return;
            }
            FetchProfileResponse response = FetchProfileResponse.newInstance(s.getResult());
            if (response == null) {
                DialogsHelper.showToast(LoginActivity.this, getString(R.string.main_activity_login_failed_toast));
            } else {
                response.getProfile().setPassword(mModel.getPassword());
                new DatabaseHandler(LoginActivity.this).updateItem(response.getProfile());
                if (mResponse!=null)
                    new DatabaseHandler(LoginActivity.this).updateItem(mResponse.getDriver());
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }

        }
    }
}
