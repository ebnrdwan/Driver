package com.wasltec.driver.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.wasltec.driver.Helpers.AnimationHelper;
import com.wasltec.driver.Helpers.AppStateHelper;
import com.wasltec.driver.Helpers.AppStateContext;
import com.wasltec.driver.Helpers.DateHelper;
import com.wasltec.driver.Helpers.DialogsHelper;
import com.wasltec.driver.Helpers.DriverService;
import com.wasltec.driver.Helpers.BroadcastHelper;
import com.wasltec.driver.Helpers.CallHelper;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.HubFactory;
import com.wasltec.driver.Helpers.ICancelDialogDelegate;
import com.wasltec.driver.Helpers.LanguageHelper;
import com.wasltec.driver.Helpers.MapHelper;
import com.wasltec.driver.Helpers.NotificationHelper;
import com.wasltec.driver.Helpers.PlayServiceHelper;
import com.wasltec.driver.Helpers.RouteHelper;
import com.wasltec.driver.Helpers.SharedPrefHelper;
import com.wasltec.driver.Helpers.States.ClientMessage;
import com.wasltec.driver.Models.Client;
import com.wasltec.driver.Models.LaterRequest;
import com.wasltec.driver.Models.Profile;
import com.wasltec.driver.R;
import com.wasltec.driver.RegistrationIntentService;
import com.wasltec.driver.Views.MenuFragment;
import com.wasltec.driver.Views.NewMessageDialog;
import com.wasltec.driver.Views.SendNewMessageDialog;
import com.wasltec.driver.Views.SimpleAlertDialog;
import java.util.Date;
import microsoft.aspnet.signalr.client.ConnectionState;

public class MainActivity extends AppCompatActivity implements NewMessageDialog.SearchNotification, SendNewMessageDialog.NewMessageNotification, OnMapReadyCallback, OnClickListener, ICancelDialogDelegate {
    private boolean forceClose;
    public boolean willRestart = false;
    Marker marker;
    Receiver mReceiver;
    TextView callPopupClientName, callPaymentType,laterCallPaymentType, callPopupClientNumber, waitingPopupClientName, waitingPopupClientNumber, txtTripDetails, txtTripDistance, add, txtTripWPM, tvLaterCallClientName, tvLaterCallDate;
    Button btnHere, btnStartTrip, btnBusy,position,btnEndRequest;
    ProgressBar progressBar = null;
    ProgressDialog mProgressDialog = null;
    PolylineOptions options = null;
    MenuFragment myMenuFragment;
    private View vBusy;
    private GoogleMap mMap;
    ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null && getIntent().hasExtra("Force"))
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        mReceiver = new Receiver();
        IntentFilter filter = new IntentFilter("com.wasltec.inform");
        this.registerReceiver(mReceiver, filter);
        new LanguageHelper().initLanguage(this, true);
        setContentView(R.layout.activity_main);
        setUpMyMenu();
        setupHub();
        new NotificationHelper().clear(getApplicationContext());
        vBusy = findViewById(R.id.vBusy);
        tvLaterCallClientName = (TextView) findViewById(R.id.tvLaterCallClientName);
        tvLaterCallDate = (TextView) findViewById(R.id.tvLaterCallTime);
        callPaymentType = (TextView) findViewById(R.id.lblPaymentType);
        laterCallPaymentType = (TextView) findViewById(R.id.lblLaterPaymentType);
        findViewById(R.id.btnLaterCallAccept).setOnClickListener(this);
        findViewById(R.id.btnLaterCallCancel).setOnClickListener(this);
        findViewById(R.id.btnEndTrip).setOnClickListener(this);
        findViewById(R.id.btnHere).setOnClickListener(this);
        findViewById(R.id.btnStartTrip).setOnClickListener(this);
        btnEndRequest=(Button)findViewById(R.id.btnEndRequest);
        btnEndRequest.setOnClickListener(this);
        findViewById(R.id.imagebutton).setOnClickListener(this);
        findViewById(R.id.btnNavigateToClient).setOnClickListener(this);
        findViewById(R.id.ImgClientWaitCall).setOnClickListener(this);
        findViewById(R.id.btnAccept).setOnClickListener(this);
        findViewById(R.id.btnCancel).setOnClickListener(this);
        findViewById(R.id.btnBusy).setOnClickListener(this);
        findViewById(R.id.btnCancelRequest).setOnClickListener(this);
        setUpServiceConnection();
        updateConnection();
        position = (Button) findViewById(R.id.position);
        setUpMapIfNeeded();
        this.setState(AppStateContext.CURRENT_STATE);
        if (new PlayServiceHelper().checkPlayServices(this) && !new SharedPrefHelper().getSharedBoolean(this, SharedPrefHelper.SENT_TOKEN_TO_SERVER)) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppStateContext.State != null) {
            AppStateContext.State.setBackground(false);
            setState(AppStateContext.CURRENT_STATE);
            setHelperState();
        }
        setUpMapIfNeeded();
        setupHub();
        updateConnection();
        Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
        if (profile != null)
            ((TextView) findViewById(R.id.tvCredit)).setText(getString(R.string.current_credit)+" "+String.format("%d",profile.getCredit()));
        if (AppStateContext.State != null)
            AppStateContext.State.setBackground(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AppStateContext.State != null) {
            AppStateContext.State.setBackground(false);
            if (AppStateContext.State.getLastMessage() != null && !AppStateContext.State.getLastMessage().isMessageSeen() && AppStateContext.State.isInCall() && !AppStateContext.State.getLastMessage().getLastMessage().equals(""))
                Message(AppStateContext.State.getLastMessage().getLastMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    public void updateTripDistance() {
        txtTripDistance.setText(String.format("%.2f", AppStateContext.State.getTrip().getKMDistance()));
        txtTripDetails.setText(String.format("%.2f", AppStateContext.State.getTrip().getCost()));
        txtTripWPM.setText(String.format("%d",(AppStateContext.State.getTrip().getWaitSeconds() / 60)));
    }

    private void setUpMyMenu() {
        Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
        if (profile!=null) {
            myMenuFragment = MenuFragment.newInstance(profile.isSupportCredit());
            myMenuFragment.setOnChangeLanguageListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    new LanguageHelper().changeLanguage(MainActivity.this);
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                    finish();
                }
            });
            myMenuFragment.setOnClickExitListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    DriverService.forceStop = true;
                    DriverService.isRun = false;
                    stopService(new Intent(MainActivity.this, DriverService.class));
                    finish();
                }
            });
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.menuframeContainer, myMenuFragment, "myCustomMenu");
            transaction.commit();
            if (myMenuFragment != null && HubFactory.connection != null)
                myMenuFragment.setConnection_title(AppStateHelper.getAppState(this, DriverService.isRun));
        }
    }

    private void setUpServiceConnection() {
        boolean isBusy = new SharedPrefHelper().getSharedBoolean(MainActivity.this, SharedPrefHelper.SHARED_PREFERENCE_IS_BUSY_KEY);
        btnBusy.setText(isBusy ? getString(R.string.start_service) : getString(R.string.busy));
        btnBusy.setTextColor(isBusy ? Color.WHITE : Color.parseColor("#6e2c69"));
        vBusy.setVisibility(isBusy ? View.VISIBLE : View.GONE);
        if (!isBusy && !DriverService.isRun)
            new ManageServiceAsync().execute(true);
    }

    public void updateConnection() {
        if (HubFactory.connection != null)
            EnableControls(HubFactory.connection.getState() == ConnectionState.Connected || DriverService.isRun);
        if (myMenuFragment != null)
            myMenuFragment.setConnection_title(AppStateHelper.getAppState(this, DriverService.isRun));
    }

    public void EnableControls(final boolean enabledState) {
        findViewById(R.id.ClientResponse).setEnabled(enabledState);
        findViewById(R.id.WaitPopup).setEnabled(enabledState);
        findViewById(R.id.tripCounter).setEnabled(enabledState);
        findViewById(R.id.CallPopup).setEnabled(enabledState);
    }

    public void setWaitProgress() {
        progressBar.setProgress(AppStateContext.State.getCallResponseWaitCounter());
    }

    public void clearTheMap() {
        options = null;
        marker = null;
        if (mMap != null) {
            mMap.clear();
            setupMapMakers();
        }
    }

    public void setupMapMakers() {
        if (AppStateContext.State != null && AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lat > 0 && AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lon > 0) {
            if (marker == null) {
                marker = mMap.addMarker(new MarkerOptions().position(new LatLng(AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lat, AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lon)).title(getString(R.string.main_activity_your_are_here_marker_label)));
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.car));
            } else
                marker.setPosition(new LatLng(AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lat, AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lon));
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(marker.getPosition());
            if (AppStateContext.State.getClient().Lat > 0 && AppStateContext.State.getClient().Lng > 0) {
                if (AppStateContext.State.getClient().clientMarker == null) {
                    AppStateContext.State.getClient().clientMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(AppStateContext.State.getClient().Lat, AppStateContext.State.getClient().Lng)).title(AppStateContext.State.getClient().Name));
                    AppStateContext.State.getClient().clientMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.man));

                } else {
                    AppStateContext.State.getClient().clientMarker.setPosition(new LatLng(AppStateContext.State.getClient().Lat, AppStateContext.State.getClient().Lng));
                }
                if (AppStateContext.State.getClient().clientMarker != null && AppStateContext.State.getClient().clientMarker.getPosition().latitude > 0)
                    builder.include(AppStateContext.State.getClient().clientMarker.getPosition());
            }
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude), 14.0f));
        }
    }

    public void Message(final String message) {
        NewMessageDialog dialog = new NewMessageDialog();
        dialog.setMessage(message);
        try {
            dialog.show(getFragmentManager(), getString(R.string.main_activity_send_new_message_dialog_title));
            AppStateContext.State.setLastMessage(new ClientMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void BadLogin() {
        if (AppStateContext.State != null && !AppStateContext.State.isBadLogin()) {
            AppStateContext.State.setBadLogin(true);
            AppStateContext.State.setInTrip(false);
            AppStateContext.State.setInCall(false);
            stopService(new Intent(this, DriverService.class));
            new DatabaseHandler(getApplicationContext()).ClearData();
            Toast.makeText(getApplicationContext(), getString(R.string.main_activity_login_failed_toast), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            willRestart = true;
            startActivity(intent);
            finish();
        }
    }

    public void DoSecondLogin() {
        DriverService.forceStop = true;
        DriverService.isRun = false;
        stopService(new Intent(MainActivity.this, DriverService.class));
        HubFactory.connection.clearInvocationCallbacks("Force Stop");
        HubFactory.connection.stop();
        HubFactory.connection = null;
        HubFactory.hub=null;
        new DatabaseHandler(this).ClearData();

        SimpleAlertDialog dialog = new SimpleAlertDialog(this, "This account is opened by  ", "second login");
        dialog.setOnButtonClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
            }
        });
        dialog.show();
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bindClientControls() {
        Client _client = AppStateContext.State.getClient();
        if (_client != null) {
            add.setText(_client.Address);
            callPopupClientName.setText(_client.Name);
            callPopupClientNumber.setText(_client.Number);
            waitingPopupClientName.setText(_client.Name);
            waitingPopupClientNumber.setText(_client.Number);
            txtTripDetails.setText(String.valueOf(AppStateContext.info.getOpenDoorPrice()));
            callPaymentType.setText(_client.paymentType == 1 ? R.string.cash : R.string.promotion);
        }
    }

    private void showAnimation(final int id) {
        AnimationHelper animationHelper = new AnimationHelper();
        if (id != R.id.btnBusy && btnBusy.getVisibility() == View.VISIBLE)
            animationHelper.HideAnimated(btnBusy, findViewById(R.id.mainCon));
        if (id != R.id.CallPopup && findViewById(R.id.CallPopup).getVisibility() == View.VISIBLE)
            animationHelper.HideAnimated(findViewById(R.id.CallPopup), findViewById(R.id.mainCon));
        if (id != R.id.WaitPopup && findViewById(R.id.WaitPopup).getVisibility() == View.VISIBLE)
            animationHelper.HideAnimated(findViewById(R.id.WaitPopup), findViewById(R.id.mainCon));
        if (id != R.id.ClientResponse && findViewById(R.id.ClientResponse).getVisibility() == View.VISIBLE)
            animationHelper.HideAnimated(findViewById(R.id.ClientResponse), findViewById(R.id.mainCon));
        if (id != R.id.tripCounter && findViewById(R.id.tripCounter).getVisibility() == View.VISIBLE)
            animationHelper.HideAnimated(findViewById(R.id.tripCounter), findViewById(R.id.mainCon));
        if (id != R.id.vLaterCallPopup && findViewById(R.id.vLaterCallPopup).getVisibility() == View.VISIBLE)
            animationHelper.HideAnimated(findViewById(R.id.vLaterCallPopup), findViewById(R.id.mainCon));
        if (id != 0)
            animationHelper.ShowAnimated(findViewById(id));
    }

    public void displayRoute(final PolylineOptions options) {
        if (mMap != null)
            mMap.addPolyline(options);
    }

    public void setState(int stateCode) {
        Log.v("newState", stateCode + "");
        Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
        switch (stateCode) {
            case AppStateContext.STATE_FREE:
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(true);
                showAnimation(R.id.btnBusy);
                clearTheMap();
                AppStateContext.HELPER_STATE = AppStateContext.STATE_FREE;
                btnBusy.setVisibility(View.VISIBLE);
                btnHere.setVisibility(View.VISIBLE);
                btnStartTrip.setVisibility(View.GONE);
                findViewById(R.id.btnCancelRequest).setVisibility(View.VISIBLE);
                findViewById(R.id.btnEndRequest).setVisibility(View.GONE);
                break;
            case AppStateContext.STATE_INCOMING_CALL:
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(false);
                btnBusy.setVisibility(View.GONE);
                bindClientControls();
                if (mMap != null)
                    setupMapMakers();
                showAnimation(R.id.CallPopup);
                break;
            case 77:
                btnBusy.setVisibility(View.GONE);
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(false);
                showAnimation(R.id.WaitPopup);
                break;
            case AppStateContext.STATE_CLIENT_CANCEL:
                btnBusy.setVisibility(View.GONE);
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(true);
                clearTheMap();
                btnHere.setVisibility(View.VISIBLE);
                btnStartTrip.setVisibility(View.GONE);
                findViewById(R.id.btnEndRequest).setVisibility(View.GONE);
                showAnimation(R.id.btnBusy);

                if (!profile.isSupportCredit())
                DialogsHelper.getAlert(MainActivity.this, getString(R.string.client_canceliation), getString(R.string.main_activity_client_cancel_order_toast), getString(R.string.ok)).show();
                AppStateContext.changeState(AppStateContext.STATE_FREE);
                break;
            case AppStateContext.STATE_CLIENT_CONFIRM_CALL:
                btnBusy.setVisibility(View.GONE);
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(false);
                bindClientControls();
                showAnimation(R.id.ClientResponse);
                PolylineOptions options = new RouteHelper().getRoutes(AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lat,
                        AppStateContext.State.getCoordinatesInfo().CurrentCoordinates.Lon, AppStateContext.State.getClient().Lat, AppStateContext.State.getClient().Lng);
                displayRoute(options);
                break;
            case AppStateContext.STATE_DRIVER_SUCCESS_SENDING_AM_HERE:
                bindClientControls();
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(false);
                if (!profile.isServiceProvider())
                    btnStartTrip.setVisibility(View.VISIBLE);
                else
                    btnEndRequest.setVisibility(View.VISIBLE);
                findViewById(R.id.btnCancelRequest).setVisibility(View.GONE);
                showAnimation(R.id.ClientResponse);
                break;
            case AppStateContext.STATE_DRIVER_SUCCESS_SENDING_START_TRIP:
                btnBusy.setVisibility(View.GONE);
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(false);
                AppStateContext.State.getClient().clientMarker = null;
                clearTheMap();
                if (mMap != null)
                    setupMapMakers();
                updateTripDistance();
                showAnimation(R.id.tripCounter);
                break;
            case AppStateContext.STATE_DRIVER_SUCCESS_SENDING_END_TRIP:
                showAnimation(R.id.btnBusy);
                if (myMenuFragment != null)
                    myMenuFragment.setMenu_btnEnabled(true);
                break;
            case AppStateContext.STATE_LATER_CALL:
                tvLaterCallClientName.setText(AppStateContext.State.getLaterCall().getClientName());
                tvLaterCallDate.setText(DateHelper.formatDateTime(new Date((long) AppStateContext.State.getLaterCall().getStartDate())));
                ((TextView) findViewById(R.id.tvLaterCallAddress)).setText(AppStateContext.State.getLaterCall().getStartPlace());
                showAnimation(R.id.vLaterCallPopup);
                laterCallPaymentType.setText(AppStateContext.State.getLaterCall().getPaymentType() == 1 ? R.string.cash : R.string.promotion);
                break;
            case AppStateContext.STATE_LATER_CALL_RESPONSE:
                //TODO: Show Result Of Later Response
                break;
            case AppStateContext.STATE_CLIENT_IGNORE_CALL:
                DialogsHelper.getAlert(this, getString(R.string.info), getString(R.string.client_does_not_confirm), getString(R.string.ok)).show();
                showAnimation(R.id.btnBusy);
                break;
        }
        if (myMenuFragment != null && HubFactory.connection != null)
            myMenuFragment.setConnection_title(AppStateHelper.getAppState(this, DriverService.isRun));
    }

    private void setupHub() {
        progressBar = (ProgressBar) findViewById(R.id.prgCallEnder);
        txtTripDetails = (TextView) findViewById(R.id.txtTripDetails);
        txtTripDistance = (TextView) findViewById(R.id.txtTripDistance);
        txtTripWPM = (TextView) findViewById(R.id.txtTripWPM);
        callPopupClientName = (TextView) findViewById(R.id.lblClientName);
        callPopupClientNumber = (TextView) findViewById(R.id.lblClientPhone);
        btnHere = (Button) findViewById(R.id.btnHere);
        waitingPopupClientName = (TextView) findViewById(R.id.lblClientWaitingName);
        waitingPopupClientNumber = (TextView) findViewById(R.id.lblClientWatingPhone);
        add = (TextView) findViewById(R.id.lblAddress);
        btnStartTrip = (Button) findViewById(R.id.btnStartTrip);
        btnBusy = (Button) findViewById(R.id.btnBusy);
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMapAsync(this);
        }
    }

    @Override
    public void onDialogPositiveClick() {
        SendNewMessageDialog dialog = new SendNewMessageDialog();
        dialog.show(getFragmentManager(), getString(R.string.main_activity_send_new_message_dialog_title));
    }

    @Override
    public void onDialogPositiveClick(String message) {
        if (message.length() > 0&&AppStateContext.CURRENT_STATE!=AppStateContext.STATE_FREE) {
            new AppStateContext().doSendMessage(message);
            updateProgressDialog(true);
        }
    }

    private void updateProgressDialog(boolean state) {
        if (mProgressDialog == null)
            mProgressDialog = DialogsHelper.getProgressDialog(this, R.string.please_wait);
        if (state)
            mProgressDialog.show();
        else
            mProgressDialog.cancel();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if ((mDialog != null) && mDialog.isShowing())
            mDialog.cancel();
        if (AppStateContext.State != null && !forceClose)
            AppStateContext.State.setBackground(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            mMap = googleMap;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            setupMapMakers();
            position.setVisibility(View.VISIBLE);
            position.setOnClickListener(this);
        }
    }

    public void doCreditSubtract() {
        Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
        DialogsHelper.getAlert(this, getString(R.string.info), getString(R.string.credit_subtract) + "\n" + getString(R.string.current_credit) +" "+ String.format("%d",profile.getCredit()), getString(R.string.ok)).show();
        ((TextView) findViewById(R.id.tvCredit)).setText(getString(R.string.current_credit)+" "+String.format("%d", profile.getCredit()));
    }

    public void doCreditRefund() {
        Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
        DialogsHelper.getAlert(this, getString(R.string.info), getString(R.string.credit_refund) + "\n" + getString(R.string.current_credit) +" "+ String.format("%d", profile.getCredit()), getString(R.string.ok)).show();
        ((TextView) findViewById(R.id.tvCredit)).setText(getString(R.string.current_credit)+" "+String.format("%d", profile.getCredit()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLaterCallAccept:
                AppStateContext.doLaterCall(true);
                updateLaterCounter();
                break;
            case R.id.btnLaterCallCancel:
                AppStateContext.doLaterCall(false);
                updateLaterCounter();
                break;
            case R.id.btnEndTrip:
                clearTheMap();
                AppStateContext.doEndTrip();
                showAnimation(R.id.btnBusy);
                findViewById(R.id.btnHere).setVisibility(View.VISIBLE);
                findViewById(R.id.btnStartTrip).setVisibility(View.GONE);
                txtTripDistance.setText("0.0");
                break;
            case R.id.btnHere:
                updateProgressDialog(true);
                new AppStateContext().doDriverHere();
                break;
            case R.id.btnStartTrip:
                updateProgressDialog(true);
                new AppStateContext().doStartTrip();
                AppStateContext.State.setInTrip(true);
                break;
            case R.id.btnEndRequest:
                AppStateContext.changeState(AppStateContext.STATE_FREE);
                break;
            case R.id.imagebutton:
                SendNewMessageDialog dialog = new SendNewMessageDialog();
                dialog.show(getFragmentManager(), getString(R.string.main_activity_send_new_message_dialog_title));
                break;
            case R.id.btnNavigateToClient:
                new MapHelper().gotoLocation(this, AppStateContext.State.getClient().Lat, AppStateContext.State.getClient().Lng);
                break;
            case R.id.ImgClientWaitCall:
                new CallHelper().call(this, AppStateContext.State.getClient().Number);
                break;
            case R.id.btnAccept:
                if (AppStateContext.State.isInCall()) {
                    if (DriverService.appStateContext != null) {
                        AppStateContext.doCallReply(true);
                        AppStateContext.startClientResponseWaitTimer();
                    }
                    showAnimation(R.id.WaitPopup);
                }
                break;
            case R.id.btnCancel:
                if (AppStateContext.State.isInCall()) {
                    AppStateContext.doCallReply(false);
                    clearTheMap();
                    AppStateContext.counting = false;
                    AppStateContext.State.setClientResponseWaitCounter(0);
                    AppStateContext.State.setInCall(false);
                    showAnimation(R.id.btnBusy);
                    if (myMenuFragment != null)
                        myMenuFragment.setMenu_btnEnabled(true);
                }
                break;
            case R.id.btnBusy:
                new ManageServiceAsync().execute(!DriverService.isRun);
                break;
            case R.id.btnCancelRequest:
                DialogsHelper.getCancelDialog(this, this).show();
                break;
            case R.id.position:
                if (mMap!=null&&marker!=null)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude), 14.0f));
                break;
        }
    }

    @Override
    public void onCancelReason(int reasonPosition) {
        AppStateContext.doCancelRequest(AppStateContext.State.getCallId(), getResources().getStringArray(R.array.cancel_reason)[reasonPosition]);
    }

    public class ManageServiceAsync extends AsyncTask<Boolean, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            mDialog = DialogsHelper.getProgressDialog(MainActivity.this, getString(R.string.please_wait), getString(R.string.please_wait));
            mDialog.show();
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            DriverService.forceStop = !params[0];
            DriverService.isRun = !params[0];
            if (params[0])
                startService(new Intent(MainActivity.this, DriverService.class));
            else {
                if (DriverService.appStateContext != null) {
                    if (HubFactory.connection != null)
                        HubFactory.connection.clearInvocationCallbacks("Force Stop");
                    DriverService.appStateContext.doStopConnection();
                }
                try {
                    Thread.sleep(3000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                DriverService.forceStop = true;
                DriverService.isRun = false;
                stopService(new Intent(MainActivity.this, DriverService.class));
            }
            BroadcastHelper.sendInform(BroadcastHelper.UPDATE_CONNECTION_STATE_BROADCAST_METHOD);
            new SharedPrefHelper().setSharedBoolean(MainActivity.this, SharedPrefHelper.SHARED_PREFERENCE_IS_BUSY_KEY, !params[0]);
            return params[0];
        }

        @Override
        protected void onPostExecute(Boolean b) {
            boolean isBusy = new SharedPrefHelper().getSharedBoolean(MainActivity.this, SharedPrefHelper.SHARED_PREFERENCE_IS_BUSY_KEY);
            if (!isBusy)
                btnBusy.setText(getString(R.string.busy));
            else
                btnBusy.setText(getString(R.string.start_service));

            btnBusy.setTextColor(isBusy ? Color.WHITE : Color.parseColor("#6e2c69"));
            vBusy.setVisibility(isBusy ? View.VISIBLE : View.GONE);
            mDialog.cancel();
        }
    }

    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            Log.v("r", "receive " + arg1.getStringExtra(BroadcastHelper.BROADCAST_EXTRA_METHOD_NAME));
            String methodName = arg1.getStringExtra(BroadcastHelper.BROADCAST_EXTRA_METHOD_NAME);
            if (methodName != null && methodName.length() > 0) {
                Log.v("receive", methodName);
                switch (methodName) {
                    case BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME:
                        setState(AppStateContext.CURRENT_STATE);
                        break;
                    case BroadcastHelper.BAD_LOGIN_BROADCAST_METHOD:
                        BadLogin();
                        btnBusy.setVisibility(View.VISIBLE);
                        break;
                    case BroadcastHelper.UPDATE_COUNTER_BROADCAST_METHOD:
                        setWaitProgress();
                        break;
                    case BroadcastHelper.SETUP_MARKERS_BROADCAST_METHOD:
                        if (mMap != null)
                            setupMapMakers();
                        break;
                    case BroadcastHelper.UPDATE_TRIP_DISTANCE_BROADCAST_METHOD:
                        updateTripDistance();
                        break;
                    case BroadcastHelper.NOT_APPROVED_BROADCAST_METHOD:
                        DialogsHelper.getAlert(MainActivity.this, getString(R.string.info), getString(R.string.not_approved), getString(R.string.ok)).show();
                        break;
                    case BroadcastHelper.NEW_MESSAGE_BROADCAST_METHOD:
                        Message(AppStateContext.State.getLastMessage().getLastMessage());
                        break;
                    case BroadcastHelper.UPDATE_CONNECTION_STATE_BROADCAST_METHOD:
                        updateConnection();
                        break;
                    case BroadcastHelper.SECOND_LOGIN_BROADCAST_METHOD:
                        DoSecondLogin();
                        break;
                    case BroadcastHelper.CLOSE_ANY:
                        forceClose = true;
                        finish();
                        break;
                    case BroadcastHelper.CREDIT_SUBTRACT:
                        doCreditSubtract();
                        break;
                    case BroadcastHelper.CREDIT_REFUND:
                        doCreditRefund();
                        break;
                    case BroadcastHelper.UPDATE_CREDIT:
                        doUpdateCredit();
                        break;
                    case BroadcastHelper.LATER_CALL:
                        setState(AppStateContext.STATE_LATER_CALL);
                        break;
                    case BroadcastHelper.LATER_CALL_RESOPNSE:
                        setState(AppStateContext.STATE_LATER_CALL_RESPONSE);
                        break;
                    case BroadcastHelper.UPDATE_HELPER_STATE:
                        setHelperState();
                        break;
                    case BroadcastHelper.UPDATE_LATER_COUNTER_BROADCAST_METHOD:
                        updateLaterCounter();
                        break;
                    case BroadcastHelper.START_RESERVATION:
                        if (AppStateContext.CURRENT_STATE == AppStateContext.STATE_FREE && DriverService.isRun) {
                            String idString = arg1.getStringExtra(BroadcastHelper.BROADCAST_EXTRA_PARAM);
                            if (idString != null) {
                                int id = Integer.parseInt(idString);
                                LaterRequest request = (LaterRequest) new DatabaseHandler(MainActivity.this).getSingle(new LaterRequest(), id);
                                Client client = new Client(request.getClientNumber(), request.getClientName(), request.getClientNumber(), request.getStartPlace(), request.getStartLat(), request.getStartLng(),request.getPaymentType());
                                AppStateContext.State.setClient(client);
                                AppStateContext.State.setInCall(true);
                                AppStateContext.State.setCallId(request.getCallID());
                                AppStateContext.doStartLaterRequest(request.getId());
                                mMap.addMarker(new MarkerOptions().position(new LatLng(request.getStartLat(),request.getStartLng())).icon(BitmapDescriptorFactory.fromResource(R.drawable.man)));
                                AppStateContext.changeState(AppStateContext.STATE_CLIENT_CONFIRM_CALL);
                                request.setStarted(true);
                                new DatabaseHandler(MainActivity.this).updateItem(request);
                            }
                        } else {
                            DialogsHelper.showToast(MainActivity.this, getString(R.string.you_are_buzy));
                        }
                        break;
                    case BroadcastHelper.RESERVATION_CONFIRM:
                        String idString = arg1.getStringExtra(BroadcastHelper.BROADCAST_EXTRA_PARAM);
                        if (idString != null) {
                            int id = Integer.parseInt(idString);
                            LaterRequest request = (LaterRequest) new DatabaseHandler(MainActivity.this).getSingle(new LaterRequest(), id);
                            if (request != null) {
                                DialogsHelper.getAlert(MainActivity.this, getString(R.string.reservation_confirmed), request.toString(), getString(R.string.ok)).show();
                            }
                        }
                        break;
                    case BroadcastHelper.CANCEL_REQUEST_FAIL:
                        DialogsHelper.getAlert(MainActivity.this, getString(R.string.error), getString(R.string.cancel_request_fail), getString(R.string.ok)).show();
                        break;
                    case BroadcastHelper.TRIP_SUMMARY:
                        String tripSummary = arg1.getStringExtra(BroadcastHelper.BROADCAST_EXTRA_PARAM);
                        DialogsHelper.getAlert(MainActivity.this, getString(R.string.trip_summary), tripSummary, getString(R.string.ok)).show();
                        break;
                    case BroadcastHelper.GPS_NOT_AVAILABLE_BROADCAST_METHOD:
                        DialogsHelper.getAlert(MainActivity.this, getString(R.string.no_gps), getString(R.string.open_gps), getString(R.string.ok)).show();
                        break;
                }
            }
        }
    }

    private void doUpdateCredit() {
        Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
        ((TextView) findViewById(R.id.tvCredit)).setText(getString(R.string.current_credit) + " " + String.format("%d", profile.getCredit()));
    }

    private void updateLaterCounter() {
        ((ProgressBar) findViewById(R.id.prgLaterCall)).setProgress(AppStateContext.State.getLaterCallResponseWaitCounter());
    }

    private void setHelperState() {
        switch (AppStateContext.HELPER_STATE) {
            case AppStateContext.STATE_DRIVER_FAIL_SENDING_MESSAGE:
                updateProgressDialog(false);
                DialogsHelper.getAlert(this, getString(R.string.error), getString(R.string.fail_to_send_message), getString(R.string.ok)).show();
                break;
            case AppStateContext.STATE_DRIVER_SUCCESS_SENDING_MESSAGE:
                updateProgressDialog(false);
                DialogsHelper.showToast(this, getString(R.string.message_send_successfully));
                break;
            case AppStateContext.STATE_DRIVER_FAIL_SENDING_AM_HERE:
                updateProgressDialog(false);
                btnStartTrip.setVisibility(View.GONE);
                findViewById(R.id.btnEndRequest).setVisibility(View.GONE);
                btnHere.setVisibility(View.VISIBLE);
                findViewById(R.id.btnCancelRequest).setVisibility(View.VISIBLE);
                DialogsHelper.getAlert(this, getString(R.string.error), getString(R.string.fail_to_send_am_here), getString(R.string.ok)).show();
                break;
            case AppStateContext.STATE_DRIVER_SUCCESS_SENDING_AM_HERE:
                btnHere.setVisibility(View.GONE);
                findViewById(R.id.btnCancelRequest).setVisibility(View.GONE);
                Profile profile = (Profile) new DatabaseHandler(this).getSingle(new Profile(), 1);
                if (profile.isServiceProvider()){
                    btnEndRequest.setVisibility(View.VISIBLE);
                }else {
                    updateProgressDialog(true);
                    btnStartTrip.setVisibility(View.VISIBLE);
                }
                updateProgressDialog(false);
                DialogsHelper.showToast(this, getString(R.string.message_send_successfully));
                break;
            case AppStateContext.STATE_DRIVER_FAIL_SENDING_START_TRIP:
                updateProgressDialog(false);
                DialogsHelper.getAlert(this, getString(R.string.error), getString(R.string.fail_to_send_start_trip), getString(R.string.ok)).show();
                break;
            case AppStateContext.STATE_DRIVER_SUCCESS_SENDING_START_TRIP:
                updateProgressDialog(false);
                DialogsHelper.showToast(this, getString(R.string.message_send_successfully));
                break;
        }
    }
}