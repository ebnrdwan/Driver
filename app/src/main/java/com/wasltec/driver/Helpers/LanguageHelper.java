package com.wasltec.driver.Helpers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.view.View;

import java.util.Locale;

public class LanguageHelper {
    public static final String ENGLISH_LANGUAGE = "en";
    public static final String ARABIC_LANGUAGE = "ar";
    private static final String LANGUAGE_SHARED_KEY = "language";

    public String getCurrentLanguage(Context context) {
        return context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getString(LANGUAGE_SHARED_KEY, ENGLISH_LANGUAGE);
    }

    public int getCurrentLanguagePosition(Context context) {
        String lang = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE).getString(LANGUAGE_SHARED_KEY, ENGLISH_LANGUAGE);
        int position = 0;
        if (!lang.equalsIgnoreCase(ENGLISH_LANGUAGE))
            position = (lang.equalsIgnoreCase(ARABIC_LANGUAGE)) ? 1 : 2;
        return position;
    }

    public void initLanguage(Activity context, boolean enforceRtl) {
        String currentLanguage = context.getBaseContext().getResources().getConfiguration().locale.getLanguage();
        String language = new SharedPrefHelper().getSharedString(context, SharedPrefHelper.SHARED_PREFERENCE_LANGUAGE_KEY);
        if (currentLanguage.equalsIgnoreCase(language))
            return;
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getBaseContext().getResources().updateConfiguration(config,
                context.getBaseContext().getResources().getDisplayMetrics());
        if (enforceRtl) {
            if (language.equalsIgnoreCase(ARABIC_LANGUAGE) )
                this.forceRTLIfSupported(context, true);

        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported(Activity context, boolean replace) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && replace)
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 && !replace)
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }

    public void changeLanguage(Activity context) {
        SharedPrefHelper helper = new SharedPrefHelper();
        String language = helper.getSharedString(context, SharedPrefHelper.SHARED_PREFERENCE_LANGUAGE_KEY);

        String newLanguage = (language.equals(ENGLISH_LANGUAGE)) ? ARABIC_LANGUAGE : ENGLISH_LANGUAGE;
        helper.setSharedString(context, SharedPrefHelper.SHARED_PREFERENCE_LANGUAGE_KEY, newLanguage);
    }

    public void changeLanguage(Activity context, String newLanguage) {
        SharedPrefHelper helper = new SharedPrefHelper();
        helper.setSharedString(context, SharedPrefHelper.SHARED_PREFERENCE_LANGUAGE_KEY, newLanguage);
    }

    public void changeLanguage(Activity context, int newLanguagePosition) {
        SharedPrefHelper helper = new SharedPrefHelper();
        String newLanguage = ENGLISH_LANGUAGE;
        if (newLanguagePosition == 1)
            newLanguage =  ARABIC_LANGUAGE ;
        helper.setSharedString(context, SharedPrefHelper.SHARED_PREFERENCE_LANGUAGE_KEY, newLanguage);
    }
}
