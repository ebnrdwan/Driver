package com.wasltec.driver.Helpers.Service;

import android.graphics.Bitmap;


public class UploadRequest {
    Bitmap Image;
    String number;
    String Code;

    public Bitmap getImage() {
        return Image;
    }

    public String getCode() {
        return Code;
    }

    public String getNumber() {
        return number;
    }

    public void setCode(String code) {
        Code = code;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
