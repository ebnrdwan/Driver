package com.wasltec.driver.Helpers;

import android.util.Log;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Models.Balance;
import com.wasltec.driver.Models.Client;
import com.wasltec.driver.Models.LaterRequest;
import com.wasltec.driver.Models.Profile;
import com.wasltec.driver.Models.Trip;
import com.wasltec.driver.Models.TripSummary;
import com.wasltec.driver.R;

import java.util.Date;

public class HubSubscriber {

    public void LaterCall(String msgId, int reservationId, String clientName, String clientNumber, double endLat, double endLng, String endPlace, String startDate, double startLat, double startLng, String startPlace, int paymentType,int callId) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(msgId)) {
            AppStateContext.messageIds.add(msgId);
            if (!AppStateContext.State.isInCall() && !AppStateContext.State.isInTrip()) {
                LaterRequest call = new LaterRequest(reservationId, clientName, clientNumber, startDate, endLat, endLng, endPlace, startPlace, paymentType, startLat, startLng,callId);
                call.setId(reservationId);
                AppStateContext.State.setLaterCall(call);
                AppStateContext.changeState(AppStateContext.STATE_LATER_CALL);
            }
        }
    }

    public void call(final String messageId, final int _callId, final float lat, final float lng, final String Number, final String name, final String address, final String number, double endLat, double endLng, String endAddress, int paymentType) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.isRun) {
                try {
                    if (!AppStateContext.State.isInCall() && !AppStateContext.State.isInTrip()) {
                        AppStateContext.State.setInCall(true);
                        AppStateContext.State.setClient(new Client(Number, name, number, address, lat, lng, paymentType));
                        AppStateContext.State.setCallId(_callId);
                        AppStateContext.State.setCallTimerCounting(true);
                        AppStateContext.changeState(AppStateContext.STATE_INCOMING_CALL);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void Subtract(final String messageId, int amount, int credit) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            Profile profile = (Profile) new DatabaseHandler(DriverService.context).getSingle(new Profile(), 1);
            profile.setCredit(credit);
            new DatabaseHandler(DriverService.context).updateItem(profile);
            AppStateContext.changeState(AppStateContext.STATE_DRIVER_CREDIT_SUBTRACT);
        }
    }

    public void Refund(final String messageId, int amount, int credit) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.isRun) {
                Profile profile = (Profile) new DatabaseHandler(DriverService.context).getSingle(new Profile(), 1);
                profile.setCredit(credit);
                new DatabaseHandler(DriverService.context).updateItem(profile);
                AppStateContext.changeState(AppStateContext.STATE_DRIVER_CREDIT_REFUND);
            }
        }
    }

    public void ClientResponse(final String messageId, final boolean accept, final String userName) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.isRun) {
                if (!AppStateContext.State.isInTrip() && AppStateContext.State.isInCall() && AppStateContext.State.getClient() != null && AppStateContext.State.getClient().UserName.equals(userName)) {
                    AppStateContext.State.setCallTimerCounting(false);
                    AppStateContext.State.setClientResponseWaitCounter(0);
                    AppStateContext.State.setCallResponseWaitCounter(0);
                    AppStateContext.changeState(accept ? AppStateContext.STATE_CLIENT_CONFIRM_CALL : AppStateContext.STATE_CLIENT_IGNORE_CALL);
                }
            }
        }
    }

    public void CancelWait(String messageId, String userName) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.isRun) {
                AppStateContext.State.setInCall(false);
                AppStateContext.State.setInTrip(false);
                if (AppStateContext.State.getClient().UserName.equals(userName)) {
                    AppStateContext.changeState(AppStateContext.STATE_CLIENT_CANCEL);
                    AppStateContext.State.setClient(new Client());
                }
            }
        }
    }

    public void Starttrip(final String messageId, final boolean accept, final int driverId, final int tripId) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.isRun) {
                if (accept) {
                    AppStateContext.State.setTrip(new Trip(tripId));
                    new DatabaseHandler(DriverService.context).startTrip(tripId, 1);
                    AppStateContext.changeState(AppStateContext.STATE_SERVER_START_TRIP);
                    new Balance().insertTripSummary(new DatabaseHandler(DriverService.context).getWritableDatabase(), new TripSummary(tripId, new Date().getTime(), AppStateContext.State.getClient().Address, Balance.TRIP_STARTED_STATUS, 0, 0, 0, 0));
                }
            }
        }
    }

    public void ReservationConfirm(final String messageId,int reservationId, boolean accept) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.context != null && accept) {
                DatabaseHandler db = new DatabaseHandler(DriverService.context);
                LaterRequest request = (LaterRequest) db.getSingle(new LaterRequest(), reservationId);
                if (request != null) {
                    request.setApproved(true);
                    request.setMissed(false);
                    db.updateItem(request);
                    BroadcastHelper.sendInform(BroadcastHelper.RESERVATION_CONFIRM, String.valueOf(reservationId));
                }
            }
        }
    }

    public void Message(final String messageId, String number, final String message) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.isRun) {
                AppStateContext.State.getLastMessage().setLastMessage(message);
                AppStateContext.State.getLastMessage().setIsMessageSeen(false);
                AppStateContext.changeState(AppStateContext.STATE_CLIENT_SEND_MESSAGE);
            }
        }
    }

    public void TripSummary(final String messageId, int Status, int tripId, double cost, double distance, double promotionAmount, double substractedamount, double payableCash) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            new Balance().updateTripSummary(new DatabaseHandler(DriverService.context).getWritableDatabase(), new TripSummary(tripId, new Date().getTime(), AppStateContext.State.getClient().Address, Balance.TRIP_ENDED_STATUS, cost, payableCash, substractedamount, promotionAmount));
            BroadcastHelper.sendInform(BroadcastHelper.TRIP_SUMMARY, DriverService.context.getString(R.string.cost) + " : " + String.format("%d", (int) Math.ceil(cost)) + "\n" +
                    DriverService.context.getString(R.string.promotion) + " : " + String.format("%d", (int) promotionAmount) + "\n" +
                    DriverService.context.getString(R.string.subtracted) + " : " + String.format("%.2f", substractedamount) + "\n" +
                    DriverService.context.getString(R.string.payable) + " : " + String.format("%d", (int) Math.ceil(payableCash)));
        }
    }

    public void BadLogin(String msg) {
        Log.v("badLogin", msg);
        AppStateContext.changeState(AppStateContext.STATE_BAD_LOGIN);
    }

    public void BadLogin() {
        if (DriverService.isRun) {
            BroadcastHelper.sendInform(BroadcastHelper.BAD_LOGIN_BROADCAST_METHOD);
        }
    }

    public void NotApproved() {
        if (DriverService.isRun) {
            AppStateContext.changeState(AppStateContext.STATE_NOT_APPROVED);
        }
    }

    public void SecondLogin(final String messageId) {
        if (AppStateContext.messageIds != null && !AppStateContext.messageIds.contains(messageId)) {
            AppStateContext.messageIds.add(messageId);
            if (DriverService.isRun) {
                BroadcastHelper.sendInform(BroadcastHelper.SECOND_LOGIN_BROADCAST_METHOD);
            }
        }
    }
}