package com.wasltec.driver.Helpers.States;


public class ClientMessage {
    private  String mLastMessage="";
    private  boolean mIsMessageSeen;

    public String getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(String lastMessage) {
        mLastMessage = lastMessage;
    }

    public boolean isMessageSeen() {
        return mIsMessageSeen;
    }

    public void setIsMessageSeen(boolean isMessageSeen) {
        mIsMessageSeen = isMessageSeen;
    }
}
