package com.wasltec.driver.Helpers.Service;

import org.json.JSONObject;


public class PostRequest {
    JSONObject mData;
    String url;
    public PostRequest(){

    }
    public PostRequest(JSONObject _object,String _url){
        this.mData = _object;
        this.url = _url;
    }

    public JSONObject getData() {
        return mData;
    }

    public String getUrl() {
        return url;
    }

    public void setData(JSONObject data) {
        this.mData = data;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
