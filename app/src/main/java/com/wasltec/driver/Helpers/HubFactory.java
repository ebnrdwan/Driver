package com.wasltec.driver.Helpers;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.transport.WebsocketTransport;

public class HubFactory {
    public static final String START_RESERVATION = "StartReservation";
    public static final String CANCEL_REQUEST = "CancelComing";
    public static HubConnection connection = null;
    public static HubProxy hub = null;
    final static Handler h = new Handler();
    public static WebsocketTransport transport;
    //public static LongPollingTransport mLongPooltransport;

    public static Timer mTimer = new Timer();
    public static Context context = null;
    public static void setContext(Context ctx) {
        context = ctx;
    }
    public static final String UPDATE_LOCATION_METHOD = "updateLocation";
    public static final String DRIVER_FREE_METHOD = "driverAmFree";
    public static final String UPDATE_TRIP_METHOD = "UpdateTrip";
    public static final String DRIVER_UPDATE_TRIP_METHOD = "DriverStartTrip";
    public static final String DRIVER_END_TRIP_METHOD = "DriverEndTrip";
    public static final String DRIVER_CALL_REPLY_METHOD = "DriverCallReply";
    public static final String DRIVER_HERE_METHOD = "driverHere";
    public static final String DRIVER_STOP_METHOD = "StopDriver";
    public static final String DRIVER_SEND_MESSAGE_METHOD = "DriverSendMessage";
    public static final String DRIVER_UPDATE_CREDIT_METHOD = "UpdateCredit";
    public static SignalRFuture<Void> awaitConnection = null;

    public HubProxy getTheHub(final String accessToken) {
        try {
            if (hub != null) {
                return hub;
            }
            Platform.loadPlatformComponent(new AndroidPlatformComponent());
            String host = "http://admin3.waselni.com";
            connection = new HubConnection(host, "", true, new Logger() {
                @Override
                public void log(String s, LogLevel logLevel) {
                    Log.v("Signalr", s);
                }
            });
            connection.getHeaders().put("Authorization", "Bearer " + accessToken);
            connection.stateChanged(new StateChangedCallback() {
                @Override
                public void stateChanged(ConnectionState connectionState, ConnectionState connectionState2) {
                    BroadcastHelper.sendInform(BroadcastHelper.UPDATE_CONNECTION_STATE_BROADCAST_METHOD);
                    if (connectionState2 == ConnectionState.Connected) {
                        AppStateContext.requireForceUpdate = true;
                        AppStateContext.doUpdateLocation();
                    }
                }
            });
            hub = connection.createHubProxy("taxiHub");
            transport = new WebsocketTransport(new Logger() {
                @Override
                public void log(String s, LogLevel logLevel) {
                    Log.v("ws", s);
                }
            });
            /*mLongPooltransport = new LongPollingTransport(new Logger() {
                @Override
                public void log(String s, LogLevel logLevel) {
                    Log.v("ws", s);
                }
            });*/

            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (connection!=null&&connection.getState() == ConnectionState.Disconnected && !DriverService.forceStop) {
                        try {
                            connection.getHeaders().put("Authorization", "Bearer " + accessToken);
                            connection.start(transport);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }, 30000, 30000);
            connection.closed(new Runnable() {
                @Override
                public void run() {
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.v("Mustafa", "!!!!!!!!!!!!!!!!!!! retry");
                            if (connection.getState() != ConnectionState.Connected && !DriverService.forceStop) {
                                try {
                                    connection.getHeaders().put("Authorization", "Bearer " + accessToken);
                                    connection.start(transport);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    }, 5000);
                }
            });
            SignalRFuture<Void> awaitConnection = connection.start(transport);
            try {
                awaitConnection.get();

            } catch (InterruptedException e) {
                Log.v("errors", "Interrupted");
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.v("Errors", "Execution");
                e.printStackTrace();
            }
            return hub;
        } catch (Exception ex) {
            return getTheHub(accessToken);
        }
    }
}