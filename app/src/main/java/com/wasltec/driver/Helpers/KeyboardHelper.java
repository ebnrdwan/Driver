package com.wasltec.driver.Helpers;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;


public class KeyboardHelper {
    public void Hide(Context context, Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (activity != null && activity.getCurrentFocus() != null)
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
