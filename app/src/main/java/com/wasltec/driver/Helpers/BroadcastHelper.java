package com.wasltec.driver.Helpers;


import android.content.Intent;

public class BroadcastHelper {
    public static final String BROADCAST_EXTRA_METHOD_NAME = "method";
    public static final String UPDATE_CONNECTION_STATE_BROADCAST_METHOD = "updateConnection";
    public static final String BAD_LOGIN_BROADCAST_METHOD = "BadLogin";
    public static final String GPS_NOT_AVAILABLE_BROADCAST_METHOD = "NoGPS";
    public static final String SET_STATE_BROARD_CAST_METHOD_NAME = "setState";
    public static final String UPDATE_COUNTER_BROADCAST_METHOD = "updateCounter";
    public static final String SETUP_MARKERS_BROADCAST_METHOD = "setupMarkers";
    public static final String UPDATE_TRIP_DISTANCE_BROADCAST_METHOD = "doUpdateTrip";
    public static final String NOT_APPROVED_BROADCAST_METHOD = "notApproved";
    public static final String NEW_MESSAGE_BROADCAST_METHOD = "newMessage";
    public static final String SECOND_LOGIN_BROADCAST_METHOD = "secondLogin";
    public static final String CLOSE_ANY = "close_any";
    public static final String CREDIT_SUBTRACT = "creditSubtract";
    public static final String CREDIT_REFUND = "creditRefund";
    public static final String LATER_CALL = "laterCall";
    public static final String LATER_CALL_RESOPNSE = "laterCallResponse";
    public static final String UPDATE_HELPER_STATE = "updateHelper";
    public static final String UPDATE_LATER_COUNTER_BROADCAST_METHOD = "updateLaterCounter";
    public static final java.lang.String UPDATE_CREDIT = "updateCreditValue";
    public static final java.lang.String START_RESERVATION = "startReservation";
    public static final String BROADCAST_EXTRA_PARAM = "param";
    public static final String RESERVATION_CONFIRM = "reservationConfirm";
    public static final String CANCEL_REQUEST_FAIL = "cancelFail";
    public static final String TRIP_SUMMARY = "tripSummary";

    public static void sendInform(String method) {
        Intent intent = new Intent();
        intent.setAction("com.wasltec.inform");
        intent.putExtra(BROADCAST_EXTRA_METHOD_NAME, method);
        try {
            if (DriverService.context != null)
                DriverService.context.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendInform(String method, String value) {
        Intent intent = new Intent();
        intent.setAction("com.wasltec.inform");
        intent.putExtra(BROADCAST_EXTRA_METHOD_NAME, method);
        if (value != null)
            intent.putExtra(BROADCAST_EXTRA_PARAM, value);
        try {
            if (DriverService.context != null)
                DriverService.context.sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
