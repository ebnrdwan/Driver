package com.wasltec.driver.Helpers.States;

import com.wasltec.driver.Models.Client;
import com.wasltec.driver.Models.CoordinatesInfo;
import com.wasltec.driver.Models.LaterRequest;
import com.wasltec.driver.Models.Trip;

public class StateStorage {
    private LaterRequest mLaterCall;
    private Client mClient;
    private Trip mTrip;
    private CoordinatesInfo mCoordinatesInfo;
    private boolean mBadLogin;
    private boolean mInCall;
    private boolean mInTrip;
    private boolean mBackground;
    private boolean mCallTimerCounting;
    private int CallId;
    private int mClientResponseWaitCounter;
    private int mCallResponseWaitCounter;
    private ClientMessage mLastMessage;
    private int mLaterCallResponseWaitCounter;


    public int getLaterCallResponseWaitCounter() {
        return mLaterCallResponseWaitCounter;
    }

    public void setLaterCallResponseWaitCounter(int laterCallResponseWaitCounter) {
        mLaterCallResponseWaitCounter = laterCallResponseWaitCounter;
    }

    public StateStorage() {
        resetData();
    }

    public void resetData() {
        setInTrip(false);
        setInCall(false);
        setBadLogin(false);
        setCoordinatesInfo(new CoordinatesInfo());
        setTrip(new Trip());
        setClient(new Client());
        setLaterCall(new LaterRequest());
        setBackground(false);
        setCallTimerCounting(false);
        setCallId(-1);
        setClientResponseWaitCounter(0);
        setLastMessage(new ClientMessage());
        setLaterCall(new LaterRequest());
        setLaterCallResponseWaitCounter(0);
    }


    public int getCallResponseWaitCounter() {
        return mCallResponseWaitCounter;
    }

    public void setCallResponseWaitCounter(int callResponseWaitCounter) {
        mCallResponseWaitCounter = callResponseWaitCounter;
    }

    public ClientMessage getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(ClientMessage lastMessage) {
        mLastMessage = lastMessage;
    }

    public int getClientResponseWaitCounter() {
        return mClientResponseWaitCounter;
    }

    public void setClientResponseWaitCounter(int clientResponseWaitCounter) {
        mClientResponseWaitCounter = clientResponseWaitCounter;
    }

    public int getCallId() {
        return CallId;
    }

    public void setCallId(int callId) {
        CallId = callId;
    }

    public boolean isCallTimerCounting() {
        return mCallTimerCounting;
    }

    public void setCallTimerCounting(boolean callTimerCounting) {
        mCallTimerCounting = callTimerCounting;
    }

    public boolean isBackground() {
        return mBackground;
    }

    public void setBackground(boolean background) {
        mBackground = background;
    }

    public LaterRequest getLaterCall() {
        return mLaterCall;
    }

    public void setLaterCall(LaterRequest laterCall) {
        mLaterCall = laterCall;
    }

    public Client getClient() {
        return mClient;
    }

    public void setClient(Client client) {
        mClient = client;
    }

    public Trip getTrip() {
        return mTrip;
    }

    public void setTrip(Trip trip) {
        mTrip = trip;
    }

    public CoordinatesInfo getCoordinatesInfo() {
        return mCoordinatesInfo;
    }

    public void setCoordinatesInfo(CoordinatesInfo coordinatesInfo) {
        mCoordinatesInfo = coordinatesInfo;
    }

    public boolean isBadLogin() {
        return mBadLogin;
    }

    public void setBadLogin(boolean badLogin) {
        mBadLogin = badLogin;
    }

    public boolean isInCall() {
        return mInCall;
    }

    public void setInCall(boolean inCall) {
        mInCall = inCall;
    }

    public boolean isInTrip() {
        return mInTrip;
    }

    public void setInTrip(boolean inTrip) {
        mInTrip = inTrip;
    }
}
