package com.wasltec.driver.Helpers;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import com.wasltec.driver.R;

public class CallHelper {
    public void call(Activity activity, String number) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(activity, R.string.permission_call_deined,Toast.LENGTH_LONG).show();
                return;
            }
        }
        activity.startActivity(callIntent);
    }
}
