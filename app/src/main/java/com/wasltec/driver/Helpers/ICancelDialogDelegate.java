package com.wasltec.driver.Helpers;


public interface ICancelDialogDelegate {
    void onCancelReason(int reasonPosition);
}
