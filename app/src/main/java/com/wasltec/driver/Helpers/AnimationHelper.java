package com.wasltec.driver.Helpers;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.TranslateAnimation;

public class AnimationHelper {
    public void translateAnimation(View view,float fromXDelta,float toXDelta,float fromYDelta,float toYDelta,long startDelay,long duration) {
view.setClickable(true);
        TranslateAnimation animation = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta, toYDelta);
        animation.setDuration(duration);
        animation.setStartOffset(startDelay);
        animation.setFillAfter(true);

        view.startAnimation(animation);

    }
    public void ShowAnimated(View view) {


        view.setVisibility(View.VISIBLE);
        view.animate()
                .translationY(0)
                .alpha(1.0f);


    }
    public void HideAnimated(View view,View parentView) {

        //view.setAlpha(0.0f);
        //view.setVisibility(View.VISIBLE);
        view.animate()
                .translationY(parentView.getHeight())
                .alpha(0.2f);
        view.setVisibility(View.GONE);


    }
    public void objectFloatAnimation(View view,String property,float value,long startDelay,long duration){
        ObjectAnimator animator = ObjectAnimator.ofFloat(view, property, value);

        animator.setStartDelay(startDelay);
        animator.setDuration(duration);

        animator.start();
    }
}
