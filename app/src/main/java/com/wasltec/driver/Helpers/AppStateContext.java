package com.wasltec.driver.Helpers;

import android.content.Intent;
import android.location.Location;
import android.util.Log;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.States.StateStorage;
import com.wasltec.driver.Activities.MainActivity;
import com.wasltec.driver.Models.Client;
import com.wasltec.driver.Models.HistoryTrip;
import com.wasltec.driver.Models.Profile;
import com.wasltec.driver.Models.Trip;
import com.wasltec.driver.Models.TripCoordinates;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;

public class AppStateContext {
    public static final int STATE_DRIVER_CANCEL_CALL = 201;
    public static final int STATE_DRIVER_ACCEPT_CALL = 202;
    public static final int STATE_CLIENT_CONFIRM_CALL = 203;
    public static final int STATE_CLIENT_IGNORE_CALL = 204;
    public static final int STATE_CLIENT_SEND_MESSAGE = 301;
    public static final int STATE_DRIVER_FAIL_SENDING_MESSAGE = 1002;
    public static final int STATE_DRIVER_SEND_AM_HERE = 401;
    public static final int STATE_DRIVER_FAIL_SENDING_AM_HERE = 402;
    public static final int STATE_DRIVER_SUCCESS_SENDING_AM_HERE = 403;
    public static final int STATE_DRIVER_SEND_START_TRIP = 501;
    public static final int STATE_DRIVER_FAIL_SENDING_START_TRIP = 502;
    public static final int STATE_DRIVER_SUCCESS_SENDING_START_TRIP = 503;
    public static final int STATE_DRIVER_SENDING_END_TRIP = 601;
    public static final int STATE_DRIVER_FAIL_SENDING_END_TRIP = 602;
    public static final int STATE_DRIVER_SUCCESS_SENDING_END_TRIP = 603;
    public static final int STATE_BAD_LOGIN = 701;
    public static final int STATE_NOT_APPROVED = 702;
    public static final int STATE_DRIVER_SENDING_AM_FREE = 801;
    public static final int STATE_DRIVER_FAIL_SENDING_AM_FREE = 802;
    public static final int STATE_DRIVER_SUCCESS_SENDING_AM_FREE = 803;
    public static final int STATE_DRIVER_SENDING_STOP_CONNECTION = 901;
    public static final int STATE_DRIVER_FAIL_SENDING_STOP_CONNECTION = 902;
    public static final int STATE_DRIVER_SUCCESS_SENDING_STOP_CONNECTION = 903;
    public static final int STATE_DRIVER_SENDING_MESSAGE = 1001;
    public static final int STATE_DRIVER_SUCCESS_SENDING_MESSAGE = 1003;
    public static final int STATE_INCOMING_CALL = 2;
    public static final int STATE_DRIVER_CREDIT_SUBTRACT = 1101;
    public static final int STATE_DRIVER_CREDIT_REFUND = 1102;
    public static final int STATE_SERVER_START_TRIP = 1103;
    private static final int STATE_DRIVER_SUCCESS_SENDING_CANCEL_REQUEST = 9901;
    private static final int STATE_DRIVER_FAIL_SENDING_CANCEL_REQUEST = 9902;
    public static ArrayList<String> messageIds;
    public static HubSubscriber mHubSubscriber;
    public final static int STATE_FREE = 1;
    public static final int STATE_CLIENT_CANCEL = 3;
    public static final int STATE_LATER_CALL = 101;
    public static final int STATE_LATER_CALL_RESPONSE = 102;
    public static boolean counting = false;
    public static StateStorage State = new StateStorage();
    public static Profile info = new Profile();
    public static long lastLocationDate;
    static Timer clientResponseWaitTime = null;
    static Timer callResponseWaitTime = null;
    static Timer generalTimer = null;
    public static int CURRENT_STATE = STATE_FREE;
    public static int HELPER_STATE = STATE_FREE;
    private static final String TAG = "APP_STATE_CONTEXT";
    public boolean updateLocationTest = false;
    private static boolean callCounterWait = false;
    private static boolean responseCounterWait = false;
    public static boolean requireForceUpdate;
    public static boolean laterCallCounterWait;
    public static Timer laterCallResponseWaitTime;

    public AppStateContext() {
        if (State == null) State = new StateStorage();
        if (messageIds == null)
            messageIds = new ArrayList<>();
        if (mHubSubscriber == null) mHubSubscriber = new HubSubscriber();
        if (generalTimer == null) {
            generalTimer = new Timer();
            generalTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    AppStateContext.doUpdateLocation();
                }
            }, 30000, 180000);
        }
        if (clientResponseWaitTime == null)
            clientResponseWaitTime = new Timer();
    }

    public static void changeState(int state) {
        switch (state) {
            case STATE_FREE:
                CURRENT_STATE = state;
                AppStateContext.State.setClient(new Client());
                AppStateContext.State.setInCall(false);
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                doAmFree();
                doUpdateLocation();
                break;
            case STATE_INCOMING_CALL:
                CURRENT_STATE = state;
                MediaHelper.start();
                startDriverCallWaitTimer();
                new NotificationHelper().ShowCallNotification(DriverService.context);
                if (AppStateContext.State.isBackground()) {
                    BroadcastHelper.sendInform(BroadcastHelper.CLOSE_ANY);
                    Intent i = new Intent(DriverService.context, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.putExtra("Force", true);
                    DriverService.context.startActivity(i);
                } else {
                    BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                }
                break;
            case STATE_DRIVER_CANCEL_CALL:

                break;
            case STATE_DRIVER_ACCEPT_CALL:

                break;
            case STATE_CLIENT_CONFIRM_CALL:
                CURRENT_STATE = state;
                responseCounterWait = false;
                if (callResponseWaitTime != null)
                    callResponseWaitTime.cancel();
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                break;
            case STATE_CLIENT_IGNORE_CALL:
                CURRENT_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                AppStateContext.State.setInCall(false);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                changeState(STATE_FREE);
                break;
            case STATE_CLIENT_CANCEL:
                CURRENT_STATE=state;
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                break;
            case STATE_CLIENT_SEND_MESSAGE:
                new NotificationHelper().ShowMessageNotification(DriverService.context, State.getLastMessage().getLastMessage());
                BroadcastHelper.sendInform(BroadcastHelper.NEW_MESSAGE_BROADCAST_METHOD);
                break;
            case STATE_DRIVER_SENDING_MESSAGE:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_FAIL_SENDING_MESSAGE:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_SUCCESS_SENDING_MESSAGE:
                CURRENT_STATE = state;
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_SEND_AM_HERE:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_FAIL_SENDING_AM_HERE:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_SUCCESS_SENDING_AM_HERE:
                CURRENT_STATE = state;
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_SEND_START_TRIP:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_FAIL_SENDING_START_TRIP:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_SUCCESS_SENDING_START_TRIP:
                CURRENT_STATE = state;
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                break;
            case STATE_DRIVER_SENDING_END_TRIP:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_FAIL_SENDING_END_TRIP:
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                break;
            case STATE_DRIVER_SUCCESS_SENDING_END_TRIP:
                CURRENT_STATE = state;
                HELPER_STATE = state;
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_HELPER_STATE);
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                changeState(STATE_FREE);
                break;
            case STATE_SERVER_START_TRIP:
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                break;
            case STATE_BAD_LOGIN:
                break;
            case STATE_NOT_APPROVED:
                BroadcastHelper.sendInform(BroadcastHelper.NOT_APPROVED_BROADCAST_METHOD);
                break;
            case STATE_DRIVER_SENDING_AM_FREE:
                break;
            case STATE_DRIVER_FAIL_SENDING_AM_FREE:
                break;
            case STATE_DRIVER_SUCCESS_SENDING_AM_FREE:
                break;
            case STATE_DRIVER_SENDING_STOP_CONNECTION:
                break;
            case STATE_DRIVER_FAIL_SENDING_STOP_CONNECTION:
                break;
            case STATE_DRIVER_SUCCESS_SENDING_STOP_CONNECTION:
                break;
            case STATE_DRIVER_CREDIT_SUBTRACT:
                BroadcastHelper.sendInform(BroadcastHelper.CREDIT_SUBTRACT);
                break;
            case STATE_DRIVER_CREDIT_REFUND:
                BroadcastHelper.sendInform(BroadcastHelper.CREDIT_REFUND);
                break;
            case STATE_LATER_CALL:
                CURRENT_STATE = state;
                MediaHelper.start();
                startDriverLaterCallWaitTimer();
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                break;
            case STATE_LATER_CALL_RESPONSE:
                changeState(STATE_FREE);
                MediaHelper.stop();
                BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                break;
            case STATE_DRIVER_SUCCESS_SENDING_CANCEL_REQUEST:
                break;
            case STATE_DRIVER_FAIL_SENDING_CANCEL_REQUEST:
                BroadcastHelper.sendInform(BroadcastHelper.CANCEL_REQUEST_FAIL);
                break;
        }
    }

    public void destroyData() {
        counting = false;
        info = new Profile();
        CURRENT_STATE = STATE_FREE;
        lastLocationDate = 0;
        updateLocationTest = false;
        callCounterWait = false;
        if (clientResponseWaitTime != null)
            clientResponseWaitTime.cancel();
        if (generalTimer != null)
            generalTimer.cancel();
    }

    public static void startDriverLaterCallWaitTimer() {
        laterCallCounterWait = true;
        laterCallResponseWaitTime = new Timer();
        laterCallResponseWaitTime.schedule(new TimerTask() {
            @Override
            public void run() {
                if (laterCallCounterWait) {
                    State.setLaterCallResponseWaitCounter(State.getLaterCallResponseWaitCounter() + 1);
                    BroadcastHelper.sendInform(BroadcastHelper.UPDATE_LATER_COUNTER_BROADCAST_METHOD);
                    if (State.getLaterCallResponseWaitCounter() >= 120) {
                        doLaterCall(false);
                        MediaHelper.stop();
                        changeState(STATE_FREE);
                        laterCallCounterWait = false;
                        State.setLaterCallResponseWaitCounter(0);
                        laterCallResponseWaitTime.cancel();
                    }
                } else {
                    State.setLaterCallResponseWaitCounter(0);
                    laterCallResponseWaitTime.cancel();
                }
            }
        }, 1000, 1000);
    }

    public static void startDriverCallWaitTimer() {
        callCounterWait = true;
        callResponseWaitTime = new Timer();
        callResponseWaitTime.schedule(new TimerTask() {
            @Override
            public void run() {
                if (callCounterWait) {
                    State.setCallResponseWaitCounter(State.getCallResponseWaitCounter() + 1);
                    BroadcastHelper.sendInform(BroadcastHelper.UPDATE_COUNTER_BROADCAST_METHOD);
                    if (State.getCallResponseWaitCounter() >= 30) {
                        MediaHelper.stop();
                        changeState(STATE_FREE);
                        callCounterWait = false;
                        State.setCallResponseWaitCounter(0);
                        callResponseWaitTime.cancel();
                    }
                } else {
                    State.setCallResponseWaitCounter(0);
                    callResponseWaitTime.cancel();
                }
            }
        }, 1000, 1000);
    }

    public static void startClientResponseWaitTimer() {
        responseCounterWait = true;
        clientResponseWaitTime = new Timer();
        clientResponseWaitTime.schedule(new TimerTask() {
            @Override
            public void run() {
                if (responseCounterWait) {
                    State.setClientResponseWaitCounter(State.getClientResponseWaitCounter() + 1);
                    BroadcastHelper.sendInform(BroadcastHelper.UPDATE_COUNTER_BROADCAST_METHOD);
                    if (State.getClientResponseWaitCounter() >= 30) {
                        AppStateContext.State.setClient(new Client());
                        AppStateContext.State.setInCall(false);
                        changeState(STATE_CLIENT_IGNORE_CALL);
                        callCounterWait = false;
                        State.setClientResponseWaitCounter(0);
                        clientResponseWaitTime.cancel();
                    }
                } else {
                    State.setClientResponseWaitCounter(0);
                    clientResponseWaitTime.cancel();
                }
            }
        }, 1000, 1000);
    }

    public void LocationChanged(Location location) {
        if (State.getCoordinatesInfo().OldCoordinates.Lat == 0 || State.getCoordinatesInfo().OldCoordinates.Lon == 0) {
            requireForceUpdate = true;
            State.getCoordinatesInfo().OldCoordinates.Lat = location.getLatitude();
            State.getCoordinatesInfo().OldCoordinates.Lon = location.getLongitude();
        } else {
            State.getCoordinatesInfo().OldCoordinates.Lat = State.getCoordinatesInfo().CurrentCoordinates.Lat;
            State.getCoordinatesInfo().OldCoordinates.Lon = State.getCoordinatesInfo().CurrentCoordinates.Lon;
        }
        State.getCoordinatesInfo().CurrentCoordinates.Lat = location.getLatitude();
        State.getCoordinatesInfo().CurrentCoordinates.Lon = location.getLongitude();
        BroadcastHelper.sendInform(BroadcastHelper.SETUP_MARKERS_BROADCAST_METHOD);
        float[] newDistance = new float[99];
        Location.distanceBetween(State.getCoordinatesInfo().OldCoordinates.Lat, State.getCoordinatesInfo().OldCoordinates.Lon, State.getCoordinatesInfo().CurrentCoordinates.Lat, State.getCoordinatesInfo().CurrentCoordinates.Lon, newDistance);
        float theNewDistance = newDistance[0];
        if (theNewDistance > location.getAccuracy())
            theNewDistance -= (location.getAccuracy() / 2);
        else
            theNewDistance = 0;
        Log.v("newDistance", theNewDistance + "");
        if (State.isInTrip() && theNewDistance > 0) {
            TripCoordinates coordinates = new TripCoordinates();
            coordinates.time = new Date();
            coordinates.fkTripId = State.getTrip().getId();
            coordinates.setLatitude(location.getLatitude());
            coordinates.setLongitude(location.getLongitude());
            new DatabaseHandler(DriverService.context).addTripCoordinates(coordinates);
            AppStateContext.State.getTrip().setMeterDistance(State.getTrip().getMeterDistance() + (int) theNewDistance);
            AppStateContext.State.getTrip().setKMDistance(((float) AppStateContext.State.getTrip().getMeterDistance()) / 1000f);
            AppStateContext.State.getTrip().setCost((float) info.getOpenDoorPrice() + ((float) ((((float) AppStateContext.State.getTrip().getMeterDistance() / 1000F)) * info.getKilometerCost()) + ((State.getTrip().getWaitSeconds() / 60) * State.getTrip().getWaitPerMinuteCost())));
            Log.v(TAG, "new distance " + AppStateContext.State.getTrip().getKMDistance() + ", " + theNewDistance + ", " + State.getTrip().getMeterDistance());
            doUpdateTrip();
            BroadcastHelper.sendInform(BroadcastHelper.UPDATE_TRIP_DISTANCE_BROADCAST_METHOD);
        } else if (State.isInTrip() && theNewDistance <= 0 && lastLocationDate > 0) {
            Calendar calendar = Calendar.getInstance();
            long ws = calendar.getTimeInMillis() - lastLocationDate;
            ws += 1000;
            State.getTrip().setWaitSeconds(State.getTrip().getWaitSeconds() + (ws / 1000));
            AppStateContext.State.getTrip().setCost((float) info.getOpenDoorPrice() + ((float) ((((float) AppStateContext.State.getTrip().getMeterDistance() / 1000F)) * info.getKilometerCost()) + ((State.getTrip().getWaitSeconds() / 60) * State.getTrip().getWaitPerMinuteCost())));
            BroadcastHelper.sendInform(BroadcastHelper.UPDATE_TRIP_DISTANCE_BROADCAST_METHOD);
            Log.v("wait", (ws / 1000) + "");
        }
        if (!State.isInTrip()) {
            if (theNewDistance > 100 || requireForceUpdate)
                doUpdateLocation();
        }else
        {
            if (theNewDistance > 20)
                doUpdateLocation();
        }
        Calendar c = Calendar.getInstance();
        lastLocationDate = c.getTimeInMillis();
        Log.v("lastLocationDate", lastLocationDate + "");
    }

    public static void doUpdateLocation() {
        try {
            if (HubFactory.hub != null && State.getCoordinatesInfo() != null && State.getCoordinatesInfo().CurrentCoordinates.Lat > 0 && State.getCoordinatesInfo().CurrentCoordinates.Lon > 0) {
                HubFactory.hub.invoke(HubFactory.UPDATE_LOCATION_METHOD, State.getCoordinatesInfo().CurrentCoordinates.Lat, State.getCoordinatesInfo().CurrentCoordinates.Lon).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "Update Location Ok");
                        requireForceUpdate = false;
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        requireForceUpdate = true;
                        Log.v(TAG, "Update Location Error call the fail back");
                    }
                });
            } else {
                requireForceUpdate = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            requireForceUpdate = true;
        }
    }

    public static void doUpdateCredit(){
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.DRIVER_UPDATE_CREDIT_METHOD).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "Credit Updated");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "Credit Updated Error");
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void doAmFree() {
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.DRIVER_FREE_METHOD).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "Am Free Ok");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "Am Free Error");
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void doUpdateTrip() {
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.UPDATE_TRIP_METHOD, AppStateContext.State.getTrip().getId(), State.getCoordinatesInfo().CurrentCoordinates.Lat, State.getCoordinatesInfo().CurrentCoordinates.Lon).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "Update Trip Ok");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "Update Trip Error");
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doStartTrip() {
        changeState(STATE_DRIVER_SEND_START_TRIP);
        State.setInTrip(true);
        info = (Profile) new DatabaseHandler(DriverService.context).getSingle(new Profile(), 1);
        AppStateContext.State.setTrip(new Trip(AppStateContext.State.getTrip().getId(), AppStateContext.info.getOpenDoorPrice(),AppStateContext.info.getWaitingPerMinuteCost()));
        DriverService.gpsHelper.changeGPSProvider();
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.DRIVER_UPDATE_TRIP_METHOD, AppStateContext.State.getTrip().getId(), State.getCoordinatesInfo().CurrentCoordinates.getLatitude(), State.getCoordinatesInfo().CurrentCoordinates.getLongitude()).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "start Trip Done");
                        changeState(STATE_DRIVER_SUCCESS_SENDING_START_TRIP);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "start Trip Error");
                        changeState(STATE_DRIVER_FAIL_SENDING_START_TRIP);
                    }
                });
            } else {
                changeState(STATE_DRIVER_FAIL_SENDING_START_TRIP);
            }
        } catch (Exception e) {
            e.printStackTrace();
            changeState(STATE_DRIVER_FAIL_SENDING_START_TRIP);
        }
        BroadcastHelper.sendInform(BroadcastHelper.UPDATE_TRIP_DISTANCE_BROADCAST_METHOD);
    }

    public static void doEndTrip() {
        changeState(STATE_DRIVER_SENDING_END_TRIP);
        final int tripCost = (int) Math.ceil(State.getTrip().getCost());
        State.getTrip().setCost(tripCost);
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.DRIVER_END_TRIP_METHOD, AppStateContext.State.getTrip().getKMDistance(), AppStateContext.State.getTrip().getCost(), AppStateContext.State.getTrip().getId(), State.getCoordinatesInfo().CurrentCoordinates.Lat, State.getCoordinatesInfo().CurrentCoordinates.Lon).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "End Trip Done");
                        changeState(STATE_DRIVER_SUCCESS_SENDING_END_TRIP);
                        HistoryTrip trip = new HistoryTrip();
                        trip.id = State.getTrip().getId();
                        trip.ClientId = 1;
                        trip.cost = tripCost;
                        trip.distance = State.getTrip().getKMDistance();
                        trip.endTime = new Date();
                        new DatabaseHandler(DriverService.context).endTrip(trip);
                        lastLocationDate = 0;
                        //AppStateContext.setCurrentState(1);
                        State.setInTrip(false);
                        DriverService.gpsHelper.changeGPSProvider();
                        AppStateContext.State = new StateStorage();
                        BroadcastHelper.sendInform(BroadcastHelper.SET_STATE_BROARD_CAST_METHOD_NAME);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "End Trip Error");
                        changeState(STATE_DRIVER_FAIL_SENDING_END_TRIP);
                    }
                });
            } else {
                changeState(STATE_DRIVER_FAIL_SENDING_END_TRIP);
            }
        } catch (Exception e) {
            e.printStackTrace();
            changeState(STATE_DRIVER_FAIL_SENDING_END_TRIP);
        }
    }

    public static void doCallReply(boolean answer) {
        MediaHelper.stop();
        callResponseWaitTime.cancel();
        State.setCallResponseWaitCounter(0);
        counting = false;
        try {
            if (HubFactory.hub != null) {

                HubFactory.hub.invoke(HubFactory.DRIVER_CALL_REPLY_METHOD, State.getCallId(), answer, AppStateContext.State.getClient().UserName).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "Call Reply OK");

                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "Call Replay Error");
                        changeState(STATE_FREE);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!answer) {
            State.setClient(new Client());
            changeState(STATE_FREE);
        }
    }

    public void doDriverHere() {
        changeState(STATE_DRIVER_SEND_AM_HERE);
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.DRIVER_HERE_METHOD, AppStateContext.State.getClient().UserName, AppStateContext.State.getCallId()).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "doDriverHere Ok");
                        changeState(STATE_DRIVER_SUCCESS_SENDING_AM_HERE);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "doDriverHere Error");
                        changeState(STATE_DRIVER_FAIL_SENDING_AM_HERE);
                    }
                });
            } else {
                changeState(STATE_DRIVER_FAIL_SENDING_AM_HERE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            changeState(STATE_DRIVER_FAIL_SENDING_AM_HERE);
        }
    }

    public void doStopConnection() {
        changeState(STATE_DRIVER_SENDING_STOP_CONNECTION);
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.DRIVER_STOP_METHOD).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "driver stop Ok");
                        changeState(STATE_DRIVER_SUCCESS_SENDING_STOP_CONNECTION);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "driver stop Error");
                        changeState(STATE_DRIVER_FAIL_SENDING_STOP_CONNECTION);
                    }
                });
            } else {
                changeState(STATE_DRIVER_FAIL_SENDING_STOP_CONNECTION);
            }
        } catch (Exception e) {
            e.printStackTrace();
            changeState(STATE_DRIVER_FAIL_SENDING_STOP_CONNECTION);
        }
    }

    public void doSendMessage(String message) {
        changeState(STATE_DRIVER_SENDING_MESSAGE);
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.DRIVER_SEND_MESSAGE_METHOD, AppStateContext.State.getClient().Number, message).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "Send Message Ok");
                        changeState(STATE_DRIVER_SUCCESS_SENDING_MESSAGE);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "Send Message Error");
                        changeState(STATE_DRIVER_FAIL_SENDING_MESSAGE);
                    }
                });
            } else {
                changeState(STATE_DRIVER_FAIL_SENDING_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            changeState(STATE_DRIVER_FAIL_SENDING_MESSAGE);
        }
    }

    public static void doLaterCall(final boolean accept) {
        State.setLaterCallResponseWaitCounter(0);
        laterCallResponseWaitTime.cancel();
        MediaHelper.stop();
        try {
            if (HubFactory.hub != null) {
                if (accept) {
                    new DatabaseHandler(DriverService.context).updateItem(State.getLaterCall());
                }
                HubFactory.hub.invoke("DriverReservationReply", accept, State.getLaterCall().getId()).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "later call Ok");

                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "later call Error");
                    }
                });
            }
            changeState(AppStateContext.STATE_FREE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void doStartLaterRequest(int id) {
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.START_RESERVATION, id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "doDriverHere Ok");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "doDriverHere Error");
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void doCancelRequest(int callId, String reason) {
        try {
            if (HubFactory.hub != null) {
                HubFactory.hub.invoke(HubFactory.CANCEL_REQUEST, reason, callId).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(TAG, "cancel request Ok");
                        changeState(STATE_FREE);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(TAG, "cancel request Error");
                        changeState(STATE_DRIVER_FAIL_SENDING_CANCEL_REQUEST);
                    }
                });
            } else {
                changeState(STATE_DRIVER_FAIL_SENDING_CANCEL_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
            changeState(STATE_DRIVER_FAIL_SENDING_CANCEL_REQUEST);
        }
    }
}