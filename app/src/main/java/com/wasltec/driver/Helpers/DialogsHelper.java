package com.wasltec.driver.Helpers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;
import com.wasltec.driver.R;

public class DialogsHelper {

    public static AlertDialog getAlert(Activity activity, String title, String message, String okButton) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(title).setMessage(message);
        dialog.setPositiveButton(okButton, null);
        return dialog.create();
    }

    public static ProgressDialog getProgressDialog(Context activity, String title, String message) {
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(title);
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public static ProgressDialog getProgressDialog(Activity activity, int title) {
        ProgressDialog dialog = new ProgressDialog(activity);
        dialog.setMessage(activity.getString(title));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public static Snackbar getSnackBar(View view, String message) {
        return Snackbar.make(view, message, Snackbar.LENGTH_LONG);
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static AlertDialog getCancelDialog(Context context, final ICancelDialogDelegate delegate) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.cancel_reseon)
                .setItems(R.array.cancel_reason, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        delegate.onCancelReason(which);
                    }
                });
        return builder.create();
    }
}
