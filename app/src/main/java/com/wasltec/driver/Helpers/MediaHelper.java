package com.wasltec.driver.Helpers;


import android.media.MediaPlayer;

import com.wasltec.driver.R;

public abstract class MediaHelper {
    private static MediaPlayer mediaPlayer;


    private static void setup() {
        if(DriverService.context != null && mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(DriverService.context, R.raw.waselni);
            mediaPlayer.setLooping(true);
        }
    }

    public static void stop(){
        setup();
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }
    public static void start(){
        setup();
        mediaPlayer.start();
    }
}
