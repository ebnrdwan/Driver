package com.wasltec.driver.Helpers;

import android.graphics.Color;
import com.wasltec.driver.Helpers.Service.CallWebAPI;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import org.json.JSONArray;
import org.json.JSONObject;

public class RouteHelper {
    public PolylineOptions getRoutes(double startLat, double startLng, double endLat, double endLng) {
        PolylineOptions options = new PolylineOptions();
        try {
            String x = new CallWebAPI().execute("http://maps.googleapis.com/maps/api/directions/json?origin=" + startLat + "," + startLng + "&destination=" + endLat + "," + endLng + "&sensor=false").get();
            JSONObject ob = new JSONObject(x);
            if (ob.has("routes")) {
                JSONArray route = ob.getJSONArray("routes");
                JSONObject start = route.optJSONObject(0);
                if (start.has("legs")) {
                    JSONArray legs = start.getJSONArray("legs");
                    JSONObject leg = legs.getJSONObject(0);
                    if (leg.has("steps")) {
                        JSONArray steps = leg.getJSONArray("steps");
                        for (int i = 0; i < steps.length(); i++) {
                            if (steps.optJSONObject(i).has("start_location")) {
                                JSONObject startLocation = steps.optJSONObject(i).getJSONObject("start_location");
                                double lat = startLocation.getDouble("lat");
                                double lng = startLocation.getDouble("lng");
                                options.add(new LatLng(lat, lng));
                                JSONObject endLocation = steps.getJSONObject(i).getJSONObject("end_location");
                                lat = endLocation.getDouble("lat");
                                lng = endLocation.getDouble("lng");
                                options.add(new LatLng(lat, lng));
                            }
                        }
                        options.width(10)
                                .color(Color.BLUE)
                                .geodesic(true);
                    }
                }
            }
        } catch (Exception ex) {}
        return options;
    }
}