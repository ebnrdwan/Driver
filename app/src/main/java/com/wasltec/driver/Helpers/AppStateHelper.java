package com.wasltec.driver.Helpers;


import android.content.Context;
import android.util.Log;

import com.wasltec.driver.R;

public class AppStateHelper {
    public static String getAppState(Context context, boolean isServiceRunning) {
        String connectionState = context.getString(R.string.main_activity_hub_disconnected_title);
        Log.v("AppStateHelper"," "+(HubFactory.connection == null));
        if (HubFactory.connection != null) {
            Log.v("AppStateHelper",HubFactory.connection.getState().name());
            switch (HubFactory.connection.getState()) {
                case Connected:
                    connectionState = context.getString(R.string.main_activity_hub_connected_title);
                    break;
                case Reconnecting:
                    connectionState = context.getString(R.string.reconnecting);
                    break;
                case Connecting:
                    connectionState = context.getString(R.string.main_activity_hub_connecting_title);
                    break;
                case Disconnected:
                    connectionState = context.getString(R.string.main_activity_hub_disconnected_title);
                    break;
            }
        }
        String serviceState = context.getString(R.string.main_activity_service_not_running_title);
        if (isServiceRunning)
            serviceState = context.getString(R.string.main_activity_service_is_running_title);
        return connectionState + " - " + serviceState;

    }
}
