package com.wasltec.driver.Helpers.GPS;


import android.location.Location;

public interface IGpsListener {
    void gpsUpdate(Location location);
}
