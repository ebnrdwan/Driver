package com.wasltec.driver.Helpers.UiHelpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

public class SlideButton extends SeekBar {

    public int ref =0;
    private Drawable thumb;
    private SlideButtonListener listener;

    public SlideButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        this.thumb = thumb;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (thumb.getBounds().contains((int) event.getX(), (int) event.getY())) {
                super.onTouchEvent(event);
            } else
                return false;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if (getProgress() > 70)
                handleSlide(ref);
            else
                setProgress(0);
           // setProgress(0);
        } else
            super.onTouchEvent(event);

        return true;
    }

    private void handleSlide(int tag) {
        listener.handleSlide(tag);
    }

    public void setSlideButtonListener(SlideButtonListener listener) {
        this.listener = listener;
    }
    public void setSlideButtonListener(SlideButtonListener listener,int ref) {
        this.ref = ref;
        this.listener = listener;
    }
}

