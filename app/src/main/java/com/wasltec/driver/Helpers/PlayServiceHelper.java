package com.wasltec.driver.Helpers;


import android.app.Activity;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.wasltec.driver.R;

public class PlayServiceHelper {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                DialogsHelper.getAlert(activity,activity.getString(R.string.error),activity.getString(R.string.device_not_supported),activity.getString(R.string.ok)).show();
            }
            return false;
        }
        return true;
    }
}
