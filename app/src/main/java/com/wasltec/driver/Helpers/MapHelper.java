package com.wasltec.driver.Helpers;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

public class MapHelper {
    public void gotoLocation(Activity activity, double lat, double lng) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + AppStateContext.State.getClient().Lat + "," + AppStateContext.State.getClient().Lng));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }
}
