package com.wasltec.driver.Helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateHelper {
	
	public static String formatDate(Date date){
		if(date == null) return null;
		SimpleDateFormat format;

		format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
	}
    public static String formatDateTime(Date date){
        if(date == null) return null;
        SimpleDateFormat format;

        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }
    public static String formatDate(Date date,boolean toDotNet){
        if(date == null) return null;
        SimpleDateFormat format;

        format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.UK);
        return format.format(date);
    }
    public static String formatDate(Date date,String fallBack){

        if(date == null) return fallBack;
        SimpleDateFormat format;

        format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        return format.format(date);
    }
	public static Date parse( String input )  {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            if (input.endsWith("Z")) {
                input = input.substring(0, input.length() - 1) + "GMT-00:00";
            }
            return df.parse(input);
        }catch (Exception e){
            e.printStackTrace();
            //todo: // FIXME: 2/7/16 
            return new Date();
        }
	}
    public static   String toString( Date date ) {

        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssz" );

        TimeZone tz = TimeZone.getTimeZone("UTC");

        df.setTimeZone( tz );

        String output = df.format( date );

        int inset0 = 9;
        int inset1 = 6;

        String s0 = output.substring( 0, output.length() - inset0 );
        String s1 = output.substring( output.length() - inset1, output.length() );

        String result = s0 + s1;

        result = result.replaceAll( "UTC", "+00:00" );

        return result;

    }
}
