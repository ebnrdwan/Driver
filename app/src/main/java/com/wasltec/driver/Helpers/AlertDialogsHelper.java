package com.wasltec.driver.Helpers;

import android.app.Activity;
import android.app.AlertDialog;

public class AlertDialogsHelper {

    public AlertDialog getAlert(Activity activity, String title, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(title).setMessage(message);
        dialog.setPositiveButton("OK", null);
        return dialog.create();
    }


}
