package com.wasltec.driver.Helpers;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import com.wasltec.driver.Database.DatabaseHandler;
import com.wasltec.driver.Helpers.GPS.GPSHelper;
import com.wasltec.driver.Helpers.Service.ServicePoster;
import com.wasltec.driver.Helpers.Service.ServiceResult;
import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.R;
import com.wasltec.driver.WebService.Responses.LoginResponse;
import com.wasltec.driver.WebService.ViewModels.LoginViewModel;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.StateChangedCallback;

public class DriverService extends Service {
    public static boolean forceStop = false;
    public static boolean isRun = false;
    public static Context context;
    public static GPSHelper gpsHelper;
    public static AppStateContext appStateContext = null;
    private final String TAG = "DRIVER_SERVICE";
    private static Driver mModel;
    public static boolean connectionLost = true;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(HubFactory.hub != null) {
            HubFactory.hub.removeSubscription("BadLogin");
            HubFactory.hub.removeSubscription("LaterCall");
            HubFactory.hub.removeSubscription("call");
            HubFactory.hub.removeSubscription("NotApproved");
            HubFactory.hub.removeSubscription("Subtract");
            HubFactory.hub.removeSubscription("Refund");
            HubFactory.hub.removeSubscription("ClientResponse");
            HubFactory.hub.removeSubscription("CancelWait");
            HubFactory.hub.removeSubscription("SecondLogin");
            HubFactory.hub.removeSubscription("BadLogin");
            HubFactory.hub.removeSubscription("Starttrip");
            HubFactory.hub.removeSubscription("Message");
        }
        forceStop = true;
        if (HubFactory.connection != null) {
            HubFactory.connection.clearInvocationCallbacks("Force Stop");
            HubFactory.connection.stop();
            HubFactory.connection = null;
        }
        AppStateContext.mHubSubscriber = null;
        HubFactory.hub = null;
        HubFactory.hub = null;
        if (HubFactory.awaitConnection != null)
            HubFactory.awaitConnection.cancel();
        if (appStateContext != null) {
            appStateContext.doStopConnection();
            appStateContext.destroyData();
        }
        appStateContext = null;
        if (gpsHelper != null) {
            gpsHelper.stopListen();
        }
        isRun = false;
        gpsHelper = null;
        Log.v(TAG, "destroy");
    }

    public void doStart() {
        context = this;
        mModel = (Driver) new DatabaseHandler(context).getSingle(new Driver(), 1);
        isRun = true;
        forceStop = false;
        appStateContext = new AppStateContext();
        AppStateContext.changeState(AppStateContext.STATE_FREE);
        Log.v(TAG, "start");

        gpsHelper = new GPSHelper(context);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                HubFactory.hub = new HubFactory().getTheHub(mModel.getAccessToken());
                Driver d = (Driver)new DatabaseHandler(context).getSingle(new Driver(),1);
                LoginViewModel model = new LoginViewModel();
                model.setNumber(d.getName());
                model.setPassword(d.getPassword());
                new PerformLogin().execute(model);
                if (HubFactory.hub != null)
                {
                    HubFactory.hub.subscribe(AppStateContext.mHubSubscriber);
                }
                if (HubFactory.connection != null)
                    HubFactory.connection.stateChanged(new StateChangedCallback() {
                        @Override
                        public void stateChanged(ConnectionState connectionState, ConnectionState connectionState2) {
                            BroadcastHelper.sendInform(BroadcastHelper.UPDATE_CONNECTION_STATE_BROADCAST_METHOD);
                            switch (connectionState2) {
                                case Connected:
                                    Log.v(TAG, "connected " + connectionLost);
                                    Log.v(TAG, HubFactory.connection.getConnectionId());
                                    if (connectionLost) {
                                        AppStateContext.doUpdateLocation();
                                        Log.v(TAG, "connected");
                                        connectionLost = false;
                                    }
                                    break;
                                case Connecting:
                                    Log.v(TAG, "connecting");
                                    break;
                                case Disconnected:
                                    connectionLost = true;
                                    Log.v(TAG, "disconnected");
                                    break;
                                case Reconnecting:
                                    Log.v(TAG, "reconnect " + connectionLost);
                                    Log.v(TAG, HubFactory.connection.getConnectionId());
                                    AppStateContext.doUpdateLocation();
                                    Log.v(TAG, "connected");
                                    break;
                            }
                        }
                    });

                Log.v(TAG, "end");
                BroadcastHelper.sendInform(BroadcastHelper.UPDATE_CONNECTION_STATE_BROADCAST_METHOD);
            }
        });
        thread.start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "on bind");
        return null;
    }

    @Override
    public void onCreate() {
        doStart();
        Log.v(TAG, "create");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "start command");
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle(getString(R.string.service_name));
        builder.setTicker(getString(R.string.service_title));
        builder.setContentText(getString(R.string.service_content_text));
        final Intent notificationIntent = new Intent(this, DriverService.class);
        final PendingIntent pi = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        builder.setContentIntent(pi);
        final Notification notification = builder.build();
        startForeground(7, notification);
        return START_STICKY;
    }

    public class PerformLogin extends AsyncTask<LoginViewModel, String, ServiceResult> {
       LoginViewModel mLoginViewModel;
        @Override
        protected ServiceResult doInBackground(LoginViewModel... params) {
            mLoginViewModel = params[0];
            return new ServicePoster(DriverService.context).DoPost(params[0].toString(), getString(R.string.login_url), true);
        }

        @Override
        protected void onPostExecute(ServiceResult s) {
            LoginResponse response = LoginResponse.newInstance(s.getResult(), mLoginViewModel);
            Log.v("res", s.getResult());
            if (response != null) {
                new DatabaseHandler(DriverService.context).updateItem(response.getDriver());
            }
        }
    }
}

