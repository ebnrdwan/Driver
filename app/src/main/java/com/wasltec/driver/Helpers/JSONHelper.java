package com.wasltec.driver.Helpers;


import android.content.Context;
import android.util.Log;

import com.wasltec.driver.Model.City;
import com.wasltec.driver.Model.Country;
import com.wasltec.driver.R;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JSONHelper {
    private static String loadJSONFromAsset(Context context) {
        String json = null;
        try {

            InputStream is = context.getAssets().open("cities.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public List<Country> getCountries(Context context) {
        String countriesString = context.getString(R.string.counties);
        Log.v("mustafa", countriesString);
        List<Country> countries = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(countriesString);
            for (int i = 0; i < jsonArray.length(); i++) {
                Country country = new Country();
                country.id = jsonArray.getJSONObject(i).getInt("Id");
                country.Name = jsonArray.getJSONObject(i).getString("NameEn");
                country.Code = jsonArray.getJSONObject(i).getString("Code");
                country.NameAr = jsonArray.getJSONObject(i).getString("NameAr");
                countries.add(country);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return countries;
    }

    public List<City> getCities(Context context, int countryId) {
        // String citiesString = context.getString(R.string.cities);
        String citiesString = loadJSONFromAsset(context);
        Log.v("ehab", citiesString);
        List<City> cities = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(citiesString);
            for (int i = 0; i < jsonArray.length(); i++) {
                City city = new City();
                city.countryId = jsonArray.getJSONObject(i).getInt("countryId");
                city.id = jsonArray.getJSONObject(i).getInt("cityId");
                city.Name = jsonArray.getJSONObject(i).getString("name");
                city.NameAr = jsonArray.getJSONObject(i).getString("nameAr");
                if (city.countryId == countryId)
                    cities.add(city);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cities;
    }

}
