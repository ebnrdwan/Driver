package com.wasltec.driver.Helpers.Service;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.wasltec.driver.WebService.Requests.BaseRequest;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

public class ServicePoster {
    String crlf = "\r\n";
    String twoHyphens = "--";
    String boundary = "*****";
    String lineEnd = "\r\n";
    private Context mContext;

    public ServicePoster(Context context) {
        this.mContext = context;
    }

    public ServiceResult DoPost(String data, String _url, boolean login) {
        ServiceResult result = new ServiceResult();
        try {
            Log.v("service", _url);
            URL url = new URL(_url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
            connection.setRequestProperty("grant_type", "password");
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            Log.v("service", data);
            wr.write(data);
            wr.flush();
            InputStream inputStream;
            Log.v("content", connection.getContentEncoding() + "");
            if (connection.getResponseCode() == 200) {
                inputStream = new BufferedInputStream((connection.getContentEncoding() != null && connection.getContentEncoding().equalsIgnoreCase("gzip")) ? new GZIPInputStream(connection.getInputStream()) : connection.getInputStream());
                result.setSuccess(true);
            } else {
                inputStream = new BufferedInputStream((connection.getContentEncoding() != null && connection.getContentEncoding().equalsIgnoreCase("gzip")) ? new GZIPInputStream(connection.getErrorStream()) : connection.getErrorStream());
                result.setSuccess(false);
            }
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader readStream = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = readStream.readLine()) != null)
                stringBuilder.append(line);
            Log.v("serviceResponse", stringBuilder.toString());
            result.setSuccess(true);
            result.setResult(stringBuilder.toString());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ServiceResult DoPost(BaseRequest request, String accessToken) {
        ServiceResult result = new ServiceResult();
        try {
            URL url = new URL(mContext.getString(request.getMethodUrl()));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
            if (accessToken != null)
                connection.setRequestProperty("Authorization", "Bearer " + accessToken);
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            Log.v("service", request.toJson().toString());
            wr.write(request.toJson().toString());
            wr.flush();
            InputStream inputStream;
            Log.v("content", connection.getContentEncoding() + "");
            if (connection.getResponseCode() == 200) {
                inputStream = new BufferedInputStream((connection.getContentEncoding() != null && connection.getContentEncoding().equalsIgnoreCase("gzip")) ? new GZIPInputStream(connection.getInputStream()) : connection.getInputStream());
                result.setSuccess(true);
            } else {
                inputStream = new BufferedInputStream((connection.getContentEncoding() != null && connection.getContentEncoding().equalsIgnoreCase("gzip")) ? new GZIPInputStream(connection.getErrorStream()) : connection.getErrorStream());
                result.setSuccess(false);
            }
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader readStream = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = readStream.readLine()) != null)
                stringBuilder.append(line);
            Log.v("serviceResponse", stringBuilder.toString());
            result.setSuccess(true);
            result.setResult(stringBuilder.toString());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ServiceResult DoPost(PostRequest request) {
        ServiceResult result = new ServiceResult();
        try {
            Log.v("service", request.getUrl());
            URL url = new URL(request.getUrl());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
            connection.setRequestProperty("grant_type", "password");
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            Log.v("service", request.getData().toString());
            wr.write(request.getData().toString());
            wr.flush();
            InputStream inputStream;
            Log.v("content", connection.getContentEncoding() + "");
            if (connection.getResponseCode() == 200) {
                inputStream = new BufferedInputStream((connection.getContentEncoding() != null && connection.getContentEncoding().equalsIgnoreCase("gzip")) ? new GZIPInputStream(connection.getInputStream()) : connection.getInputStream());
                result.setSuccess(true);
            } else {
                inputStream = new BufferedInputStream((connection.getContentEncoding() != null && connection.getContentEncoding().equalsIgnoreCase("gzip")) ? new GZIPInputStream(connection.getErrorStream()) : connection.getErrorStream());
                result.setSuccess(false);
            }
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader readStream = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = readStream.readLine()) != null)
                stringBuilder.append(line);
            Log.v("serviceResponse", stringBuilder.toString());
            result.setSuccess(true);
            result.setResult(stringBuilder.toString());
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String DoPost(PostRequest request, int d) {
        try {
            URL url = new URL(request.getUrl());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setChunkedStreamingMode(0);
            connection.setConnectTimeout(1000 * 600);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Charset", "UTF-8");
            connection.setRequestProperty("Content-Type", "multipart/form-data boundary=" + boundary);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            if (request.getData() != null) {
                for (int i = 0; i < request.getData().length(); i++) {
                    String key = request.getData().names().getString(i);
                    wr.writeBytes(twoHyphens + boundary + lineEnd);
                    wr.writeBytes("Content-Disposition: form-data; charset=UTF-8 name=\"" + key + "\"" + lineEnd);
                    wr.writeBytes(lineEnd);
                    wr.writeBytes(request.getData().getString(key));
                    wr.writeBytes(lineEnd);
                    wr.writeBytes(twoHyphens + boundary + lineEnd);
                }
            }
//            wr = insertImage(image1, wr, "PersonalPic");
//            wr = insertImage(image2, wr, "PersonalIDPic");
//            wr = insertImage(image3, wr, "PersonalDocPic");
            wr.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            //wr.writeBytes("Content-Type: text/plain;charset=UTF-8");
            int response = connection.getResponseCode();
            Log.v("responseCode", String.valueOf(response));
            if (response != 200) {
                InputStream inputStream = new BufferedInputStream(connection.getErrorStream());
                StringBuilder stringBuilder = new StringBuilder();
                BufferedReader readStream = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = readStream.readLine()) != null)
                    stringBuilder.append(line);
                wr.flush();
                return stringBuilder.toString();
            }
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader readStream = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = readStream.readLine()) != null)
                stringBuilder.append(line);
            wr.flush();
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Error";
    }

    private DataOutputStream insertImage(Bitmap image, DataOutputStream wr, String name) {
        try {
            if (image != null) {
                String attachmentFileName = name + ".bmp";
                wr.writeBytes(twoHyphens + boundary + crlf);
                wr.writeBytes("Content-Disposition: form-data; charset=UTF-8 name=\"" + name + "\";filename=\"" + attachmentFileName + "\"" + crlf);
                wr.writeBytes(crlf);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] pixels = stream.toByteArray();

                wr.write(pixels);
                wr.writeBytes(crlf);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wr;
    }
}