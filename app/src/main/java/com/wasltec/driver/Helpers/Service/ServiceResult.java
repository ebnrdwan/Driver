package com.wasltec.driver.Helpers.Service;


public class ServiceResult {
    private boolean mSuccess = false;
    private String mResult ="";

    public String getResult() {
        return mResult;
    }
    public boolean getSuccess(){
        return  mSuccess;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public void setSuccess(boolean success) {
        mSuccess = success;
    }
}
