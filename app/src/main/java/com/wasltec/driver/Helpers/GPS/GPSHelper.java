package com.wasltec.driver.Helpers.GPS;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.wasltec.driver.Helpers.AppStateContext;
import com.wasltec.driver.Helpers.BroadcastHelper;
import com.wasltec.driver.Helpers.DriverService;

import java.util.ArrayList;

public class GPSHelper implements LocationListener {
    private static final String TAG = "GPSHelper";
    private static ArrayList<IGpsListener> listeners = new ArrayList<>();
    private static boolean isStarted;
    private static LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_GPS_DISTANCE = 0f;
    private static final float LOCATION_NETWORK_DISTANCE = 1000f;
    public Context context;

    public void stopListen() {
        isStarted=false;
        if (mLocationManager != null) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocationManager.removeUpdates(this);
        }
    }

    public void changeGPSProvider() {
        stopListen();
        if (mLocationManager == null)
            mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        if (AppStateContext.State.isInCall() || !mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_GPS_DISTANCE,
                    this);
        } else {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_NETWORK_DISTANCE,
                    this);
        }
    }

    public GPSHelper(Context _context) {
        this.context = _context;
        startUp(_context);
    }

    private void startUp(Context _context) {
        if (!isStarted) {
            isStarted = true;
            mLocationManager = (LocationManager) this.context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            if (!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) && !mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                BroadcastHelper.sendInform(BroadcastHelper.GPS_NOT_AVAILABLE_BROADCAST_METHOD);
            }
            try {
                if (AppStateContext.State.isInTrip() || !mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    mLocationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_GPS_DISTANCE,
                            this);
                } else {
                    if (mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        mLocationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_NETWORK_DISTANCE,
                                this);
                    } else {
                        mLocationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_GPS_DISTANCE,
                                this);
                    }
                }
            } catch (SecurityException ex) {
                Log.i(TAG, "fail to request location update, ignore", ex);
                BroadcastHelper.sendInform(BroadcastHelper.GPS_NOT_AVAILABLE_BROADCAST_METHOD);
            } catch (IllegalArgumentException ex) {
                Log.d(TAG, "network provider does not exist, " + ex.getMessage());
                BroadcastHelper.sendInform(BroadcastHelper.GPS_NOT_AVAILABLE_BROADCAST_METHOD);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location.getLatitude() != 0 && location.getLongitude() != 0) {
            Log.e(TAG, "onLocationChanged: " + location);
            if (DriverService.appStateContext != null)
                DriverService.appStateContext.LocationChanged(location);
            if (listeners != null && listeners.size() > 0) {
                for (IGpsListener listener : listeners) {
                    if (listener != null)
                        listener.gpsUpdate(location);
                }
            }
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        //Log.e(TAG, "onProviderDisabled: " + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        //Log.e(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //Log.e(TAG, "onStatusChanged: " + provider);
    }
}
