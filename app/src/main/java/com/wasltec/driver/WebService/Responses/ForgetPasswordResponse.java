package com.wasltec.driver.WebService.Responses;

import org.json.JSONObject;


public class ForgetPasswordResponse extends BaseResponse {


    public static ForgetPasswordResponse newInstance(String data) {
        try {
            ForgetPasswordResponse model = new ForgetPasswordResponse();
            JSONObject object = new JSONObject(data);
            model.setSuccess(object.getBoolean("Success"));
            model.setMessage(object.getString("Message"));
            model.setCode(object.getInt("Code"));
            model.setValidResponse(true);
            return model;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


}
