package com.wasltec.driver.WebService.Requests;


import com.wasltec.driver.R;
import com.wasltec.driver.WebService.ViewModels.UpdateProfileViewModel;

import org.json.JSONObject;

public class UpdateProfileRequest extends BaseRequest {
    private UpdateProfileViewModel mModel;
    public UpdateProfileRequest(){
        setModel(new UpdateProfileViewModel());
    }

    public UpdateProfileViewModel getModel() {
        return mModel;
    }

    public void setModel(UpdateProfileViewModel model) {
        mModel = model;
    }

    @Override
    public int getMethodUrl() {
        return R.string.url_update_profile;
    }

    @Override
    public JSONObject toJson() {
        return mModel.toJson();
    }
}
