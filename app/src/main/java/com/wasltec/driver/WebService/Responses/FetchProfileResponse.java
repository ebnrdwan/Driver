package com.wasltec.driver.WebService.Responses;

import android.util.Log;
import com.wasltec.driver.Models.Profile;
import org.json.JSONObject;

public class FetchProfileResponse extends BaseResponse  {

    private Profile mProfile;

    public static FetchProfileResponse newInstance(String data) {
        try {
            Log.e("alldata",data);
            FetchProfileResponse instance = new FetchProfileResponse();
            JSONObject object = new JSONObject(data).getJSONObject("Data");
            JSONObject result = new JSONObject(data);
            instance.setProfile(new Profile());
            instance.getProfile().setFirstName(object.getString("FirstName"));
            instance.getProfile().setEmail(object.getString("Email"));
            instance.getProfile().setPlateNumber(object.getString("PlateNumber"));
            instance.getProfile().setCompany(object.getString("Company"));
            instance.getProfile().setCredit(object.getInt("Credit"));
            instance.getProfile().setHourCost(object.getDouble("HourCost"));
            instance.getProfile().setKilometerCost(object.getDouble("KilometerCost"));
            instance.getProfile().setOpenDoorPrice(object.getDouble("OpenDoorPrice"));
            instance.getProfile().setWaitingPerMinuteCost(object.getDouble("WaitingPerMinuteCost"));
            instance.getProfile().setSupportCredit(object.getBoolean("SupportCredit"));
            instance.getProfile().setCurrancyArabic(object.getString("CurrancyArabicName"));
            instance.getProfile().setCurrancyEnglish(object.getString("CurrancyEnglishName"));
            instance.getProfile().setIsServiceProvider(object.getBoolean("IsServiceProvider"));
            instance.getProfile().setVehicleTypeArabic(object.getString("vechletypeArabicName"));
            instance.getProfile().setVehicleTypeEnglish(object.getString("VechleTypeEnglishName"));
            instance.setSuccess(result.getBoolean("Success"));
            instance.setMessage(result.getString("Message"));
            instance.setCode(result.getInt("Code"));
            instance.setValidResponse(true);
            return instance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public Profile getProfile() {
        return mProfile;
    }

    public void setProfile(Profile profile) {
        mProfile = profile;
    }
}