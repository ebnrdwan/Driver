package com.wasltec.driver.WebService.Responses;


import com.wasltec.driver.WebService.ViewModels.CarModel;
import com.wasltec.driver.WebService.ViewModels.CarColor;
import com.wasltec.driver.WebService.ViewModels.VehicleType;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GetDataResponse extends BaseResponse {

    private ArrayList<CarColor> mColors;
    private ArrayList<CarModel> mModels;
    private ArrayList<VehicleType> mVehicleTypes;

    public ArrayList<CarColor> getColors() {
        return mColors;
    }

    public void setColors(ArrayList<CarColor> colors) {
        mColors = colors;
    }

    public ArrayList<CarModel> getModels() {
        return mModels;
    }

    public void setModels(ArrayList<CarModel> models) {
        mModels = models;
    }

    public ArrayList<VehicleType> getVehicleTypes() {
        return mVehicleTypes;
    }

    public void setVehicleTypes(ArrayList<VehicleType> vehicleTypes) {
        mVehicleTypes = vehicleTypes;
    }

    public GetDataResponse() {
        setColors(new ArrayList<CarColor>());
        setModels(new ArrayList<CarModel>());
        setVehicleTypes(new ArrayList<VehicleType>());
    }

    public static GetDataResponse newInstance(String jsonData) {

        try {
            GetDataResponse item = new GetDataResponse();
            JSONObject object = new JSONObject(jsonData);
            item.setSuccess(object.getBoolean("Success"));
            item.setCode(object.getInt("Code"));
            JSONArray colorArray = object.getJSONObject("Data").getJSONArray("Colors");
            for(int i=0;i<colorArray.length();i++){
                CarColor color = CarColor.newInstance(colorArray.getString(i));
                if(color != null)
                item.getColors().add(color);
            }
            JSONArray modelsArray = object.getJSONObject("Data").getJSONArray("Models");
            for(int i=0;i<modelsArray.length();i++){
                CarModel model = CarModel.newInstance(modelsArray.getString(i));
                if(model != null)
                    item.getModels().add(model);
            }
            JSONArray vehiclesArray = object.getJSONObject("Data").getJSONArray("Vehicles");
            for(int i=0;i<vehiclesArray.length();i++){
                VehicleType vehicle = VehicleType.newInstance(vehiclesArray.getString(i));
                if(vehicle != null)
                    item.getVehicleTypes().add(vehicle);
            }
            item.setValidResponse(true);
            return item;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
