package com.wasltec.driver.WebService.ViewModels;

import com.wasltec.driver.Models.BaseModel;
import com.wasltec.driver.R;
import org.json.JSONObject;

public class UpdateProfileViewModel extends BaseModel implements IBaseViewModel, IViewModelValidation {
    private String mFirstName;
    private String mEmail;
    private String mNewPassword;


    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }



    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getNewPassword() {
        return mNewPassword;
    }

    public void setNewPassword(String newPassword) {
        mNewPassword = newPassword;
    }

    @Override
    public JSONObject toJson() {
        JSONObject object = new JSONObject();
        try {
            object.put("Name", getFirstName());
            object.put("NewPassword", getNewPassword());
            object.put("Email", getEmail());
        }catch (Exception e){
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public ValidationResult Validate() {

        if (getFirstName() == null || getFirstName().length() < 2)
            return new ValidationResult(false,R.string.please_enter_first_name);
        if (getNewPassword() == null || getNewPassword().length() < 6)
            return new ValidationResult(false,R.string.please_enter_password);
        if (getEmail() == null || getEmail().length() < 6)
            return new ValidationResult(false,R.string.please_enter_email_address);

        return new ValidationResult(true,R.string.success);
    }
}
