package com.wasltec.driver.WebService.ViewModels;


import com.wasltec.driver.R;

import org.json.JSONObject;

public class ForgetPasswordViewModel  implements IViewModelValidation,IBaseViewModel {
    private String mNumber;

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }


    @Override
    public JSONObject toJson() {
        JSONObject object = new JSONObject();
        try{
            object.put("Number",getNumber());
        }catch (Exception e){
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public ValidationResult Validate() {
        if(getNumber() == null || getNumber().length() < 6)
            return new ValidationResult(false, R.string.login_activity_enter_your_mobile_toast);
        return new ValidationResult(true,R.string.success);
    }
}
