package com.wasltec.driver.WebService.Requests;


import com.wasltec.driver.R;
import com.wasltec.driver.WebService.ViewModels.UpdatePushViewModel;

import org.json.JSONObject;

public class UpdatePushRequest extends BaseRequest {
private UpdatePushViewModel mModel;

    public UpdatePushViewModel getModel() {
        return mModel;
    }

    public void setModel(UpdatePushViewModel model) {
        mModel = model;
    }

    @Override
    public int getMethodUrl() {
        return R.string.url_update_push;
    }

    @Override
    public JSONObject toJson() {
        return getModel().toJson();
    }
}
