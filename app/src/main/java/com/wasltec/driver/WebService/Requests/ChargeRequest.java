package com.wasltec.driver.WebService.Requests;

import com.wasltec.driver.R;

import org.json.JSONObject;


public class ChargeRequest extends BaseRequest {
    private String mCardNumber;
    public ChargeRequest(String number){
        setCardNumber(number);
    }
    @Override
    public int getMethodUrl() {
        return R.string.charge_url;
    }

    @Override
    public JSONObject toJson() {
        JSONObject object = new JSONObject();
        try{
            object.put("Key",getCardNumber());
        }catch (Exception e){
            e.printStackTrace();
        }
        return object;
    }

    public String getCardNumber() {
        return mCardNumber;
    }

    public void setCardNumber(String cardNumber) {
        mCardNumber = cardNumber;
    }
}
