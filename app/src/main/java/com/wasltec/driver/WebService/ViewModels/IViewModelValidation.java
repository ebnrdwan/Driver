package com.wasltec.driver.WebService.ViewModels;

/**
 * Created by mustafa on 26/06/2015.
 */
public interface IViewModelValidation {
     ValidationResult Validate();
}
