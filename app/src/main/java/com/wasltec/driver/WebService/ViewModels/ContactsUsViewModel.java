package com.wasltec.driver.WebService.ViewModels;

import com.wasltec.driver.R;

import org.json.JSONObject;


public class ContactsUsViewModel  implements IViewModelValidation,IBaseViewModel {
    private final String TITLE_KEY="title";
    private final String BODY_KEY ="body";

    private String mTitle,mBody;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    @Override
    public JSONObject toJson() {
        JSONObject object = new JSONObject();
        try{
            object.put(TITLE_KEY,getTitle());
            object.put(BODY_KEY,getBody());
        }catch (Exception e){
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public ValidationResult Validate() {
        if(getTitle() == null || getTitle().length() < 3)
            return new ValidationResult(false, R.string.please_enter_message_title);
        if(getBody() == null || getBody().length() < 3)
            return new ValidationResult(false,R.string.please_enter_message_body);
        return new ValidationResult(true,R.string.success);
    }
}
