package com.wasltec.driver.WebService.ViewModels;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;

import com.wasltec.driver.Helpers.DateHelper;
import com.wasltec.driver.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegisterViewModel implements IViewModelValidation, IBaseViewModel {

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public JSONObject toJson() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("CountryId", getCountry());
            jsonObject.put("CityId", getCity());
            jsonObject.put("FirstName", getFirstName());
            jsonObject.put("LastName", getLastName());
            jsonObject.put("Username", getNumber());
            jsonObject.put("Password", getPassword());
            jsonObject.put("Number", getNumber());
            jsonObject.put("CarPlatNumber", getCarPlatNumber());
            jsonObject.put("Email", getEmail());
            jsonObject.put("InsuranceExpirationDate", DateHelper.toString(getInsuranceExpirationDate()));
            jsonObject.put("InsuranceNumber", getInsuranceNumber());
            jsonObject.put("InteriorColor", getInteriorColor());
            jsonObject.put("ExteriorColor", getExteriorColor());
            jsonObject.put("VehicleType", getVehicleType());
            jsonObject.put("DriverLicenseExpirationDate", DateHelper.toString(getDriverLicenseExpirationDate()));
            jsonObject.put("DriverLicenseNumber", getDriverLicenseNumber());
            jsonObject.put("MobileTypeId", getMobileTypeId());
            jsonObject.put("OSVersion", getOsVersion());
            jsonObject.put("AppVersion", getAppVersion());
            jsonObject.put("CarModel", getModel());
            jsonObject.put("EmployeeCode", getEmployeeCode());
            jsonObject.put("IsServiceProvider",isServiceProvider());
            return jsonObject;
        } catch (Exception e) {
            return null;
        }
    }

    private String mNumber;
    private String mFirstName;
    private String mLastName;
    private String mEmail;
    private int mCountry;
    private int mCity;
    private int mModel;
    private String mCarPlatNumber;
    private String mPassword;
    private Date mInsuranceExpirationDate;
    private String mInsuranceNumber = "empty string";
    private int mExteriorColor = 1;
    private int mInteriorColor = 1;
    private int mVehicleType;
    private Date mDriverLicenseExpirationDate;
    private String mDriverLicenseNumber = "empty string";
    private Date mDriverResidencyExpirationDate;
    private String mDriverResidencyNumber = "empty string";
    private String mEmployeeCode = "";
    private int mMobileTypeId = 2;
    private String mOsVersion;
    private int mAppVersion;
    private boolean isServiceProvider;

    public RegisterViewModel(Context context) {
        setOsVersion(Build.VERSION.RELEASE);
        setDriverLicenseExpirationDate(new Date());
        setDriverResidencyExpirationDate(new Date());
        setInsuranceExpirationDate(new Date());
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            setAppVersion(pInfo.versionCode);
        } catch (Exception e) {
            e.printStackTrace();
            setAppVersion(777);
        }
    }

    public Date getInsuranceExpirationDate() {
        return mInsuranceExpirationDate;
    }

    public void setInsuranceExpirationDate(Date insuranceExpirationDate) {
        mInsuranceExpirationDate = insuranceExpirationDate;
    }

    public String getInsuranceNumber() {
        return mInsuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        mInsuranceNumber = insuranceNumber;
    }

    public int getExteriorColor() {
        return mExteriorColor;
    }

    public void setExteriorColor(int exteriorColor) {
        mExteriorColor = exteriorColor;
    }

    public int getInteriorColor() {
        return mInteriorColor;
    }

    public void setInteriorColor(int interiorColor) {
        mInteriorColor = interiorColor;
    }

    public int getVehicleType() {
        return mVehicleType;
    }

    public void setVehicleType(int vehicleType) {
        mVehicleType = vehicleType;
    }

    public Date getDriverLicenseExpirationDate() {
        return mDriverLicenseExpirationDate;
    }

    public void setDriverLicenseExpirationDate(Date driverLicenseExpirationDate) {
        mDriverLicenseExpirationDate = driverLicenseExpirationDate;
    }

    public String getDriverLicenseNumber() {
        return mDriverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        mDriverLicenseNumber = driverLicenseNumber;
    }

    public Date getDriverResidencyExpirationDate() {
        return mDriverResidencyExpirationDate;
    }

    public void setDriverResidencyExpirationDate(Date driverResidencyExpirationDate) {
        mDriverResidencyExpirationDate = driverResidencyExpirationDate;
    }

    public String getDriverResidencyNumber() {
        return mDriverResidencyNumber;
    }

    public void setDriverResidencyNumber(String driverResidencyNumber) {
        mDriverResidencyNumber = driverResidencyNumber;
    }

    public int getMobileTypeId() {
        return mMobileTypeId;
    }

    public void setMobileTypeId(int mobileTypeId) {
        mMobileTypeId = mobileTypeId;
    }

    public String getOsVersion() {
        return mOsVersion;
    }

    public void setOsVersion(String osVersion) {
        mOsVersion = osVersion;
    }

    public int getAppVersion() {
        return mAppVersion;
    }

    public void setAppVersion(int appVersion) {
        mAppVersion = appVersion;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public int getCountry() {
        return mCountry;
    }

    public void setCountry(int country) {
        mCountry = country;
    }

    public int getCity() {
        return mCity;
    }

    public void setCity(int city) {
        mCity = city;
    }

    public int getModel() {
        return mModel;
    }

    public void setModel(int model) {
        mModel = model;
    }

    public String getCarPlatNumber() {
        return mCarPlatNumber;
    }

    public void setCarPlatNumber(String carPlatNumber) {
        mCarPlatNumber = carPlatNumber;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getEmployeeCode() {
        return mEmployeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        mEmployeeCode = employeeCode;
    }

    public boolean isServiceProvider() {
        return isServiceProvider;
    }

    public void setIsServiceProvider(int isServiceProvider) {
        this.isServiceProvider = (isServiceProvider > 0);
    }

    @Override
    public ValidationResult Validate() {

        if (this.mNumber.trim().length() < 9)
            return new ValidationResult(false, R.string.register_view_model_enter_mobile_toast);
        if (this.mFirstName.trim().length() < 3)
            return new ValidationResult(false, R.string.register_view_model_activity_enter_your_fname_toast);
        if (this.mLastName.trim().length() < 3)
            return new ValidationResult(false, R.string.register_view_model_activity_enter_your_lname_toast);
        if (this.mPassword.trim().length() < 6)
            return new ValidationResult(false, R.string.register_view_model_enter_your_password_toast);
        if (this.mEmail.trim().length() < 6)
            return new ValidationResult(false, R.string.please_enter_email_address);
        if (!isEmailValid(this.mEmail))
            return new ValidationResult(false, R.string.wrong_email);
        if (this.mCarPlatNumber.trim().length() < 6 && !this.isServiceProvider)
            return new ValidationResult(false, R.string.register_view_model_enter_plat_number_toast);
        if (this.getCountry() == 0)
            return new ValidationResult(false, R.string.please_choose_country);
        if (this.getCity() == 0)
            return new ValidationResult(false, R.string.please_choose_city);
        if (this.mModel <= 0)
            return new ValidationResult(false, R.string.register_view_model_select_car_model_toast);
        return new ValidationResult(true, R.string.success);
    }


    private boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


}
