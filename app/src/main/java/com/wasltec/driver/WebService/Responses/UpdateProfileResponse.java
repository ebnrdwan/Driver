package com.wasltec.driver.WebService.Responses;


import org.json.JSONObject;

public class UpdateProfileResponse extends BaseResponse {
    public static UpdateProfileResponse newInstance(String data) {
        try {
            UpdateProfileResponse model = new UpdateProfileResponse();
            JSONObject object = new JSONObject(data);
            model.setSuccess(object.getBoolean("Success"));
            model.setMessage(object.getString("Message"));
            model.setCode(object.getInt("Code"));
            model.setValidResponse(true);
            return model;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
