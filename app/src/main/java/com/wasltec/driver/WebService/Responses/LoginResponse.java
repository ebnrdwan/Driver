package com.wasltec.driver.WebService.Responses;


import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.WebService.ViewModels.LoginViewModel;

import org.json.JSONObject;

public class LoginResponse extends BaseResponse {
    Driver mDriver;
//{"error":"invalid_grant","error_description":"The user name or password is incorrect."}
    public static LoginResponse newInstance(String data,LoginViewModel info) {
        try{
            LoginResponse model = new LoginResponse();
            JSONObject object = new JSONObject(data);
            if(object.has("error"))
                return null;
            model.setDriver(new Driver());
            model.getDriver().setName(info.getNumber());
            model.getDriver().setPassword(info.getPassword());
            model.getDriver().setCredit(0);
            model.getDriver().setId(1);
            model.getDriver().setCountryId(1);
            model.getDriver().setAccessToken(object.getString("access_token"));
            model.getDriver().setAccessExpires(object.getLong("expires_in"));
            model.setValidResponse(true);
            return model;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public Driver getDriver() {
        return mDriver;
    }

    public void setDriver(Driver driver) {
        mDriver = driver;
    }
}
