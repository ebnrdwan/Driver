package com.wasltec.driver.WebService.Requests;


import com.wasltec.driver.R;
import com.wasltec.driver.WebService.ViewModels.ContactsUsViewModel;

import org.json.JSONObject;

public class ContactUsRequest extends BaseRequest {
private ContactsUsViewModel mContactsUsViewModel;

    public ContactUsRequest(){
        setContactsUsViewModel(new ContactsUsViewModel());
    }
    public ContactsUsViewModel getContactsUsViewModel() {
        return mContactsUsViewModel;
    }

    public void setContactsUsViewModel(ContactsUsViewModel contactsUsViewModel) {
        mContactsUsViewModel = contactsUsViewModel;
    }

    @Override
    public int getMethodUrl() {
        return R.string.url_contact_us;
    }

    @Override
    public JSONObject toJson() {
        return mContactsUsViewModel.toJson();
    }
}
