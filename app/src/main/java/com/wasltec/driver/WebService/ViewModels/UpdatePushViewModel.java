package com.wasltec.driver.WebService.ViewModels;


import org.json.JSONObject;

public class UpdatePushViewModel implements IBaseViewModel {
    private final String PUSH_ENABLED_KEY="PushEnabled";
    private final String PUSH_TOKEN_KEY ="PushKey";

    private boolean mPushEnabled;
    private String mPushToken;

    public String getPushToken() {
        return mPushToken;
    }

    public void setPushToken(String pushToken) {
        mPushToken = pushToken;
    }

    public boolean isPushEnabled() {
        return mPushEnabled;
    }

    public void setPushEnabled(boolean pushEnabled) {
        mPushEnabled = pushEnabled;
    }

    @Override
    public JSONObject toJson() {
        JSONObject object = new JSONObject();
        try{
            object.put(PUSH_ENABLED_KEY,isPushEnabled());
            object.put(PUSH_TOKEN_KEY,getPushToken());
        }catch (Exception e){
            e.printStackTrace();
        }
        return object;
    }
}
