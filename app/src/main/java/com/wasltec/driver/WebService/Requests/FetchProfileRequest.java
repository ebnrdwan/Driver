package com.wasltec.driver.WebService.Requests;


import com.wasltec.driver.R;

import org.json.JSONObject;

public class FetchProfileRequest extends BaseRequest {
    @Override
    public int getMethodUrl() {
        return R.string.fetch_profile_url;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject();
    }
}
