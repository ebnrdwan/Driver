package com.wasltec.driver.WebService.ViewModels;


public class ValidationResult {
    private boolean mSuccess;
    private int mMessage;

    public ValidationResult(boolean _success, int _errorMessage) {
        setSuccess(_success);
        setMessage(_errorMessage);
    }

    public int getMessage() {
        return mMessage;
    }

    public void setMessage(int message) {
        this.mMessage = message;
    }

    public boolean getSuccess() {
        return mSuccess;
    }

    public void setSuccess(boolean success) {
        this.mSuccess = success;
    }
}
