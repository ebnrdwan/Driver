package com.wasltec.driver.WebService.ViewModels;


public class LaterCallViewModel {
    private String mClientName;
    private String mClientNumber;
    private String StartDate;
    private String EndDate;
    private double EndLat;
    private double EndLng;
    private String EndPlace;
    private   String StartPlace;
    private int PaymentType;
    private int Id;
    private double mStartLat;
    private double mStartLng;
    public LaterCallViewModel(){

    }

    public LaterCallViewModel(String clientName,String clientNumber,String startDate,String endDate,double endLat,double endLng, String endPlace,String startPlace, int paymentType,double startLat,double startLng){
        setClientName(clientName);
        setClientNumber(clientNumber);
        setStartDate(startDate);
        setEndDate(endDate);
        setEndLat(endLat);
        setEndLng(endLng);
        setEndPlace(endPlace);
        setStartLat(startLat);
        setStartLng(startLng);
        setStartDate(startPlace);
        setPaymentType(paymentType);
    }

    public double getStartLat() {
        return mStartLat;
    }

    public void setStartLat(double startLat) {
        mStartLat = startLat;
    }

    public double getStartLng() {
        return mStartLng;
    }

    public void setStartLng(double startLng) {
        mStartLng = startLng;
    }

    public String getClientName() {
        return mClientName;
    }

    public void setClientName(String clientName) {
        mClientName = clientName;
    }

    public String getClientNumber() {
        return mClientNumber;
    }

    public void setClientNumber(String clientNumber) {
        mClientNumber = clientNumber;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public double getEndLat() {
        return EndLat;
    }

    public void setEndLat(double endLat) {
        EndLat = endLat;
    }

    public double getEndLng() {
        return EndLng;
    }

    public void setEndLng(double endLng) {
        EndLng = endLng;
    }

    public String getEndPlace() {
        return EndPlace;
    }

    public void setEndPlace(String endPlace) {
        EndPlace = endPlace;
    }

    public String getStartPlace() {
        return StartPlace;
    }

    public void setStartPlace(String startPlace) {
        StartPlace = startPlace;
    }

    public int getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(int paymentType) {
        PaymentType = paymentType;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
