package com.wasltec.driver.WebService.ViewModels;


import org.json.JSONObject;

public class ContactUsViewModel implements IViewModelValidation, IBaseViewModel {
    private String mTitle;
    private String mBody;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    @Override
    public JSONObject toJson() {
        return null;
    }

    @Override
    public ValidationResult Validate() {
        return null;
    }
}
