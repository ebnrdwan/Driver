package com.wasltec.driver.WebService.ViewModels;


import org.json.JSONObject;

public class VehicleType {
    private final String ID_KEY = "Id", ENGLISH_NAME_KEY = "EnglishName", ARABIC_NAME_KEY = "ArabicName",HOUR_COST_KEY="HourCost",KILOMETER_COST_KEY="KilometerCost",OPEN_DOOR_PRICE_KEY="OpenDoorPrice",WAITING_PER_MINUTE_COST_KEY="WaitingPerMinuteCost",SUPPORT_SCHEDULE_KEY="SupportSchedule",IS_SERVICE_PROVIDER="IsServiceProvider";
    private int mId;
    private boolean mSupportSchedule;
    private String mArabicName;
    private String mEnglishName;
    private double mHourCost;
    private double mKilometerCost;
    private double mOpenDoorPrice;
    private double mWaitingPerMinuteCost;
    private boolean isServiceProvider;
    public static VehicleType newInstance(String jsonData) {
        try {
            VehicleType instance = new VehicleType();
            JSONObject object = new JSONObject(jsonData);
            instance.setId(object.getInt(instance.ID_KEY));
            instance.setEnglishName(object.getString(instance.ENGLISH_NAME_KEY));
            instance.setArabicName(object.getString(instance.ARABIC_NAME_KEY));
            instance.setSupportSchedule(object.getBoolean(instance.SUPPORT_SCHEDULE_KEY));
            instance.setHourCost(object.getDouble(instance.HOUR_COST_KEY));
            instance.setOpenDoorPrice(object.getDouble(instance.OPEN_DOOR_PRICE_KEY));
            instance.setWaitingPerMinuteCost(object.getDouble(instance.WAITING_PER_MINUTE_COST_KEY));
            instance.setIsServiceProvider(object.getBoolean(instance.IS_SERVICE_PROVIDER));
            return instance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public boolean isSupportSchedule() {
        return mSupportSchedule;
    }

    public void setSupportSchedule(boolean supportSchedule) {
        mSupportSchedule = supportSchedule;
    }

    public int getId() {
        return mId;
    }


    public void setId(int id) {
        mId = id;
    }

    public String getArabicName() {
        return mArabicName;
    }

    public void setArabicName(String arabicName) {
        mArabicName = arabicName;
    }

    public String getEnglishName() {
        return mEnglishName;
    }

    public void setEnglishName(String englishName) {
        mEnglishName = englishName;
    }

    public double getHourCost() {
        return mHourCost;
    }

    public void setHourCost(double hourCost) {
        mHourCost = hourCost;
    }

    public double getKilometerCost() {
        return mKilometerCost;
    }

    public void setKilometerCost(double kilometerCost) {
        mKilometerCost = kilometerCost;
    }

    public double getOpenDoorPrice() {
        return mOpenDoorPrice;
    }

    public void setOpenDoorPrice(double openDoorPrice) {
        mOpenDoorPrice = openDoorPrice;
    }

    public double getWaitingPerMinuteCost() {
        return mWaitingPerMinuteCost;
    }

    public void setWaitingPerMinuteCost(double waitingPerMinuteCost) {
        mWaitingPerMinuteCost = waitingPerMinuteCost;
    }

    public boolean isServiceProvider() {
        return isServiceProvider;
    }

    public void setIsServiceProvider(boolean isServiceProvider) {
        this.isServiceProvider = isServiceProvider;
    }
}
