package com.wasltec.driver.WebService.ViewModels;


import com.wasltec.driver.R;

import org.json.JSONObject;

public class LoginViewModel implements IViewModelValidation,IBaseViewModel  {
    private String mNumber;
    private String mPassword;
    private String mGrantType ="password";

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String number) {
        mNumber = number;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getGrantType() {
        return mGrantType;
    }

    @Override
    public String toString() {
        return "grant_type="+getGrantType()+"&UserName="+getNumber()+"&Password="+getPassword();
    }

    @Override
    public JSONObject toJson() {

        try{
            JSONObject object = new JSONObject();
            object.put("grant_type",getGrantType());
            object.put("UserName",getNumber());
            object.put("Password",getPassword());
            return object;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ValidationResult Validate() {
        if(getNumber() == null || getNumber().length() < 6)
        return new ValidationResult(false,R.string.login_activity_enter_your_mobile_toast);
        if(getPassword() == null || getPassword().length()< 6)
            return new ValidationResult(false, R.string.login_activity_enter_your_password_toast);
        return new ValidationResult(true,R.string.success);
    }
}
