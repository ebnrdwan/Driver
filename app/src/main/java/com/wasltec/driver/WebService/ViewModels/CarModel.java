package com.wasltec.driver.WebService.ViewModels;


import org.json.JSONObject;

public class CarModel {
    private final String ID_KEY = "Id", ENGLISH_NAME_KEY = "EnglishName", ARABIC_NAME_KEY = "ArabicName";
    private int mId;
    private String mEnglishName;
    private String mArabicName;
    public static CarModel newInstance(String jsonData) {
        try {
            CarModel instance = new CarModel();
            JSONObject object = new JSONObject(jsonData);
            instance.setId(object.getInt(instance.ID_KEY));
            instance.setEnglishName(object.getString(instance.ENGLISH_NAME_KEY));
            instance.setArabicName(object.getString(instance.ARABIC_NAME_KEY));
            return instance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getEnglishName() {
        return mEnglishName;
    }

    public void setEnglishName(String englishName) {
        mEnglishName = englishName;
    }

    public String getArabicName() {
        return mArabicName;
    }

    public void setArabicName(String arabicName) {
        mArabicName = arabicName;
    }
}
