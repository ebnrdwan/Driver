package com.wasltec.driver.WebService.Responses;

import org.json.JSONObject;

public class ContactUsResponse  extends BaseResponse {
    public static ContactUsResponse newInstance(String data) {
        ContactUsResponse instance = new ContactUsResponse();
        try {
            JSONObject object = new JSONObject(data);
            instance.setSuccess(object.getBoolean("Success"));
            instance.setMessage(object.getString("Message"));
            instance.setCode(object.getInt("Code"));
            instance.setValidResponse(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }
}
