package com.wasltec.driver.WebService.Responses;


public class BaseResponse {
    private boolean mValidResponse;
    private boolean mSuccess;
    private String mMessage;
    private int mCode;
    public boolean isValidResponse() {
        return mValidResponse;
    }

    public void setValidResponse(boolean validResponse) {
        mValidResponse = validResponse;
    }
    public boolean isSuccess() {
        return mSuccess;
    }

    public void setSuccess(boolean success) {
        mSuccess = success;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public int getCode() {
        return mCode;
    }

    public void setCode(int code) {
        mCode = code;
    }
}
