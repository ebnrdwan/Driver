package com.wasltec.driver.WebService.ViewModels;


import org.json.JSONObject;

public interface IBaseViewModel {
    JSONObject toJson();
}
