package com.wasltec.driver.WebService.Requests;


import org.json.JSONObject;

public abstract class BaseRequest {


    public abstract int getMethodUrl();
    public abstract JSONObject toJson();

}
