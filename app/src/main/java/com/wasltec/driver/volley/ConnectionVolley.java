package com.wasltec.driver.volley;

import android.app.ProgressDialog;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.wasltec.driver.Helpers.DialogsHelper;
import com.wasltec.driver.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shafeek on 21/09/16.
 */
public class ConnectionVolley extends StringRequest {

    private String url;
    Map<String, String> params;
    Map<String, String> headers;
    Context context;
    public static ProgressDialog dialog;
    boolean dialogStatus;

    public ConnectionVolley(Context context, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, Map<String, String> params, boolean dialogStatus) {
        super(method, url, listener, errorListener);
        this.params = params;
        this.context = context;
        this.dialogStatus = dialogStatus;
        this.headers = new HashMap<>();
        loadingDialog();
    }

    public ConnectionVolley(Context context, int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener, Map<String, String> params, Map<String, String> headers, boolean dialogStatus) {
        super(method, url, listener, errorListener);
        this.params = params;
        this.context = context;
        this.dialogStatus = dialogStatus;
        this.headers = headers;
        loadingDialog();
    }

    public ConnectionVolley(String url, Response.Listener<String> listener, Response.ErrorListener errorListener, Map<String, String> params) {
        super(url, listener, errorListener);
        this.params = params;
    }

    protected void loadingDialog() {
        if (dialogStatus) {
            dialog = DialogsHelper.getProgressDialog(context, context.getString(R.string.loading), context.getString(R.string.please_wait));
            dialog.show();
        }
    }

    @Override
    protected void deliverResponse(String response) {
        super.deliverResponse(response);
        try {
            dialog.dismiss();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        return super.parseNetworkResponse(response);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}
