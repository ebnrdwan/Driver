package com.wasltec.driver.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.wasltec.driver.Models.Balance;
import com.wasltec.driver.Models.HistoryTrip;
import com.wasltec.driver.Models.Info;
import com.wasltec.driver.Models.LaterRequest;
import com.wasltec.driver.Models.Profile;
import com.wasltec.driver.Models.TripCoordinates;
import com.wasltec.driver.Models.City;
import com.wasltec.driver.Models.Country;
import com.wasltec.driver.Models.Driver;
import com.wasltec.driver.Models.IModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final int DB_VERSION = 15;
    public static final String DB_NAME = "driver";

    public DatabaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS info (id INTEGER ,code TEXT ,name TEXT,openDoorPrice TEXT,kiloMeterPrice TEXT)");
        db.execSQL("CREATE TABLE IF NOT EXISTS trip (id INTEGER PRIMARY KEY , clientId INTEGER, startTime LONG, endTime LONG, cost DOUBLE, distance DOUBLE,clientMobile NUMBER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS tripCoordinates (id INTEGER PRIMARY KEY AUTOINCREMENT, lng DOUBLE, lat DOUBLE, time LONG, fkTripId INTEGER, FOREIGN KEY (fkTripId) REFERENCES trip(id) )");
        db.execSQL("CREATE TABLE IF NOT EXISTS profile_info (id INTEGER ,firstName TEXT,lastName TEXT ,mobileNumber TEXT,password TEXT,email TEXT,carplat TEXT,carModelId TEXT)");
        db.execSQL("INSERT INTO profile_info (id ,firstName ,lastName,mobileNumber,password,email) VALUES (1,'','','','','')");
        db.execSQL(new Country().getSqlCreation());
        db.execSQL(new City().getSqlCreation());
        db.execSQL(new Driver().getSqlCreation());
        db.execSQL(new Profile().getSqlCreation());
        db.execSQL(new LaterRequest().getSqlCreation());
        db.execSQL(new Balance().getSqlCreation());
    }

    public IModel getSingle(IModel model, int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(model.getSingleSql(id), null);
        if (cursor != null && cursor.moveToFirst()) {
            IModel iModel = model.fromCursor(cursor);
            cursor.close();
            db.close();
            return iModel;
        }
        return null;
    }

    public void updateItem(IModel model) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(model.getSingleSql(model.getId()), null);
        if (cursor != null && cursor.moveToFirst()) {
            db.update(model.getTableName(), model.toContentValues(), " " + model.getIdField() + " =?", new String[]{String.valueOf(model.getId())});
            cursor.close();
        } else {
            db.insert(model.getTableName(), null, model.toContentValues());
        }
        db.close();
    }

    public void updateItem(IModel model,SQLiteDatabase db) {
        Cursor cursor = db.rawQuery(model.getSingleSql(model.getId()), null);
        if (cursor != null && cursor.moveToFirst()) {
            db.update(model.getTableName(), model.toContentValues(), " " + model.getIdField() + " =?", new String[]{String.valueOf(model.getId())});
            cursor.close();
        } else {
            db.insert(model.getTableName(), null, model.toContentValues());
        }
    }

    public ArrayList<IModel> getList(IModel model) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(model.getListSql(), null);
        ArrayList<IModel> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            result.add(model.fromCursor(cursor));
        }
        cursor.close();
        db.close();
        return result;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Profile profile = getProfile(db);
        db.execSQL("DROP TABLE IF EXISTS "+new Profile().getTableName());
        db.execSQL("DROP TABLE IF EXISTS "+new LaterRequest().getTableName());
        this.onCreate(db);
        updateItem(profile,db);
    }

    public void ClearData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("info", "id = 1", null);
        db.execSQL(new LaterRequest().getClearSql());
        db.execSQL(new Driver().getClearSql());
        db.execSQL(new Profile().getClearSql());
        db.close();
        clearProfile();
    }

    public void updateInfo(Info info) {
        Info old = getInfo();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", 1);
        contentValues.put("name", info.name);
        contentValues.put("code", info.code);
        contentValues.put("openDoorPrice", info.openDoorPrice);
        contentValues.put("kiloMeterPrice", info.kiloMeterCost);
        if (old.name != null && old.name.length() > 0 && old.name.equals(info.name)) {
            Log.v("Register", "Old Profile");
            db.update("info", contentValues, "id = 1", null);
        } else {
            Log.v("Register", "New Profile");
            db.insertOrThrow("info", null, contentValues);
        }
        db.close();
    }

    public Info getInfo() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor query = db.rawQuery("SELECT * FROM info LIMIT 0,1", null);
        Info result = new Info();
        if (query != null) {
            if (query.moveToFirst()) {
                result.name = query.getString(query.getColumnIndex("name"));
                result.code = query.getString(query.getColumnIndex("code"));
                result.openDoorPrice = Double.parseDouble(query.getString(query.getColumnIndex("openDoorPrice")));
                result.kiloMeterCost = Double.parseDouble(query.getString(query.getColumnIndex("kiloMeterPrice")));
            }
            query.close();
        }
        db.close();
        return result;
    }

    public void clearProfile() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", 1);
        contentValues.put("firstName", "");
        contentValues.put("lastName", "");
        contentValues.put("mobileNumber", "");
        contentValues.put("password", "");
        contentValues.put("email", "");
        contentValues.put("carplat", "");
        contentValues.put("carModelId", "");
        db.update("profile_info", contentValues, "id = 1", null);
        Log.v("db", "profile cleared");
        db.close();
    }

    public void startTrip(HistoryTrip trip) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", trip.id);
        contentValues.put("clientId", trip.ClientId);
        contentValues.put("startTime", trip.startTime.getTime());
        long id = db.insert("trip", null, contentValues);
        Log.v("Trip", "Start Trip" + String.valueOf(id));
        db.close();
    }

    public void startTrip(int tripId, int clientId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", tripId);
        contentValues.put("clientId", clientId);
        contentValues.put("startTime", new Date().getTime());
        long id = db.insert("trip", null, contentValues);
        Log.v("Trip", "Start Trip" + String.valueOf(id));
        db.close();
    }

    public void endTrip(HistoryTrip trip) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("distance", trip.distance);
        contentValues.put("cost", trip.cost);
        contentValues.put("endTime", trip.endTime.getTime());
        int id = db.update("trip", contentValues, "id =" + trip.id, null);
        Log.v("Trip", "End Trip" + String.valueOf(id));
        db.close();
    }

    public void addTripCoordinates(TripCoordinates tripCoordinates) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("lat", tripCoordinates.getLatitude());
        contentValues.put("lng", tripCoordinates.getLongitude());
        contentValues.put("time", tripCoordinates.time.getTime());
        contentValues.put("fkTripId", tripCoordinates.fkTripId);
        long id = db.insert("tripCoordinates", null, contentValues);
        Log.v("Trip", "Add Trip Coordinates" + String.valueOf(id));
        db.close();
    }


    public ArrayList<HistoryTrip> getAllTrips() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor query = db.rawQuery("SELECT * FROM trip order by endTime DESC, startTime DESC", null);
        ArrayList<HistoryTrip> tripList = new ArrayList<>();
        while (query != null && query.moveToNext()) {
            HistoryTrip trip = new HistoryTrip();
            trip.id = query.getInt(query.getColumnIndex("id"));
            trip.ClientId = query.getInt(query.getColumnIndex("clientId"));
            trip.cost = query.getDouble(query.getColumnIndex("cost"));
            trip.distance = query.getDouble(query.getColumnIndex("distance"));
            trip.startTime = new Date(query.getLong(query.getColumnIndex("startTime")));
            trip.endTime = new Date(query.getLong(query.getColumnIndex("endTime")));
            SimpleDateFormat formatter;
            formatter = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = String.format("%s", formatter.format(trip.startTime));
            String endDate = String.format("%s", formatter.format(trip.endTime));
            if (startDate.equals("01-01-1970") || startDate.equals("٠١-٠١-١٩٧٠"))
                continue;
            if (endDate.equals("01-01-1970") || endDate.equals("٠١-٠١-١٩٧٠"))
                continue;
            tripList.add(trip);
        }
        if (query != null)
            query.close();
        db.close();
        return tripList;
    }

    public HistoryTrip getTripInfo(int tripId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor query = db.rawQuery("SELECT * FROM trip WHERE id =" + tripId, null);
        HistoryTrip trip = new HistoryTrip();
        if (query != null && query.moveToFirst()) {
            trip.id = query.getInt(query.getColumnIndex("id"));
            trip.ClientId = query.getInt(query.getColumnIndex("clientId"));
            trip.cost = query.getDouble(query.getColumnIndex("cost"));
            trip.distance = query.getDouble(query.getColumnIndex("distance"));
            trip.startTime = new Date(query.getLong(query.getColumnIndex("startTime")));
            trip.endTime = new Date(query.getLong(query.getColumnIndex("endTime")));
            query.close();
        }
        db.close();
        trip.tripCoordinates = getTripCoordinates(tripId);
        return trip;
    }

    private ArrayList<TripCoordinates> getTripCoordinates(int tripId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor query = db.rawQuery("SELECT tc.* FROM trip t INNER JOIN tripCoordinates tc on tc.fkTripId = t.id and t.id = " + tripId, null);
        ArrayList<TripCoordinates> tripCoordinatesList = new ArrayList<>();
        while (query != null && query.moveToNext()) {
            TripCoordinates tripCoordinates = new TripCoordinates();
            tripCoordinates.setLatitude(query.getDouble(query.getColumnIndex("lat")));
            tripCoordinates.setLongitude(query.getDouble(query.getColumnIndex("lng")));
            tripCoordinates.time = new Date(query.getLong(query.getColumnIndex("time")));
            tripCoordinates.fkTripId = tripId;
            tripCoordinatesList.add(tripCoordinates);
        }
        if (query != null)
            query.close();
        db.close();
        return tripCoordinatesList;
    }

    private Profile getProfile(SQLiteDatabase db){
        Cursor cursor = db.rawQuery("SELECT * FROM profile WHERE id =" + 1, null);
        Profile profile = new Profile();
        if (cursor!=null&&cursor.moveToNext())
            profile =(Profile) profile.fromCursor(cursor);
        if (profile.getVehicleTypeEnglish()==null){
            profile.setVehicleTypeEnglish("zzzzzzz");
            profile.setVehicleTypeArabic("zzzzzzz");
        }
        return profile;
    }
}